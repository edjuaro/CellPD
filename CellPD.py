from datetime import datetime
import tzlocal

local_tz = tzlocal.get_localzone()
TOOL_NAME = "CellPD: Cell Line Phenotype Digitizer"
VERSION = 1.0
TIME_STAMP = local_tz.localize(datetime.now()).isoformat(sep='T')
print("========================================")
print("Setup completed on", TIME_STAMP)
print("{:s} (version {:1.1f}) will begin to run now.".format(TOOL_NAME, VERSION))
print("========================================")

import sys
import argparse
import os

if not len(sys.argv) > 1:
    print("***************************************")
    print("***************************************")
    file = input("Please type the name of the input file:")
    #file = '_template'
    multiplot_line = False
    CALL_CELLPD = True
    CREATE_INPUTS = True
    CROSS = False

else:
    parser = argparse.ArgumentParser()
    parser.add_argument("name", help="specify input file name")
    parser.add_argument("--lines", help="Type '--lines=no' if you want to ignore connecting lines in the multiple environmental conditions case")
    parser.add_argument("--skip", help="Type '--skip=skip' if you want to skip parameter estiamtion and want to only replot data")
    parser.add_argument("--cl", help="Type '--cl=yes' if you want to make CrossLinker plots")

    args = parser.parse_args()
    file = args.name
    print("Using input file name '", file, "' as selected")

    if args.lines == 'no':
        print('We will ignore lines connecting different environmental conditions in plots.')
        multiplot_line = False
    else:
        multiplot_line = True

    if args.skip == 'skip':
        CALL_CELLPD = False
        CREATE_INPUTS = False
    else:
        CALL_CELLPD = True
        CREATE_INPUTS = True

    if args.cl == 'yes':
        CROSS = True
    else:
        CROSS = False


if ('.xlsx' not in file) or ('.xls' not in file):
    if os.path.isfile(file + ".xlsx"):
        print("Adding '.xlsx' to the filename")
        file = file + ".xlsx"
    elif os.path.isfile(file + ".xls"):
        print("Adding '.xls' to the filename")
        file = file + ".xls"

import shutil

shutil.copy("files/matplotlibrc", "matplotlibrc")

from lmfit import minimize, Parameters, report_fit
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cmap
import copy
from scipy.integrate import odeint
from itertools import cycle
from tabulate import tabulate
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.compat import range

import webbrowser
import time as get_time
import pickle

# For debugging purposes
# from inspect import currentframe, getframeinfo
from inspect import getframeinfo, stack

# Set the seed for reproducibility
np.random.seed(0)

# Define some global variables
# format_list = ["png", "svg", "tiff", "jpg", "pdf", "eps"]  # PDF and TIFF causes problems in some machines
# format_list = ["png", "svg"]
format_list = ["png",]
drop_values = False
full_output = False

OPEN_BROWSER = False
HOLD_CMD = False
# CREATE_INPUTS = True
#CALL_CELLPD = True

uncertainty_note = ''

tic = get_time.time()


def tic():
    tic = get_time.time()
    return


def toc():
    caller = getframeinfo(stack()[1][0])
    global tic
    elapsed = get_time.time() - tic
    print("Elapsed ", elapsed, "Currently on line", caller.lineno)
    tic = get_time.time()
    return


## Define all the models
def live(y, t, params):
    # Live Cell counts only
    # L = y[0]
    # growth_rate = params["growth_rate"].value
    # dL = growth_rate*L
    return np.reshape(params["seeding_cells"].value * np.exp(params["growth_rate"].value * (t - min(t))), (len(t), 1))


def live_ODE(y, t, params):
    # Live Cell counts only
    L = y[0]

    growth_rate = params["growth_rate"].value

    dL = growth_rate * L

    return dL


def live_logistic_ODE(y, t, params):
    # Live Cell counts only
    L = y[0]

    growth_rate = params["growth_rate"].value
    carrying_capacity = params["carrying_capacity"].value
    #
    # C =  -(1-(carrying_capacity/L0)/carrying_capacity)
    # return carrying_capacity/(1 - np.exp(-growth_rate*t-carrying_capacity*C))
    #
    dL = growth_rate * (1 - L / carrying_capacity) * L
    #
    return dL


def live_logistic(y, t, params):
    # Live Cell counts only
    L = y[0]

    growth_rate = params["growth_rate"].value
    carrying_capacity = params["carrying_capacity"].value

    dL = growth_rate * (1 - L / carrying_capacity) * L

    return dL


def gompertz(y, t, params): # Not being use in this version
    carrying_capacity = params["carrying_capacity"].value
    growth_rate = params["growth_rate"].value

    b = np.log(carrying_capacity / params["seeding_cells"].value)

    return np.reshape(carrying_capacity * np.exp(-b * np.exp(-growth_rate * (t - min(t)))), (len(t), 1))

def logistic_gf(y, t, params):
    carrying_capacity = params["carrying_capacity"].value
    growth_rate = params["growth_rate"].value
    y_0 = params["seeding_cells"].value

    lag = carrying_capacity/(4*growth_rate)*(np.log(carrying_capacity/y_0 - 1)-2)

    return np.reshape( carrying_capacity / ( t/t + np.exp( (4*growth_rate/carrying_capacity)* (lag - (t - min(t))) +2 ) ) ,( len(t), 1))


def live_dead(y, t, params):
    # Live Cell Counts + Viability
    # Requires four parameters: [birth_rate, death_rate, clearance_rate, carrying_capacity]
    L = y[0]
    D = y[1]

    birth_rate = params["birth_rate"].value
    death_rate = params["death_rate"].value
    clearance_rate = params["clearance_rate"].value

    dL = birth_rate * L - death_rate * L
    dD = death_rate * L - clearance_rate * D

    return [dL, dD]
    # return [max(0,dL),max(0,dD)]


def live_dead_logistic(y, t, params):
    # Live Cell Counts + Viability
    # Requires four parameters: [birth_rate, death_rate, clearance_rate, carrying_capacity]
    L = y[0]
    D = y[1]

    birth_rate = params["birth_rate"].value
    death_rate = params["death_rate"].value
    clearance_rate = params["clearance_rate"].value
    carrying_capacity = params["carrying_capacity"].value

    dL = birth_rate * (1 - L / carrying_capacity) * L - death_rate * L
    dD = death_rate * L - clearance_rate * D

    return [dL, dD]
    # return [max(0,dL),max(0,dD)]

def log_live_dead(y, t, params):
    # Live Cell Counts + Viability
    # Requires two parameters: [birth_slope, death_slope]

    L_0 = np.log(y[0])
    D_0 = np.log(y[1])

    birth_slope = params["birth_rate"].value
    death_slope = params["death_rate"].value

    # L = L_0 + birth_slope * (t - min(t))
    # D = D_0 + death_slope * (t - min(t))

    L = np.reshape(np.exp(L_0 + birth_slope * (t - min(t))), (len(t), 1))
    D = np.reshape(np.exp(D_0 + death_slope * (t - min(t))), (len(t), 1))

    output = np.empty(shape=(len(t),2))
    for temp_index in range(len(t)):
        output[temp_index]=[L[temp_index],D[temp_index]]
    return output


def total(y, t, params):
    # Live Cell Counts + Viability but we only track Total Cells
    # T_0 = y[0]
    # growth_rate = params["growth_rate"].value
    # dT = growth_rate*T
    return np.reshape(params["seeding_cells"].value * np.exp(params["growth_rate"].value * (t - min(t))), (len(t), 1))


def total_ODE(y, t, params):
    # Live Cell Counts + Viability but we only track Total Cells
    T = y[0]

    growth_rate = params["growth_rate"].value

    dT = growth_rate * T

    return dT


def total_logistic(y, t, params):
    # Live Cell Counts + Viability but we only track Total Cells
    T = y[0]

    growth_rate = params["growth_rate"].value
    carrying_capacity = params["carrying_capacity"].value

    dT = growth_rate * (1 - T / carrying_capacity) * T

    # minimum_population = y[0]*0.06

    # dT = growth_rate * (1- minimum_population/T) *(1 - T / carrying_capacity) * T

    return dT
    # return max(0,dT)

def total_allee(y, t, params): # Not being used in this version
    # Live Cell Counts + Viability but we only track Total Cells
    T = y[0]

    growth_rate = params["growth_rate"].value
    carrying_capacity = params["carrying_capacity"].value
    minimum_population = params["minimum_population"].value

    dT = growth_rate * (1- minimum_population/T) *(1 - T / carrying_capacity) * T

    return dT


# Create some global variables
# create a dictionary of functions
choose_model = {
    "live": live,
    "live_logistic": live_logistic,
    "live_dead": live_dead,
    "live_dead_logistic": live_dead_logistic,
    "total": total,
    "gompertz": gompertz,
    "logistic_gf": logistic_gf,
    "total_logistic": total_logistic,
    "total_allee": total_allee,
    "log_live_dead": log_live_dead,
}


def MyError(params, function_name, time, y_init, target, sigmas):
    # This function computes the (Weighted Relative) Square Error

    if function_name in ['live', 'total', 'gompertz','logistic_gf']:
        y = choose_model[function_name](params['seeding_cells'], time, params)
        y.shape = target.shape
    elif function_name in ['live_logistic', 'total_logistic','total_allee']:
        y = odeint(choose_model[function_name], params['seeding_cells'], time, args=(params,))
    elif function_name in ['live_dead', 'live_dead_logistic']:
        y = odeint(choose_model[function_name], [params['seeding_cells_live'], params['seeding_cells_dead']], time,
                   args=(params,))
    elif function_name in ['log_live_dead']:
        y = choose_model[function_name]([params['seeding_cells_live'].value, params['seeding_cells_dead'].value], time, params)
        # y = np.exp(y)
    else:
        print('The model [:s] is not supported.'.format(function_name))

    # print(y,target,time)

    z = copy.deepcopy(y)
    # print(target)
    # print(z)
    # input('----')
    errors = np.divide(z - target, sigmas)
    errors.shape = errors.shape[0] * errors.shape[1]

    return errors


def MAPE(params, function_name, time, y_init, target, sigmas):
    if function_name in ['live', 'total', 'gompertz', 'logistic_gf']:
        y = choose_model[function_name](y_init, time, params)
        y.shape = target.shape
    elif function_name in ['live_logistic', 'total_logistic','total_allee']:
        y = odeint(choose_model[function_name], params['seeding_cells'], time, args=(params,))
    elif function_name in ['live_dead', 'live_dead_logistic']:
        y = odeint(choose_model[function_name], [params['seeding_cells_live'], params['seeding_cells_dead']], time,
                   args=(params,))
    elif function_name in ['log_live_dead']:
        y = choose_model[function_name]([params['seeding_cells_live'].value, params['seeding_cells_dead'].value], time, params)
    else:
        print('The model "{:s}" is not supported.'.format(function_name))

    z = copy.deepcopy(y)

    errors = abs(100 * np.divide(z - target, target))
    errors.shape = errors.shape[0] * errors.shape[1]
    MAPE = sum(errors) / len(errors)

    return MAPE


# Define more global variables:
name_dictionary = {
    "live": "Live cells model",
    "live_logistic": "Live cells model (with logistic limits)",
    "live_dead": "Live cells model (with cell death)",
    "live_dead_logistic": "Live cells model (with cell death and logistic limits)",
    "total": "Total cells model",
    "total_ODE": "Total cells model (integrated ODE)",
    "total_logistic": "Total cells model (with logistic limits)",
    "total_allee": "Gompertzian model",
    "gompertz": "Gompertzian model",
    "logistic_gf": "Logistic model implemented by grofit",
    "log_live_dead": "Live dead model --in log space--",
}
title_dictionary = {
    "live": "Live cells model",
    "live_logistic": "Live cells model (with logistic limits)",
    "live_dead": "Live cells model (with cell death)",
    "live_dead_logistic": "Live cells model (with cell death and logistic limits)",
    "total": "Total cells model",
    "total_ODE": "Total cells model (ODE)",
    "total_logistic": "Total cells model (with logistic limits)",
    "total_allee": "total_allee",
    "gompertz": "Gompertzian model",
    "logistic_gf": "Logistic model implemented by grofit",
    "log_live_dead": "Live dead model --in log space--",
}
legend_dictionary = {
    "live": ["Live Cells"],
    "live_logistic": ["Live Cells"],
    "live_dead": ["Live Cells", "Dead Cells"],
    "live_dead_logistic": ["Live Cells", "Dead Cells"],
    "total": ["Total Cells"],
    "total_ODE": ["Total Cells (ODE)"],
    "total_logistic": ["Total Cells"],
    "total_allee": ["Total Cells"],
    "gompertz": ["Total Cells"],
    "logistic_gf": ["Total Cells"],
    "log_live_dead": ["log(Live Cells)", "log(Dead Cells)"],

}
sheet_dictionary = {
    "live": "Live",
    "live_logistic": "Live_logistic",
    "live_dead": "Live+Dead",
    "live_dead_logistic": "Live+Dead_logistic",
    "total": "Total_Cells",
    "total_ODE": "Total_Cells",
    "total_logistic": "Total_logistic",
    "total_allee": "Total_logistic",
    "gompertz": "Total_Cells",
    "logistic_gf": "Total_Cells",
    "log_live_dead": "Live+Dead",
}
description_dictionary = {
    "live": "This is an exponential model that describes the growth of the live cells.",
    "live_logistic": "This is an exponential model that describes the growth of the live cells "
                     "with logistic limitations.",
    "live_dead": "This is an exponential model that describes the growth of the live cells "
                 "and accounts for apoptosis.",
    "live_dead_logistic": "This is an exponential model that describes the growth of the live cells "
                          "with logistic limitations, it also accounts for apoptosis",
    "total": "This is an exponential model that describes the growth of the live and apoptosing cells combined.",
    "total_ODE": "This is an exponential model that describes the growth of the live and apoptosing cells combined "
                 "by integrating an Ordinary Differential Equation (ODE)",
    "total_logistic": "This is an exponential model that describes the growth of the live"
                      " and apoptosing cells combined accounting for logistic limitations.",
    "gompertz": "Gompertzian model [write a description here]",
    "total_allee": "Gompertzian model [write a description here]",
    "logistic_gf": "Logistic model implemented by grofit",
    "log_live_dead": "Live dead model --in log space--",
}
model_dictionary = {
    "live": r"\begin{equation}"
            "\n"
            r"\frac{d[\rm{Live}]}{dt} = [\rm{growth\_rate}][\rm{Live}]"
            "\n"
            r"\end{equation}"
            "\n",
    "live_logistic": r"\begin{equation}"
                     "\n"
                     r"\frac{d[\rm{Live}]}{dt} = [\rm{growth\_rate}]\left(1-\frac{[\rm{Live}]}{{[\rm{max\_density}]}}\right)[\rm{Live}]"
                     "\n"
                     r"\end{equation}"
                     "\n",
    "live_dead": r"\begin{eqnarray}"
                 "\n"
                 r"\frac{d[\rm{Live}]}{dt} &=& [\rm{birth\_rate}][\rm{Live}] - [\rm{death\_rate}][\rm{Live}]\\"
                 "\n"
                 r"\frac{d[\rm{Dead}]}{dt} &=& [\rm{death\_rate}][\rm{Live}] - [\rm{clearance\_rate}][\rm{Dead}]\\"
                 "\n"
                 r"\end{eqnarray}"
                 "\n",
    "live_dead_logistic": r"\begin{eqnarray}"
                          "\n"
                          r"\frac{d[\rm{Live}]}{dt} &=& [\rm{birth\_rate}]\left(1-\frac{[\rm{Live}]}{[\rm{max\_density}]}\right)[\rm{Live}] - [\rm{death\_rate}][\rm{Live}]\\"
                          "\n"
                          r"\frac{d[\rm{Dead}]}{dt} &=& [\rm{death\_rate}][\rm{Live}] - [\rm{clearance\_rate}][\rm{Dead}]\\"
                          "\n"
                          r"\end{eqnarray}"
                          "\n",
    "total": r"\begin{eqnarray}"
             "\n"
             r"\frac{d[Total]}{dt} = [\rm{growth\_rate}][Total]"
             "\n"
             r"\end{eqnarray}"
             "\n",
    "total_ODE": r"\begin{eqnarray}"
                 "\n"
                 r"\frac{d[Total]}{dt} = [\rm{growth\_rate}][Total]"
                 "\n"
                 r"\end{eqnarray}"
                 "\n",
    "total_logistic": r"\begin{eqnarray}"
                      "\n"
                      r"\frac{d[Total]}{dt} = [\rm{growth\_rate}]\left(1-\frac{[Total]}{[\rm{max\_density}]}\right)[\rm{Total}]"
                      "\n"
                      r"\end{eqnarray}"
                      "\n",
    "gompertz": "[Insert equation]",
    "total_allee": "[Insert equation]",
    "logistic_gf": "[insert equation line 412]",
    "log_live_dead": "[insert equations]",
}

rate = {
    "growth_rate": "growth_phase",
    "birth_rate": "birth_phase",
    "carrying_capacity": "cell_cycle_arrest",
    "death_rate": "death_phase",
    "clearance_rate": "cell_death/duration",
    "seeding_cells": "seeding_cells",
    "seeding_cells_dead": "seeding_cells_dead",
    "seeding_cells_live": "seeding_cells_live",
    "minimum_population": "minimum_population",
}
phase = {
    "growth_rate": "growth_phase",
    "birth_rate": "birth_phase",
    "carrying_capacity": "cell_cycle_arrest",
    "death_rate": "death_phase",
    "clearance_rate": "cell_death/duration",
    "total": "growth_phase",
    "total_logistic": "growth_phase",
    "total_allee": "growth_phase",
    "live": "birth_phase",
    "live_logistic": "birth_phase",
    "live_dead": "birth_phase",
    "live_dead_logistic": "birth_phase",
    "seeding_cells": "seeding_cells",
    "gompertz": "",
    "seeding_cells_live": "seeding_cells_live",
    "seeding_cells_dead": "seeding_cells_dead",
    "minimum_population": "minimum_population",
    "logistic_gf": "",
    "log_live_dead": "Live dead model --in log space--",
}


def CustomPlot(function_name, y_init, time, params, result, units, target, sigmas, fig_counter, correlation_flag,
               stderr_flag, missing_errorbar_flag, color_list):


    if not os.path.exists(out_dir_root + "/output/" + function_name + "/"):
        os.makedirs(out_dir_root + "/output/" + function_name + "/")
    out_dir = out_dir_root + "/output/" + function_name + "/"

    src = "files/" + function_name + "/"
    src_files = os.listdir(src)
    for file_name in src_files:
        full_file_name = os.path.join(src, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy(full_file_name, out_dir)

    if template == "Total+Viability":
        color_dictionary = {
            "live": color_list[0],
            "live_logistic": color_list[0],
            "live_dead": [color_list[i] for i in [0, 1]],
            "live_dead_logistic": [color_list[i] for i in [0, 1]],
            "total": color_list[0],
            "total_logistic": color_list[0],
            "total_allee": color_list[0],
            "log_live_dead": [color_list[i] for i in [0, 1]],
        }
        markers_dictionary = {
            "live": ('^'),
            "live_logistic": ('^'),
            "live_dead": ('^', 'v'),
            "live_dead_logistic": ('^', 'v'),
            "total": ('D'),
            "total_logistic": ('d'),
            "total_allee": ('d'),
            "log_live_dead": ('^', 'v'),
        }

    elif template == "Total_Cells":
        color_dictionary = {
            "total": color_list[0],
            "total_logistic": color_list[0],
            "total_allee": color_list[0],
            "gompertz": color_list[0],
            "logistic_gf": color_list[0],
        }
        markers_dictionary = {
            "total": ('D'),
            "total_logistic": ('d'),
            "total_allee": ('d'),
            "gompertz": ('d'),
            "logistic_gf": ('d'),
        }

    # Simulation time is a vector of 100 evenly spaced values in the interval [experiment_initial_time,experiemnt_final_time]
    t_sim = np.array(np.linspace(np.min(time), np.max(time), num=100))

    # Integrate the ODE model in the interval defined above, using the parameters and initial conditions provided in the function call
    if function_name in ['live', 'total', 'gompertz', 'logistic_gf']:
        y = choose_model[function_name](params['seeding_cells'].value, t_sim, params)
    elif function_name in ['live_logistic', 'total_logistic','total_allee']:
        y = odeint(choose_model[function_name], params['seeding_cells'].value, t_sim, args=(params,))
    elif function_name in ['live_dead', 'live_dead_logistic']:
        y = odeint(choose_model[function_name],
                   [params['seeding_cells_live'].value, params['seeding_cells_dead'].value], t_sim, args=(params,))
    elif function_name in ['log_live_dead']:
        y = choose_model[function_name]([params['seeding_cells_live'].value, params['seeding_cells_dead'].value], t_sim, params)
    else:
        print('The model [:s] is not supported.'.format(function_name))

    lines = ("-", "--", "-.", ":")  # to be used in the B&W plots

    number_of_subplots = 1

    fig, axs = plt.subplots(1, number_of_subplots, sharex=True, sharey='none', squeeze=True)

    # Matplotlib handles figures with no subplots differently.

    if (function_name == "total") or (function_name == "total_logistic") or (function_name == 'total_allee'):
        axs.plot(t_sim, y[:, 0], color=color_list[0], label='Total cells (fitted)')
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt='.', label='Total cells')
        if show_replicates == True:
            print("Showing Replicates")
            for time_point in data.keys():
                if data[time_point]['Total']['single_bio_rep']:
                    for tech_rep in data[time_point]['Total']['bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                else:
                    # print('More than 1 Bio Rep')
                    for br_key in data[time_point].keys():

                        if not isinstance(br_key, str):

                            axs.plot(time_point * conversion_factor, data[time_point][br_key]['Total']['mean'], '.',
                                     color=color_list[0], alpha=0.5)

    else:
        axs.plot(t_sim, y[:, 0], color=color_list[0], label='Live cells (fitted)')
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt='.', label='Live cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point]['Live']['single_bio_rep']:
                    for tech_rep in data[time_point]['Live']['bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, str):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key]['Live']['mean'], '.',
                                     color=color_list[0], alpha=0.5)

    axs.set_ylabel("Number of Cells")

    if y.shape[1] == 2:
        axs.plot(t_sim, y[:, 1], color=color_list[1], label='Dead cells (fitted)')
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 1], yerr=sigmas[:, 1], color=color_list[1], fmt='.', label='Dead cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point]['Dead']['single_bio_rep']:
                    for tech_rep in data[time_point]['Dead']['bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, str):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key]['Dead']['mean'], '.',
                                     color=color_list[0], alpha=0.5)

    plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
    plt.xlabel("time (Hours)")

    lgd_1 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
    lgd_2 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

    title = plt.suptitle(title_dictionary[function_name], size=20.0, y=1.02)

    # Saving the figure in multiple formats
    for format in format_list:
        fig.savefig(out_dir + function_name + "." + format, dpi=75, format=format,
                    bbox_extra_artists=[lgd_1, lgd_2, title, ], bbox_inches='tight')

    if full_output:
        # Saving the figure data as a pickle file
        pickle.dump((t_sim, y,), open(out_dir + function_name + "_output.p", "wb"))

        # Make the same plots in log-linear space
        plt.gca().set_yscale('log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + function_name + "_loglinear." + format, dpi=75, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, title), bbox_inches='tight')

        plt.close(fig)

        # Now making plots in B&G
        fig, axs = plt.subplots(1, number_of_subplots, sharex=True, sharey='none', squeeze=True)
        plt.rc("axes", color_cycle="k")

        time_2 = np.array(np.linspace(np.min(time), np.max(time), num=len(time) * 2))
        if function_name in ['live', 'total', 'gompertz', 'logistic_gf']:
            y_2 = choose_model[function_name](params['seeding_cells'].value, time_2, params)
        elif function_name in ['live_logistic', 'total_logistic','total_allee']:
            y_2 = odeint(choose_model[function_name], params['seeding_cells'].value, time_2, args=(params,))
        elif function_name in ['live_dead', 'live_dead_logistic']:
            y_2 = odeint(choose_model[function_name],
                        [params['seeding_cells_live'].value, params['seeding_cells_dead'].value], time_2, args=(params,))
        elif function_name in ['log_live_dead']:
            y_2 = choose_model[function_name]([params['seeding_cells_live'].value, params['seeding_cells_dead'].value],
                                              time_2, params)
        else:
            print('The model {:s} is not supported.'.format(function_name))
        

        ROW = 0  # live/total cells
        if (function_name == "total") or (function_name == "total_logistic") or (function_name == 'total_allee'):
            axs.plot(time_2, y_2[:, ROW], linewidth=0.0, linestyle=lines[ROW],
                     marker=markers_dictionary[function_name][ROW], markerfacecolor='none', markeredgewidth=1.5,
                     label='Total cells (fitted)')
            axs.plot(t_sim, y[:, ROW], linewidth=1.5, linestyle=lines[ROW], marker=None)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label='Total cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Total']['single_bio_rep']:
                        for tech_rep in data[time_point]['Total']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Total']['mean'], '.',
                                         color='#000000', alpha=0.5)
        else:
            axs.plot(time_2, y_2[:, ROW], linewidth=0.0, linestyle=lines[ROW],
                     marker=markers_dictionary[function_name][ROW], markerfacecolor='none', markeredgewidth=1.5,
                     label='Live cells (fitted)')
            axs.plot(t_sim, y[:, ROW], linewidth=1.5, linestyle=lines[ROW], marker=None)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label='Live cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Live']['single_bio_rep']:
                        for tech_rep in data[time_point]['Live']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Live']['mean'], '.',
                                         color='#000000', alpha=0.5)

        axs.set_ylabel("Number of Cells")

        if y.shape[1] == 2:  # Dead cells
            ROW = 1
            axs.plot(time_2, y_2[:, ROW], linewidth=0.0, linestyle=lines[ROW],
                     marker=markers_dictionary[function_name][ROW], markerfacecolor='none', markeredgewidth=1.5,
                     label='Dead cells (fitted)')
            axs.plot(t_sim, y[:, ROW], linewidth=1.5, linestyle=lines[ROW], marker=None)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label='Dead cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Dead']['single_bio_rep']:
                        for tech_rep in data[time_point]['Dead']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Dead']['mean'], '.',
                                         color='#000000', alpha=0.5)

        plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
        plt.xlabel("time (Hours)")

        lgd_1 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        lgd_2 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

        title = plt.suptitle(title_dictionary[function_name], size=20.0, y=1.01)

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + function_name + "_BW." + format, dpi=75, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, title,), bbox_inches='tight')

        # Make the same plots in log-linear space
        plt.gca().set_yscale('log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + function_name + "_loglinear_BW." + format, dpi=75, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, title,), bbox_inches='tight')

    plt.close(fig)

    # Writing the figure captions to a text file
    dest_filename = out_dir + function_name + "_caption.txt"
    errors = MyError(params, function_name, time, y_init, target, sigmas)
    SSE = sum(errors ** 2)
    mape = MAPE(params, function_name, time, y_init, target, sigmas)
    f = open(dest_filename, "w")
    f.write(r"Shows the GoF of the " + name_dictionary[function_name] + ",MultiCellDS name: '" + function_name + ".' ")
    f.write("Sum of Squared Errors (SSE) is {:.3g}, ".format(SSE))
    f.write("the Mean Absolute Percentage Error (MAPE) is {:2.2f}%,".format(mape))
    if result.chisqr < 1e-16:
        f.write("and the Reduced Chi-Squared Goodness of Fit (GoF) is {:.3g}.".format(result.chisqr))
    else:
        f.write("and the Reduced Chi-Squared Goodness of Fit (GoF) is {:.3g}.".format(result.redchi))
    f.close()

    # Writing the figure titles to a separate text file
    dest_filename = out_dir + function_name + "_title.txt"
    f = open(dest_filename, "w")
    f.write(title_dictionary[function_name])
    f.close()

    # Writing the model names with correct LaTeX formatting
    dest_filename = out_dir + function_name + "_latex_title.txt"
    f = open(dest_filename, "w")
    f.write(name_dictionary[function_name].replace(" ", "\_"))
    f.close()

    # Writing the model description with correct LaTeX formatting
    dest_filename = out_dir + function_name + "_description.txt"
    f = open(dest_filename, "w")
    f.write(description_dictionary[function_name])
    f.close()


    # Now output the parameters as an excel file
    dest_filename = out_dir + function_name + "_parameters.xlsx"
    wb = Workbook()
    ws = wb.active
    ws.title = sheet_dictionary[function_name]
    ws.append([title_dictionary[function_name] + "Parameter Estimation Results"])
    ws.append([])
    headers = ["Parameter", "Value", "Std. Error", "units", "Inv_value"]
    temp_list = []
    for key, value in params.items():
        if value.value == 0:
            temp_list.append([key, value.value, value.stderr, units[key], "inf"])
        else:
            temp_list.append([key, value.value, value.stderr, units[key], 1 / value.value])
    ws.append(headers)
    for temp_row in temp_list:
        ws.append(temp_row)
    ws.append([])
    ws.append([])
    wb.save(filename=dest_filename)

    # Writing the standalone latex file which contains the parameter table
    dest_filename = out_dir + function_name + "_table.tex"
    f = open(dest_filename, "w")
    table = tabulate(temp_list, headers, tablefmt="latex", floatfmt=".3g")
    f.write(table)
    f.close()

    # Writing the file that contains the html code for the parameter table
    dest_filename = out_dir + function_name + "_table_html.txt"
    f = open(dest_filename, "w")
    table = tabulate(temp_list, headers, tablefmt="html", floatfmt=".3g", numalign="none")
    f.write(table)
    f.close()

    # Writing the standalone latex file which contains the model equations
    dest_filename = out_dir + function_name + "_model.tex"
    f = open(dest_filename, "w")
    f.write(model_dictionary[function_name])
    f.close()

    # Writing a csv file [to be used by LaTeX]
    dest_filename = out_dir + function_name + "_table.csv"
    f = open(dest_filename, "w")
    f.write("parameter,value,std. err.,units,invval\n")
    for key, value in params.items():
        f.write("{:s},".format(key.replace("_", "\_")))  # for LaTeX purposes
        f.write("{:.3g},".format(value.value))
        f.write("{:.3g},".format(value.stderr))
        f.write("{:s},".format(units[key]))
        if value.value == 0.0:
            f.write("{:s}\n".format("inf"))
        else:
            f.write("{:.3g}\n".format(1 / value.value))
    f.close()

    # Writing a txt file
    dest_filename = out_dir + function_name + "_table.txt"
    f = open(dest_filename, "w")
    f.write("parameter,value,std. err.,units,invval\n")
    for key, value in params.items():
        f.write("{:s},".format(key))
        f.write("{:.3g},".format(value.value))
        f.write("{:.3g},".format(value.stderr))
        f.write("{:s},".format(units[key]))
        if value.value == 0.0:
            f.write("{:s}\n".format("inf"))
        else:
            f.write("{:.3g}\n".format(1 / value.value))
    f.close()

    # Write the or append the xml file that contains the MCDS models:
    model_xml_dictionary = {
        "model_name": function_name,
        "model_number": str(fig_counter - 2),
        "param_name": "test",
        "param_number": 0.,
        "param_units": "test",
        "param_stderr": 0.,
        "param_value": 1.,

        "death_type": "Apoptosis",
        "death_units": "Hours",
        "death_stderr": 12.,
        "death_value": 24.,

        "clearance_units": "Hours",
        "clearance_stderr": 12.,
        "clearance_value": 24.,

        "arrest_units": "Squared_Microns",
        "arrest_stderr": 100.,
        "arrest_value": 1000.,
    }

    counter = 0
    dest_filename = out_dir + function_name + "_partial.xml"
    if "death_rate" in params.keys():
        f = open(dest_filename, "w")
        f.write(
            '\t\t\t\t<cell_cycle model="{0[model_name]:s}" ID="{0[model_number]:s}">\n'.format(model_xml_dictionary))
        # each cell cycle has the same 'background death rate'
        model_xml_dictionary["death_type"] = "Apoptosis"
        model_xml_dictionary["death_units"] = units["death_rate"]
        model_xml_dictionary["death_stderr"] = params["death_rate"].stderr
        model_xml_dictionary["death_value"] = params["death_rate"].value

        if "carrying_capacity" in params.keys():
            model_xml_dictionary["arrest_units"] = units["carrying_capacity"]
            model_xml_dictionary["arrest_stderr"] = params["carrying_capacity"].stderr
            model_xml_dictionary["arrest_value"] = params["carrying_capacity"].value
            for key, value in params.items():
                if key not in ["carrying_capacity", "death_rate","seeding_cells","seeding_cells_live","seeding_cells_dead"]:
                    if key == "clearance_rate":
                        # don't write to the DCL yet.
                        model_xml_dictionary["clearance_units"] = units[key]
                        model_xml_dictionary["clearance_stderr"] = value.stderr
                        model_xml_dictionary["clearance_value"] = value.value
                    else:
                        model_xml_dictionary["param_name"] = phase[key]
                        model_xml_dictionary["param_number"] = counter
                        model_xml_dictionary["param_units"] = units[key]
                        model_xml_dictionary["param_stderr"] = value.stderr
                        model_xml_dictionary["param_value"] = value.value
                        if value.value == 0:
                            model_xml_dictionary["param_inv_value"] = float("inf")
                        else:
                            model_xml_dictionary["param_inv_value"] = 1 / value.value
                        f.write('\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'
                                '\t\t\t\t\t\t<birth_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                '\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                '\t\t\t\t\t\t</birth_rate>\n'
                                '\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                                '\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                                '\t\t\t\t\t\t</duration>\n'
                                '\t\t\t\t\t\t<death_rate units="{0[death_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[death_stderr]:1.3g}">\n'
                                '\t\t\t\t\t\t{0[death_value]:1.3g}\n'
                                '\t\t\t\t\t\t</death_rate>\n'
                                '\t\t\t\t\t\t<cell_cycle_arrest>\n'
                                '\t\t\t\t\t\t\t<condition type="maximum_cell_density" units="{0[arrest_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[arrest_stderr]:1.3g}">\n'
                                '\t\t\t\t\t\t\t{0[arrest_value]:1.3g}\n'
                                '\t\t\t\t\t\t\t</condition>\n'
                                '\t\t\t\t\t\t</cell_cycle_arrest>\n'
                                '\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                        counter += 1
        else:  # No cell arrest
            for key, value in params.items():
                if key == "clearance_rate":
                    # don't write to the DCL yet.
                    model_xml_dictionary["clearance_units"] = units[key]
                    model_xml_dictionary["clearance_stderr"] = value.stderr
                    model_xml_dictionary["clearance_value"] = value.value

                else:
                    if key not in ["carrying_capacity", "death_rate","seeding_cells","seeding_cells_live","seeding_cells_dead"]:
                        model_xml_dictionary["param_name"] = phase[key]
                        model_xml_dictionary["param_number"] = counter
                        model_xml_dictionary["param_units"] = units[key]
                        model_xml_dictionary["param_stderr"] = value.stderr
                        model_xml_dictionary["param_value"] = value.value
                        if value.value == 0:
                            model_xml_dictionary["param_inv_value"] = float("inf")
                        else:
                            model_xml_dictionary["param_inv_value"] = 1 / value.value
                        f.write('\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'
                                '\t\t\t\t\t\t<birth_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                '\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                '\t\t\t\t\t\t</birth_rate>\n'
                                '\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                                '\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                                '\t\t\t\t\t\t</duration>\n'
                                '\t\t\t\t\t\t<death_rate units="{0[death_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[death_stderr]:1.3g}">\n'
                                '\t\t\t\t\t\t{0[death_value]:1.3g}\n'
                                '\t\t\t\t\t\t</death_rate>\n'
                                '\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                        counter += 1

        f.write('\t\t\t\t</cell_cycle>\n')
        f.write('\t\t\t\t<cell_death type="{0[death_type]:s}">\n'
                '\t\t\t\t\t<duration units="{0[clearance_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[clearance_stderr]:1.3g}">\n'
                '\t\t\t\t\t{0[clearance_value]:1.3g}\n'
                '\t\t\t\t\t</duration>\n'
                '\t\t\t\t</cell_death>'.format(model_xml_dictionary))
        f.close()
    else:  # no cell death
        f = open(dest_filename, "w")
        f.write(
            '\t\t\t\t<cell_cycle model="{0[model_name]:s}" ID="{0[model_number]:s}">\n'.format(model_xml_dictionary))

        if "carrying_capacity" in params.keys():
            model_xml_dictionary["arrest_units"] = units["carrying_capacity"]
            model_xml_dictionary["arrest_stderr"] = params["carrying_capacity"].stderr
            model_xml_dictionary["arrest_value"] = params["carrying_capacity"].value
            for key, value in params.items():
                if key not in ["carrying_capacity", "death_rate","seeding_cells","seeding_cells_live","seeding_cells_dead"]:
                    model_xml_dictionary["param_name"] = phase[key]
                    model_xml_dictionary["param_number"] = counter
                    model_xml_dictionary["param_units"] = units[key]
                    model_xml_dictionary["param_stderr"] = value.stderr
                    model_xml_dictionary["param_value"] = value.value
                    if value.value == 0:
                        model_xml_dictionary["param_inv_value"] = float("inf")
                    else:
                        model_xml_dictionary["param_inv_value"] = 1 / value.value
                    f.write('\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'.format(
                        model_xml_dictionary))
                    if function_name in ['live']:
                        f.write(
                            '\t\t\t\t\t\t<birt_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                            '\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                            '\t\t\t\t\t\t</birt_rate>\n'.format(model_xml_dictionary))
                    else:
                        f.write(
                            '\t\t\t\t\t\t<net_birth_rate  units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                            '\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                            '\t\t\t\t\t\t</net_birth_rate >\n'.format(model_xml_dictionary))
                    f.write('\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                            '\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                            '\t\t\t\t\t\t</duration>\n'
                            '\t\t\t\t\t\t<cell_cycle_arrest>\n'
                            '\t\t\t\t\t\t\t<condition type="maximum_cell_density" units="{0[arrest_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[arrest_stderr]:1.3g}">\n'
                            '\t\t\t\t\t\t\t{0[arrest_value]:1.3g}\n'
                            '\t\t\t\t\t\t\t</condition>\n'
                            '\t\t\t\t\t\t</cell_cycle_arrest>\n'
                            '\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                    counter += 1
        else:  # No cell arrest
            for key, value in params.items():
                if key not in ["carrying_capacity", "death_rate","seeding_cells","seeding_cells_live","seeding_cells_dead"]:
                    if key == "clearance_rate":
                        # don't write to the DCL yet.
                        model_xml_dictionary["clearance_units"] = units[key]
                        model_xml_dictionary["clearance_stderr"] = value.stderr
                        model_xml_dictionary["clearance_value"] = value.value
                    else:
                        model_xml_dictionary["param_name"] = phase[key]
                        model_xml_dictionary["param_number"] = counter
                        model_xml_dictionary["param_units"] = units[key]
                        model_xml_dictionary["param_stderr"] = value.stderr
                        model_xml_dictionary["param_value"] = value.value
                        if value.value == 0:
                            model_xml_dictionary["param_inv_value"] = float("inf")
                        else:
                            model_xml_dictionary["param_inv_value"] = 1 / value.value
                        f.write('\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'.format(
                            model_xml_dictionary))
                        if function_name in ['live']:
                            f.write(
                                '\t\t\t\t\t\t<birt_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                '\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                '\t\t\t\t\t\t</birt_rate>\n'.format(model_xml_dictionary))
                        else:
                            f.write(
                                '\t\t\t\t\t\t<net_birth_rate  units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                '\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                '\t\t\t\t\t\t</net_birth_rate >\n'.format(model_xml_dictionary))
                        f.write('\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                                '\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                                '\t\t\t\t\t\t</duration>\n'
                                '\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                        counter += 1
        f.write('\t\t\t\t</cell_cycle>')
        f.close()

    # Writing the table captions to a text file
    dest_filename = out_dir + function_name + "_table_caption.txt"
    f = open(dest_filename, "w")
    f.write(r"shows the list of parameters that achieve minimal Sum of Squared Errors (SSE) for the " + name_dictionary[
        function_name] +
            ", MultiCellDS name: '" + function_name + ".' ")
    f.write("Sum of Squared Errors is {:.3g}".format(SSE) + ", Mean Absolute Percentage Error is {:2.2f}".format(
        mape) + "%,")
    if result.chisqr < 1e-16:
        f.write("and the Reduced Chi Squared Goodness of Fit is {:.3g}.".format(result.chisqr))
    else:
        f.write("and the Reduced Chi Squared Goodness of Fit is {:.3g}.".format(result.redchi))
    if correlation_flag:
        f.write(
            "</p><p><b>Warning:</b> Parameters 'death rate' and 'clearance rate' had a correlation larger than 0.9 \n"
            "so they could not be estimated reliably at the same time, this also means the covariance \n"
            "matrix was singular, so no confidence intervals can be estimated. To address that problem \n"
            "'clearance rate' was fixed to 0 and the rest of the parameters were re-estimated. \n"
            "You may change this behavior if you want.")
    if missing_errorbar_flag:
        f.write(
            "</p><p><b>Warning:</b> The parameter estimate standard errors could not be estimated. This model may not be appropiate.\n")
    if stderr_flag:
        if SSE < 1e-16:
            stderr_flag = False
            f.write(
                "</p><p><b>Note:</b>Parameter estimate standard errors may have not been estimated correctly because the SSE is too small ({:.3g})\n".format(
                    SSE))
        else:
            f.write(
                "</p><p><b>Warning:</b> At least one parameter estimate standard error is of the same order of magnitude as its corresponding parameter\n."
                "This model may not be appropiate.\n")



    if (len(target[:, 0])/max(time) > 2): # More than 2 samples per hour
        print("*********************************")
        x = np.log(target[:, 0])
        first_diff = np.diff(x, n=1, axis=0)
        time_diff = np.diff(time, n=1, axis=0)
        # print("Max growth rate")
        # print("This is used by cellGrowth:Max growth rate with 3 hours of smoothing",max(running_mean(first_diff/time_diff,round(3/np.mean(time_diff)))))
    # if True:
        f.write("</p><p><b>Note:</b> High sampling rate was detected ({:d} samples in {:2.2f} hours). You may consider using this maximum growth rate instead:\n"
                "<b>max_growth_rate</b>={:2.3g} (computed using a 3 hour window smoothing)".format(len(target[:, 0]),max(time),max(running_mean(first_diff/time_diff,round(3/np.mean(time_diff))))))
        # print('window:',round(3/np.mean(time_diff)),'dt=',np.mean(time_diff))
        # print("</p><p><b>Note:</b> High sampling rate was detected ({:d} samples in {:2.2f} hours). You may consider using this maximum growth rate instead:\n"
        #         "<b>max_growth_rate</b>={:2.3g} (computed using a 3 hour window smoothing)".format(len(target[:, 0]),max(time),max(running_mean(first_diff/time_diff,round(3/np.mean(time_diff))))))
    f.close()
    #max(first_diff/time_diff)

    warning = ''
    if stderr_flag:
        if correlation_flag:
            if missing_errorbar_flag:
                warning = '(u+c+e)'
            else:
                warning = '(u+c)'
        else:
            if missing_errorbar_flag:
                warning = '(u+e)'
            else:
                warning = '(u)'
    else:
        if correlation_flag:
            if missing_errorbar_flag:
                warning = '(c+e)'
            else:
                warning = '(c)'
        else:
            if missing_errorbar_flag:
                warning = '(e)'
            else:
                warning = ''

    if result.chisqr < 1e-16:
        models_chi.append(('<a href="' + "./output/" + function_name + "/" + function_name + '_report.html">' +
                           title_dictionary[function_name] + "</a>", "{:2.3f}".format(result.chisqr), warning))
    else:
        models_chi.append(('<a href="' + "./output/" + function_name + "/" + function_name + '_report.html">' +
                           title_dictionary[function_name] + "</a>", "{:2.3f}".format(result.redchi), warning))

    models_mape.append(('<a href="' + "./output/" + function_name + "/" + function_name + '_report.html">' +
                        title_dictionary[function_name] + "</a>", "{:.2f}%".format(mape), warning))

    if result.chisqr < 1e-16:
        models_chi_table.append([title_dictionary[function_name], result.chisqr, warning,function_name])
    else:
        models_chi_table.append([title_dictionary[function_name], result.redchi, warning,function_name])
    models_mape_table.append([title_dictionary[function_name], mape, warning,function_name ])

    # Writing the report for this model
    f = open(out_dir + function_name + "_report.html", "w")

    number_of_equations = {
        "live": "1",
        "live_logistic": "1",
        "live_dead": "2",
        "live_dead_logistic": "2",
        "total": "1",
        "total_ODE": "1",
        "total_logistic": "1",
        "total_allee": "1",
        "gompertz": "1",
        "logistic_gf": "1",
        "log_live_dead": "2",
    }

    report_template = open("files/report_template.html").read()

    # Drop all the points where the first diff is negative and/or the second diff is positive
    x = np.log(target[:, 0])
    first_diff = np.diff(x, n=1, axis=0)
    second_diff = np.diff(first_diff, n=1, axis=0)
    (a,) = (first_diff >= 0).ravel().nonzero()
    (b,) = (second_diff <= 0).ravel().nonzero()


    if len(a) == 0:
        print("These data does not behave like exponential growth, cells do not even increase in value.")
        a = [0]
    if len(b) == 0:
        print(
            "These data does not behave like exponential growth, cells appear to be growing close to logistic limits from the beggining of experiment.")
        b = [0]
    first_index = max(a[0], b[0])

    # report cell doubling times
    if "growth_rate" in params.keys():
        exponential_doubling_time = np.log(2) / params["growth_rate"]
        naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
        naive_doubling_time = np.log(2) / naive_growth_rate
        growth_rate = np.log(target[-1, 0] / target[first_index, 0]) / ((time[-1] - time[first_index]))
        doubling_time = np.log(2) / growth_rate
    elif "birth_rate" in params.keys():
        exponential_doubling_time = np.log(2) / (params["birth_rate"] - params["death_rate"])
        naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
        naive_doubling_time = np.log(2) / naive_growth_rate
        growth_rate = np.log(target[-1, 0] / target[first_index, 0]) / ((time[-1] - time[first_index]))
        doubling_time = np.log(2) / growth_rate
    else:
        if "death_rate" in params.keys():
            # net_cell_cycle_length = 1/(params["r_G0G1"]-params["death_rate"])+1/(params["r_S"]-params["death_rate"])+1/(params["r_G2M"]-params["death_rate"])
            net_cell_cycle_length = 1 / (params["r_G2M"] * np.mean((y[:, 4] / y[:, 0])) - params["death_rate"])
            exponential_doubling_time = np.log(2) * (net_cell_cycle_length)
            naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
            naive_doubling_time = np.log(2) / naive_growth_rate
            growth_rate = np.log(target[-1, 0] / target[first_index, 0]) / ((time[-1] - time[first_index]))
            doubling_time = np.log(2) / growth_rate
        else:
            # net_cell_cycle_length = 1/params["r_G0G1"]+1/params["r_S"]+1/params["r_G2M"]
            net_cell_cycle_length = 1 / (params["r_G2M"] * np.mean((y[:, 3] / y[:, 0])))
            exponential_doubling_time = np.log(2) * net_cell_cycle_length
            naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
            naive_doubling_time = np.log(2) / naive_growth_rate
            # growth_rate = np.log(target[-1,0]/target[first_index,0])/((time[-1]-time[first_index]))
            growth_rate = np.log(target[-1, 0] / target[1, 0]) / ((time[-1] - time[1]))
            doubling_time = np.log(2) / growth_rate

    if len(target) <= first_index +1:
        first_growth_rate = np.log(target[1,0]/target[0,0])/((time[1]-time[0]))
    else:
        first_growth_rate = np.log(target[first_index + 1, 0] / target[first_index, 0]) / (
        (time[first_index + 1] - time[first_index]))
        if len(target) <= first_index+2:
            second_growth_rate = np.log(target[first_index + 1, 0] / target[first_index, 0]) / (
            (time[first_index + 1] - time[first_index]))
        else:
            second_growth_rate = np.log(target[first_index + 2, 0] / target[first_index + 1, 0]) / (
            (time[first_index + 2] - time[first_index + 1]))
    first_doubling_time = np.log(2) / first_growth_rate
    second_doubling_time = np.log(2) / second_growth_rate
    first_naive_growth_rate = np.log(target[1, 0] / target[0, 0]) / ((time[1] - time[0]))
    first_naive_doubling_time = np.log(2) / first_naive_growth_rate
    temp = y[:, 0] / y[0, 0]
    (c,) = (temp >= 2).ravel().nonzero()

    # print("*********************************")
    # x = np.log(target[:, 0])
    # first_diff = np.diff(x, n=1, axis=0)
    # time_diff = np.diff(time, n=1, axis=0)
    # second_diff = np.diff(first_diff, n=1, axis=0)
    # print("Max growth rate")
    # print("Max growth rate with {:2.2f} hours of smoothing (~1 doubling)".format(exponential_doubling_time),max(running_mean(first_diff/time_diff,exponential_doubling_time)))
    # print("Max growth rate with {:2.2f} hours of smoothing (~2 doublings)".format(2*exponential_doubling_time),max(running_mean(first_diff/time_diff,2*exponential_doubling_time)))
    # print("Max growth rate with {:2.2f} hours of smoothing (~3 doublings)".format(3*exponential_doubling_time),max(running_mean(first_diff/time_diff,3*exponential_doubling_time)))
    # print("*********************************")

    if c.shape[0] == 0:
        first_simulation_doubling_time = float("inf")
    else:
        first_simulation_doubling_time = t_sim[c[0]] - t_sim[0]

    # Write this into an html document:
    report_dictionary = {
        "tool_name": TOOL_NAME,
        "version": VERSION,
        "model_name": title_dictionary[function_name],
        "MCDS_name": function_name,
        "model_equations": "./" + function_name + "_plane",
        "number_of_equations": number_of_equations[function_name],
        "citation_text": open("files/citation.txt").read(),
        "model_description": open("files/" + function_name + "/" + function_name + "_description.txt").read(),
        "parameters_table": table,
        "table_caption": open(out_dir + function_name + "_table_caption.txt").read(),
        "model_caption": open(out_dir + function_name + "_caption.txt").read(),
        "exponential_doubling_time": exponential_doubling_time,
        "naive_doubling_time": naive_doubling_time,
        "doubling_time": doubling_time,
        "first_doubling_time": first_doubling_time,
        "second_doubling_time": second_doubling_time,
        "first_naive_growth_rate": first_naive_doubling_time,
        "first_simulation_doubling_time": first_simulation_doubling_time,
        "cell_line_name": str(metadata_dictionary["cell_line"]["name"]),
    }

    f.write(report_template.format(report_dictionary))
    f.close()

    return


def PlotData(time, target, sigmas, function_name, fig_counter, color_list):
    markers_dictionary = {
        "live": (':^'),
        "live_logistic": (':^'),
        "live_dead": (':^', ':v'),
        "live_dead_logistic": (':^', ':v'),
        "total": (':D'),
        "total_logistic": (':d'),
        "total_allee": (':d'),
    }

    if not os.path.exists(out_dir_root + "/output/data/"):
        os.makedirs(out_dir_root + "/output/data/")
    out_dir = out_dir_root + "/output/data/"
    lines = ("-", "--", "-.", ":")

    number_of_subplots = 1

    fig, axs = plt.subplots(1, number_of_subplots, sharex=True)

    if (function_name == "total") or (function_name == "total_logistic") or (function_name == 'total_allee'):
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point]['Total']['single_bio_rep']:
                    for tech_rep in data[time_point]['Total']['bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, str):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key]['Total']['mean'], '.',
                                     color=color_list[0], alpha=0.5)
        if True:  # not all_single_bio_rep_flag:
            # axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.', label='Total cells')
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt='.', label='Total cells')
    else:
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.', label='Live cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point]['Live']['single_bio_rep']:
                    for tech_rep in data[time_point]['Live']['bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, str):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key]['Live']['mean'], '.',
                                     color=color_list[0], alpha=0.5)

    axs.set_ylabel("Number of Cells")

    if target.shape[1] == 2:
        axs.errorbar(time, target[:, 1], yerr=sigmas[:, 1], color=color_list[1], fmt=':.', label='Dead cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point]['Dead']['single_bio_rep']:
                    for tech_rep in data[time_point]['Dead']['bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, str):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key]['Dead']['mean'], '.',
                                     color=color_list[0], alpha=0.5)

    plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
    plt.xlabel("time (Hours)")

    lgd_1 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
    lgd_2 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

    suptitle = plt.suptitle("Data provided", size=24, y=1.01)

    # Saving the figure in multiple formats
    for format in format_list:
        fig.savefig(out_dir + "data." + format, dpi=75, format=format, bbox_extra_artists=(lgd_1, lgd_2, suptitle,),
                    bbox_inches='tight')

    if full_output:
        # Saving the figure data as a pickle file
        pickle.dump((time, target, sigmas,), open(out_dir + "Data.p", "wb"))

        # Make the same plots in log-linear space
        plt.gca().set_yscale('log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + "data_loglinear." + format, dpi=75, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, suptitle,), bbox_inches='tight')

        # toc()

        plt.close(fig)

        # Now making plots in B&G
        plt.rc("axes", color_cycle="k")
        fig, axs = plt.subplots(number_of_subplots, sharex=True)

        ROW = 0  # live/total cells
        if (function_name == "total") or (function_name == "total_logistic") or (function_name == "total_allee"):
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label='Total cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Total']['single_bio_rep']:
                        for tech_rep in data[time_point]['Total']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Total']['mean'], '.',
                                         color='#000000', alpha=0.5)
        else:
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label='Live cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Live']['single_bio_rep']:
                        for tech_rep in data[time_point]['Live']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Live']['mean'], '.',
                                         color='#000000', alpha=0.5)

        axs.set_ylabel("Number of Cells")

        if target.shape[1] == 2:  # Dead cells
            ROW = 1
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label='Dead cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Dead']['single_bio_rep']:
                        for tech_rep in data[time_point]['Dead']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Dead']['mean'], '.',
                                         color='#000000', alpha=0.5)

        plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
        plt.xlabel("time (Hours)")

        lgd_1 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        lgd_2 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

        plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
        plt.xlabel("time (Hours)")

        suptitle = plt.suptitle("Data provided", size=24, y=1.01)

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + "data_BW." + format, dpi=75, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, suptitle,), bbox_inches='tight')

        # Make the same plots in log-linear space
        plt.gca().set_yscale('log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + "data_loglinear_BW." + format, dpi=75, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, suptitle,), bbox_inches='tight')

    plt.close(fig)

    return


def CustomTable(function_name, params, report):
    # This function creates a xlsx table of the parameters

    headers = ["Parameter", "Value", "Std. Error"]
    rows = []
    for key, value in params.items():
        rows.append([key, value.value, value.stderr])

    wb = Workbook()
    dest_filename = 'Parameter_Estimates.xlsx'

    ws1 = wb.active
    ws1.title = "Parameters"

    ws1.append(headers)

    for row in range(2, len(rows)):
        ws1.append(rows[row])

    ws2 = wb.create_sheet(title="LaTeX")
    ws2.append(["You can also create a table in Latex using this code:"])
    ws2.append([tabulate(rows, headers, tablefmt="latex")])

    wb.save(filename=dest_filename)

    return headers, rows


def CustomOutput(function_name, params):
    # This function creates a XLSX file with a custom output (including parameter estimates)

    sheet_dictionary = {
        "live": "Live",
        "live_logistic": "Live_logistic",
        "live_dead": "Live+Dead",
        "live_dead_logistic": "Live+Dead_logistic",
        "total": "Total_Cells",
        "total_logistic": "Total_logistic",
        "total_allee": "Total_logistic",
    }

    dest_filename = 'output.xlsx'
    wb = load_workbook(filename=dest_filename)

    ws1 = wb.create_sheet(title=sheet_dictionary[function_name])

    ws1.append([function_name])
    ws1.append([])

    headers = ["Parameter", "Value", "Std. Error"]
    temp_list = []
    for key, value in params.items():
        temp_list.append([key, value.value, value.stderr])

    ws1.append(headers)

    for i in range(0, len(temp_list)):
        ws1.append(temp_list[i])

    ws1.append([])
    ws1.append([])

    ws1.append(["You can also create a table in Latex using this code:"])
    ws1.append([tabulate(temp_list, headers, tablefmt="latex")])

    wb.save(filename=dest_filename)

    return


def CustomEstimation(init_params, units, function_name, time, fig_counter, means, stds, sem, color_list):
    if template == "Total+Viability":
        # y_init_dictionary ={
        #     "L": means[0,[1]],
        #     "T": means[0,[0]],
        #     "LD": means[0,[1,2]],
        #     "live" : means[0,[1]],
        #     "live_logistic" : means[0,[1]],
        #     "live_dead" : means[0,[1,2]],
        #     "live_dead_logistic" : means[0,[1,2]],
        #     "total" : means[0,[0]],
        #     "total_ODE" : means[0,[0]],
        #     "total_logistic" : means[0,[0]],
        #     "log_live_dead" : means[0,[1,2]],
        # }
        target_dictionary = {
            "L": means[:, [1]],
            "T": means[:, [0]],
            "LD": means[:, [1, 2]],
            "live": means[:, [1]],
            "live_logistic": means[:, [1]],
            "live_dead": means[:, [1, 2]],
            "live_dead_logistic": means[:, [1, 2]],
            "total": means[:, [0]],
            "total_ODE": means[:, [0]],
            "total_logistic": means[:, [0]],
            "total_allee": means[:, [0]],
            "log_live_dead": means[:, [1, 2]],
        }
        sigmas_dictionary = {
            "L": stds[:, [1]],
            "T": stds[:, [0]],
            "LD": stds[:, [1, 2]],
            "live": stds[:, [1]],
            "live_logistic": stds[:, [1]],
            "live_dead": stds[:, [1, 2]],
            "live_dead_logistic": stds[:, [1, 2]],
            "total": stds[:, [0]],
            "total_ODE": stds[:, [0]],
            "total_logistic": stds[:, [0]],
            "total_allee": stds[:, [0]],
            "log_live_dead": stds[:, [1, 2]],
        }
        std_error_dictionary = {
            "L": sem[:, [1]],
            "T": sem[:, [0]],
            "LD": sem[:, [1, 2]],
            "live": sem[:, [1]],
            "live_logistic": sem[:, [1]],
            "live_dead": sem[:, [1, 2]],
            "live_dead_logistic": sem[:, [1, 2]],
            "total": sem[:, [0]],
            "total_ODE": sem[:, [0]],
            "total_logistic": sem[:, [0]],
            "total_allee": sem[:, [0]],
            "log_live_dead": sem[:, [1, 2]],
        }
    elif template == "Total_Cells":
        # y_init_dictionary ={
        #     "T": means[0,[0]],
        #     "total" : means[0,[0]],
        #     "gompertz" : means[0,[0]],
        #     "total_logistic" : means[0,[0]],
        # }
        target_dictionary = {
            "T": means[:, [0]],
            "total": means[:, [0]],
            "gompertz": means[:, [0]],
            "total_logistic": means[:, [0]],
            "total_allee": means[:, [0]],
            "logistic_gf": means[:, [0]],
        }
        sigmas_dictionary = {
            "T": stds[:, [0]],
            "total": stds[:, [0]],
            "gompertz": stds[:, [0]],
            "total_logistic": stds[:, [0]],
            "total_allee": stds[:, [0]],
            "logistic_gf": stds[:, [0]],
        }
        std_error_dictionary = {
            "T": sem[:, [0]],
            "total": sem[:, [0]],
            "gompertz": sem[:, [0]],
            "total_logistic": sem[:, [0]],
            "total_allee": sem[:, [0]],
            "logistic_gf": sem[:, [0]],
        }

    correlation_flag = False  # this will be set to True if something goes wrong
    missing_errorbar_flag = False
    # Make a copy of the parameters
    params = copy.deepcopy(init_params)

    # Do fit, here with leastsq model (Levenberg-Marquardt method)

    # Define population initial values
    target = target_dictionary[function_name]
    sigmas = std_error_dictionary[function_name]

    # sigmas = sigmas_dictionary[function_name] # Using standard deviation instead of standard error
    # sigmas = np.power(sigmas,2) # Using variance instead of standard deviation
    y_init = y_init_dictionary[function_name]

    # Invoke the minimizer
    result = minimize(MyError, params, args=(function_name, time, y_init, target, sigmas), method='leastsq')

    global uncertainty_note

    # write error report
    if (result.errorbars == False):
        correls = {}
        min_correl = 0.9
        for i, name in enumerate(result.var_map):
            par = result.params[name]

            if not par.vary:
                continue
            if hasattr(par, 'correl') and par.correl is not None:
                for name2 in result.var_map[i + 1:]:
                    if (name != name2 and name2 in par.correl and
                                abs(par.correl[name2]) > min_correl):
                        correls["%s, %s" % (name, name2)] = par.correl[name2]
        if any(correls):
            correlation_flag = True
        else:
            missing_errorbar_flag = True
            print(
                'We could not estimate the standard error of the parameter estimates. This model [{:s}] may not be the correct one to use'.format(function_name))
            uncertainty_note += '\nThe model [{:s}] may not be the correct one to use because the standard error of the parameter estimates could not be estimated.\n'.format(function_name)

    # else:
    print('The estimated parameters parameters are:')
    report_fit(result.params, show_correl=False)
    print('--------------------')
    # print("The initial error was\t%".format(MAPE(init_params,function_name,time,y_init,target,sigmas)))
    final_error = MAPE(params, function_name, time, y_init, target, sigmas)
    init_error = MAPE(init_params, function_name, time, y_init, target, sigmas)

    print("The final Mean Absolute Percentage Error (MAPE) is {:2.2f}%".format(final_error),
          '(init= {:2.2f}%)'.format(init_error))

    if init_error < final_error:
        print(
            '\tNote that the algorithm minimizes the Sum of Squared Errors (SSE), this sometimes leads to an increase in MAPE')
        print(
            "\tThe final SSE is {:2.2g}".format(sum(MyError(params, function_name, time, y_init, target, sigmas) ** 2)),
            '(init= {:2.2g})'.format(sum(MyError(init_params, function_name, time, y_init, target, sigmas)) ** 2))

    # Check if any parameter has a standard error of the same order of magnitude the value of the parameter, ignore parameter values == 0.0
    stderr_flag = False
    for key in params:
        if params[key].value != 0.0:
            if np.log10(params[key].stderr / params[key].value) >= 0:
                stderr_flag = True
                uncertainty_note = uncertainty_note + \
                                   '//cell_cycle[@model="' + function_name + '"]/cell_cycle_phase[@name="' + phase[
                                       function_name] + '"]/' + rate[key]

    # plot results
    CustomPlot(function_name=function_name, y_init=y_init, time=time, params=params, result=result, units=units,
               target=target, sigmas=std_error_dictionary[function_name], fig_counter=fig_counter,
               correlation_flag=correlation_flag, stderr_flag=stderr_flag, color_list=color_list,
               missing_errorbar_flag=missing_errorbar_flag)

    fit_summary = {}
    fit_summary['function_name'] = function_name
    fit_summary['y_init'] = y_init
    fit_summary['parameters'] = params
    fit_summary['result'] = result
    return fit_summary


def ReadMetadata(ws):
    user = {
        "orcid": ws.cell("D2").value,
        "given_names": ws.cell("D3").value,
        "family_name": ws.cell("D4").value,
        "email": ws.cell("D5").value,
        "website": ws.cell("D6").value,
        "organization": ws.cell("D7").value,
        "department": ws.cell("D8").value,
        "list": [
            ["Given names", ws.cell("D3").value, ],
            ["Family name", ws.cell("D4").value, ],
            ["Email", ws.cell("D5").value, ],
            ["Website", ws.cell("D6").value, ],
            ["Organization", ws.cell("D7").value, ],
            ["Department", ws.cell("D8").value, ],
            ["ORCID", ws.cell("D2").value, ],
        ],
    }
    cell_line = {
        "link": ws.cell("D9").value,
        "citation": ws.cell("D10").value,
        "MultiCellDB": ws.cell("D11").value,
        "name": ws.cell("D12").value,
        "synonyms": ws.cell("D13").value,
        "origins": ws.cell("D14").value,
        "description": ws.cell("D15").value,
        "CLO": ws.cell("D16").value,
        "BTO": ws.cell("D17").value,
        "organism": ws.cell("D18").value,
        "organ": ws.cell("D19").value,
        "disease": ws.cell("D20").value,
        "morphology": ws.cell("D21").value,
        "oxygenation_name": ws.cell("D22").value,
        "oxygenation_level": ws.cell("D23").value,
        "oxygenation_measurement_type": ws.cell("D24").value,
        "time": TIME_STAMP,
        "caption": ws.cell("D26").value,
        "list": [
            ["Link to data", ws.cell("D9").value, ],
            ["Citation information", ws.cell("D10").value, ],
            ["MultiCellDB ID", ws.cell("D11").value, ],
            ["Cell line name", ws.cell("D12").value, ],
            ["Synonyms", ws.cell("D13").value, ],
            ["Cell line origins", ws.cell("D14").value, ],
            ["Brief description", ws.cell("D15").value, ],
            ["CLO ID", ws.cell("D16").value, ],
            ["BTO ID", ws.cell("D17").value, ],
            ["Organism", ws.cell("D18").value, ],
            ["Organ", ws.cell("D19").value, ],
            ["Disease", ws.cell("D20").value, ],
            ["Morphology", ws.cell("D21").value, ],
            ["oxygenation level name", ws.cell("D22").value, ],
            ["oxygenation level", ws.cell("D23").value, ],
            ["oxygenation Measurement Type", ws.cell("D24").value, ],
            ["Time of creation", TIME_STAMP, ],
        ]
    }
    metadata_dictionary = {
        "user": user,
        "cell_line": cell_line,
    }

    return metadata_dictionary


def ApproximateColor(cell_theme):
    if cell_theme == 0:
        print("White was set to #FFFFFF.")
        hex_val = "#FFFFFF"
    elif cell_theme == 1:
        print("Black was set to #000000.")
        hex_val = "#000000"
    elif cell_theme == 2:
        print("Light Gray was set to #E7E6E6.")
        hex_val = "#E7E6E6"
    elif cell_theme == 3:
        print("Dark Gray was set to #44546A.")
        hex_val = "#44546A"
    elif cell_theme == 4:
        print("Sky Blue was set to #5B9BD5.")
        hex_val = "#5B9BD5"
    elif cell_theme == 5:
        print("Pumpkin Orange was set to #ED7D31.")
        hex_val = "#ED7D31"
    elif cell_theme == 6:
        print("Gray was set to #A5A5A5.")
        hex_val = "#A5A5A5"
    elif cell_theme == 7:
        print("Yellow was set to #FFC000.")
        hex_val = "#FFC000"
    elif cell_theme == 8:
        print("Light Blue was set to #4472C4.")
        hex_val = "#4472C4"
    elif cell_theme == 9:
        print("Green was set to #70AD47.")
        hex_val = "#70AD47"
    else:
        print("This color is not recognized, using Aggie Maroon instead #500000.")
        hex_val = "#500000"
    return hex_val


def GetCellColor(cell):
    if cell.fill.start_color.type == "theme":
        cell_color = ApproximateColor(cell.fill.start_color.theme)
        theme_flag = True
    else:
        cell_color = '#' + cell.fill.start_color.rgb[2:8]
        theme_flag = False
    return cell_color, theme_flag


def ReadColors(wb):
    ws = wb.get_sheet_by_name("Preferences")
    custom_colors_flag = ws.cell("B1").value
    live_color = "#FFFFFF"  # Setting it to white to initialize the variable!
    dead_color = "#FFFFFF"  # Setting it to white to initialize the variable!
    complete_color_list = [live_color, dead_color, ]
    custom_color_list = []
    theme_flag = False

    if custom_colors_flag == "Use default colors":
        print("Using default colors.")
        live_color = "#0000ff"
        dead_color = "#500000"
        complete_color_list = [live_color, dead_color, ]
    elif custom_colors_flag == "Use old default colors":
        print("Using old default colors.")
        live_color = "#7570B3"
        dead_color = "#000000"
        complete_color_list = [live_color, dead_color, ]
    elif custom_colors_flag == "Use custom HEX colors":
        print("Using the rgb HEX colors provided.")
        live_color = ws.cell("B2").value
        dead_color = ws.cell("B3").value
        complete_color_list = [live_color, dead_color, ]
    elif custom_colors_flag == "Use cell fill color":
        live_color, temp_flag = GetCellColor(ws.cell("B2"))
        theme_flag = theme_flag or temp_flag
        dead_color, temp_flag = GetCellColor(ws.cell("B3"))
        theme_flag = theme_flag or temp_flag
        complete_color_list = [live_color, dead_color, ]
        if theme_flag:
            print(
                "\nUnable to convert excel 'Theme Colors' to the proper RGB, using approximate colors instead, as listed above.\n"
                "Please set your custom colors using excel's 'Standard Colors' or by selecting rgb values using excel's 'Custom Colors...', "
                "not by picking from the 'Theme colors' list.\n"
                "For greatest control over the colors, select 'Use custom HEX colors' and pick your hexadecimal values.\n"
                "Remember to include the '#' sign. You can get those codes from a site like http://www.w3schools.com/tags/ref_colorpicker.asp\n")
    else:
        print("This situation shouldn't have happend, this situation should have not happened!\n"
              "The program will error out now. Please check the entry on cell 'B1' under the Preferences sheet.\n"
              "Goodbye!")

    # The following is a temporary solution. Python is not processing the '#' symbol well.
    complete_color_list = [s.replace('#', '') for s in complete_color_list]
    live_color = live_color.replace('#', '')
    dead_color = dead_color.replace('#', '')
    # End of temporary solution


    if any(current_color == "#FFFFFF" for current_color in complete_color_list) or \
            any(current_color == "FFFFFF" for current_color in complete_color_list):
        print("Warning: one of the selected colors is white.")

    template = wb.get_sheet_by_name("Metadata").cell("B29").value
    # From the complete color list, select only the colors that will be used
    if template == "Total+Viability":
        custom_color_list = ['#' + live_color, '#' + dead_color, ]
    elif template == "Total_Cells":
        custom_color_list = ['#' + live_color, ]

    return custom_color_list


def DataTable(data, headers, template):
    # This is a custom function to write the data table, particularly due to the headers

    if template == "Total_Cells":
        html_text = "<table>\n\t<tr>"

        for column in headers:
            if column in ["Days", "Hours", "Minutes", "Seconds"]:
                html_text += "<th></th>"
            # elif column != "Total cells":
            else:
                html_text += '<th colspan="2">' + str(column) + "</th>"
        html_text += "</tr>\n\t<tr>"
        for column in headers:
            if column in ["Days", "Hours", "Minutes", "Seconds"]:
                html_text += "<th>" + str(column) + "</th>"
            # elif column != "Total cells":
            else:
                html_text += "<th>" + "Mean" + "</th>" + "<th>" + "STD" + "</th>"
        html_text += "</tr>\n"
        # now write the cells
        for row in data:
            html_text += "\t<tr>"
            for col in row:
                html_text += "<td>" + str(col) + "</td>"
            html_text += "</tr>\n"
        html_text += "</table>"
    else:
        html_text = "<table>\n\t<tr>"

        for column in headers:
            if column in ["Days", "Hours", "Minutes", "Seconds"]:
                html_text += "<th></th>"
            elif column != "Total cells":
                # else:
                html_text += '<th colspan="2">' + str(column) + "</th>"
        html_text += "</tr>\n\t<tr>"
        for column in headers:
            if column in ["Days", "Hours", "Minutes", "Seconds"]:
                html_text += "<th>" + str(column) + "</th>"
            elif column != "Total cells":
                # else:
                html_text += "<th>" + "Mean" + "</th>" + "<th>" + "STD" + "</th>"
        html_text += "</tr>\n"
        # now write the cells
        for row in data:
            html_text += "\t<tr>"
            for col in row:
                html_text += "<td>" + str(col) + "</td>"
            html_text += "</tr>\n"
        html_text += "</table>"

    return html_text


#######################################################################
######################## Beginning of file ############################
#######################################################################
# Read target to be fitted


# Removing a matplotib warning about tight_layout
import warnings

warnings.filterwarnings("ignore", category=UserWarning, module="matplotlib")

# load the Microsoft Excel file

show_replicates = True

f = load_workbook(file)
print("Successfully read the data file named ", file, ".")

conditions_type = f.get_sheet_by_name("Metadata").cell("B32").value

if conditions_type == 'Single environmental condition':
    out_dir_root = f.get_sheet_by_name("Preferences").cell("B9").value
    print("Creating the output directory called", out_dir_root)
    # Create the output directory if it doesn't exists
    if not os.path.exists(out_dir_root):
        os.makedirs(out_dir_root)
    if not os.path.exists(out_dir_root + "/output/"):
        os.makedirs(out_dir_root + "/output/")

    # ws is worksheet that contains the metadata
    ws = f.get_sheet_by_name("Metadata")
    metadata_dictionary = ReadMetadata(ws)

    print("Hello", metadata_dictionary["user"]["given_names"], "\n")

    print(f.get_sheet_by_name("Metadata").cell("B32").value)
    if f.get_sheet_by_name("Metadata").cell("B32").value == 'Multiple environmental conditions':
        print('\a')
        print('********************************************************************************')
        print('********************************************************************************')
        print('Dear',metadata_dictionary["user"]["given_names"],'You should be using the file called "runMultipleConditions".')
        print('If you meant to run only one environmental condition, please indicate so on your input file, in the sheet "Metadata", cell B32.')
        print("CellPD will now exit.")
        print('********************************************************************************')
        print('********************************************************************************')
        exit()

    # Reading the graph colors
    color_list = ReadColors(f)
    # Reading the rest of the preferences

    time_input_units = f.get_sheet_by_name("Preferences").cell("B8").value
    if time_input_units == 'Days':
        conversion_factor = 24
    elif time_input_units == 'Hours':
        conversion_factor = 1
    elif time_input_units == 'Minutes':
        conversion_factor = 1 / 60
    elif time_input_units == 'Seconds':
        conversion_factor = 1 / (60 * 60)
    else:
        print('Time unit "{:s}" not supported'.format(time_input_units))

    # Now save data into a standard table
    # ws is worksheet that contains the data
    template = f.get_sheet_by_name("Metadata").cell("B29").value
    template_type = f.get_sheet_by_name("Metadata").cell("B31").value
    ws = f.get_sheet_by_name(template)
    print("Now working with", ws)

    # time_measurement = f.get_sheet_by_name("Preferences").cell("B8").value
    time_measurement = "Hours"
    # Depending on the template, set up the variables:
    if template == "Total+Viability":
        number_of_measurements = 2
        number_of_headers = 3
        if template_type == "Means+SE":
            data_cols = [1, 4, ]
        else:
            data_cols = [3, 4, ]
        table_headers = [time_measurement, "Total cells", "Live cells", "Dead cells"]
        # model_list = ["live", "live_logistic",
        #               "total", "total_logistic",
        #               "live_dead", "live_dead_logistic", ]
        model_list = ["live", "live_logistic",
                      "total", "total_logistic",
                      "live_dead", "live_dead_logistic", "log_live_dead"]
        # model_list = [  "total"]
        # model_list = [  "log_live_dead"] # For Debugging purposes
    elif template == "Total_Cells":
        number_of_measurements = 1
        number_of_headers = 1
        if template_type == "Means+SE":
            data_cols = [1, ]
        else:
            data_cols = [3, ]
        table_headers = [time_measurement, "Total cells"]
        model_list = [  "total","total_logistic",]

    else:
        print("An unexpected error happened. Program will error out now.")
    data_points = len(list(ws.columns)[0]) - 1  # Read how many columns are there in the file, this is the number of time points\
    # row_count = ws.get_highest_row() - 1

    # Check if the bottom rows are emtpy, if so throw them away
    while (list(ws.columns)[0][data_points].value == None):
        data_points -= 1

    data = {}

    if template_type == "Means+SE":

        show_replicates = False

        # Reading in the data in OpenPyXL cell format
        time_cells = list(ws.columns)[0][2:data_points + 1]
        time = np.array([i.value for i in time_cells])
        means = np.zeros((len(np.unique(time)), number_of_headers))
        stds = np.zeros((len(np.unique(time)), number_of_headers))
        sem = np.zeros((len(np.unique(time)), number_of_headers))
        n_val = np.empty(len(np.unique(time)))
        # -------------------------------------------------
        # CHANGE THIS!!! ASSUMING 3 BIO REPS FOR NOW
        n_val.fill(3)
        # -------------------------------------------------

        replicate_values = []

        data_col_dictionary = {
            0: 1,  # total
            1: 4,  # live
            2: 0,  # dead -- not to be used
        }

        col_index = 0
        # for current_data_col in data_cols:
        for current_data_header in range(number_of_headers):
            current_data_col = data_col_dictionary[current_data_header]
            mean_cells = list(ws.columns)[current_data_col][2:data_points + 1]
            sem_cells = list(ws.columns)[current_data_col + 1][2:data_points + 1]
            stds_cells = list(ws.columns)[current_data_col + 2][2:data_points + 1]

            for current_row in range(len(mean_cells)):
                if col_index in [0, 1]:  # Total cells and Live Cells
                    means[current_row, col_index] = mean_cells[current_row].value
                    stds[current_row, col_index] = stds_cells[current_row].value
                    sem[current_row, col_index] = sem_cells[current_row].value
                else:
                    total_mean_cells = list(ws.columns)[1][2:data_points + 1]
                    total_sem_cells = list(ws.columns)[2][2:data_points + 1]
                    total_stds_cells = list(ws.columns)[3][2:data_points + 1]
                    live_mean_cells = list(ws.columns)[4][2:data_points + 1]
                    live_sem_cells = list(ws.columns)[5][2:data_points + 1]
                    live_stds_cells = list(ws.columns)[6][2:data_points + 1]
                    if col_index == 2:  # dead cells
                        means[current_row, col_index] = total_mean_cells[current_row].value - live_mean_cells[
                            current_row].value
                        sem[current_row, col_index] = total_sem_cells[current_row].value * (
                        total_mean_cells[current_row].value - live_mean_cells[current_row].value) / total_mean_cells[
                                                          current_row].value
                        stds[current_row, col_index] = total_stds_cells[current_row].value * (
                        total_mean_cells[current_row].value - live_mean_cells[current_row].value) / total_mean_cells[
                                                           current_row].value
                    else:
                        print("This should have not happened.")
                stds[current_row, col_index] = max(stds[current_row, col_index], 1e-16)
                sem[current_row, col_index] = max(sem[current_row, col_index], 1e-16)

            col_index += 1

        if time_input_units == 'Days':
            time = time * 24
            conversion_factor = 24
        elif time_input_units == 'Hours':
            conversion_factor = 1
        elif time_input_units == 'Minutes':
            time = time / 60
            conversion_factor = 1 / 60
        elif time_input_units == 'Seconds':
            time = time / (60 * 60)
            conversion_factor = 1 / (60 * 60)
    # elif template_type == "Technical replicates for each biological replicates":
    else:
        print('Technical replicates and biological replicates')
        # Reading in the data in OpenPyXL cell format
        time_cells = list(ws.columns)[2][1:data_points + 1]
        biological_replicate_cells = list(ws.columns)[0][1:data_points + 1]
        technical_replicate_cells = list(ws.columns)[1][1:data_points + 1]
        data_cells_total = list(ws.columns)[3][1:data_points + 1]
        if number_of_headers == 1:
            data_cells_viable = data_cells_total
        else:
            data_cells_viable = list(ws.columns)[4][1:data_points + 1]

        # Converting the cell arrays to NumPy arrays
        ti_array = np.array([i.value for i in time_cells])
        br_array = np.array([i.value for i in biological_replicate_cells])
        tr_array = np.array([i.value for i in technical_replicate_cells])
        to_array = np.array([i.value for i in data_cells_total])
        vi_array = np.array([i.value for i in data_cells_viable])
        de_array = to_array - vi_array

        time = np.empty(len(np.unique(ti_array)))
        means = np.empty((len(np.unique(ti_array)), number_of_headers))
        stds = np.empty((len(np.unique(ti_array)), number_of_headers))
        sem = np.empty((len(np.unique(ti_array)), number_of_headers))
        n_val =  np.empty(len(np.unique(ti_array)))

        tech_reps_time = []
        tech_reps_total = []
        tech_reps_viable = []

        bio_reps_time = []
        bio_reps_total_mean = []
        bio_reps_total_sem = []
        bio_reps_viable = []

        test_time = []

        time_keys, indices, counts = np.unique(ti_array, return_index=True, return_counts=True, )

        number_of_biological_replicates = 0
        number_of_technical_replicates = 0

        single_bio_rep_flag = False
        all_single_bio_rep_flag = False

        row_counter = 0
        for time_key in time_keys:
            test_time.append(time_key)
            data[time_key] = {}
            br_keys, tech_replicate_counts = np.unique(br_array[ti_array == time_key], return_counts=True)

            temp_br_array_total = np.empty(len(br_keys))
            temp_br_array_viable = np.empty(len(br_keys))
            temp_br_array_dead = np.empty(len(br_keys))
            temp_br_index = 0

            number_of_biological_replicates = max([number_of_biological_replicates, len(br_keys)])
            number_of_technical_replicates = max([number_of_technical_replicates, max(tech_replicate_counts)])
            # print(bio_rep,tech_rep)
            data[time_key]['Total'] = {}
            data[time_key]['Live'] = {}
            data[time_key]['Dead'] = {}

            for br_key in br_keys:
                data[time_key][br_key] = {}
                data[time_key][br_key]['Total'] = {}
                data[time_key][br_key]['Live'] = {}
                data[time_key][br_key]['Dead'] = {}

                # print(br_key)
                # print(br_array==br_key)
                # print(ti_array==time_key)
                # print(np.logical_and((br_array==br_key),(ti_array==time_key)))
                # print((br_array==br_key)&(ti_array==time_key),'\n')

                # print((br_array==br_key and (ti_array==time_key)))
                tech_reps_time.append(ti_array[((br_array == br_key) & (ti_array == time_key))])
                tech_reps_total.append(to_array[((br_array == br_key) & (ti_array == time_key))])
                tech_reps_viable.append(vi_array[((br_array == br_key) & (ti_array == time_key))])

                bio_reps_time.append(time_key)
                bio_reps_total_mean.append(np.mean(to_array[((br_array == br_key) & (ti_array == time_key))]))
                bio_reps_total_sem.append(np.std(to_array[((br_array == br_key) & (ti_array == time_key))]) / len(
                    to_array[((br_array == br_key) & (ti_array == time_key))]))
                bio_reps_viable = []

                data[time_key][br_key]['Total']['median'] = np.median(
                    to_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Total']['mean'] = np.mean(to_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Total']['std'] = np.std(to_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Total']['sem'] = data[time_key][br_key]['Total']['std'] / len(
                    to_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Total']['tech_reps'] = to_array[((br_array == br_key) & (ti_array == time_key))]
                data[time_key][br_key]['Total']['tech_reps_time'] = ti_array[
                    ((br_array == br_key) & (ti_array == time_key))]
                temp_br_array_total[temp_br_index] = np.mean(to_array[((br_array == br_key) & (ti_array == time_key))])

                data[time_key][br_key]['Live']['median'] = np.median(
                    vi_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Live']['mean'] = np.mean(vi_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Live']['std'] = np.std(vi_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Live']['sem'] = data[time_key][br_key]['Live']['std'] / len(
                    vi_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Live']['tech_reps'] = vi_array[((br_array == br_key) & (ti_array == time_key))]
                data[time_key][br_key]['Live']['tech_reps_time'] = ti_array[((br_array == br_key) & (ti_array == time_key))]
                temp_br_array_viable[temp_br_index] = np.mean(vi_array[((br_array == br_key) & (ti_array == time_key))])

                data[time_key][br_key]['Dead']['median'] = np.median(
                    de_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Dead']['mean'] = np.mean(de_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Dead']['std'] = np.std(de_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Dead']['sem'] = data[time_key][br_key]['Dead']['std'] / len(
                    de_array[((br_array == br_key) & (ti_array == time_key))])
                data[time_key][br_key]['Dead']['tech_reps'] = de_array[((br_array == br_key) & (ti_array == time_key))]
                data[time_key][br_key]['Dead']['tech_reps_time'] = ti_array[((br_array == br_key) & (ti_array == time_key))]
                temp_br_array_dead[temp_br_index] = np.mean(de_array[((br_array == br_key) & (ti_array == time_key))])

                temp_br_index += 1
            if len(br_keys) == 1:  # Time 'time_key' has only one biological replicate
                # We take its variability from the technical replicates

                all_single_bio_rep_flag = all_single_bio_rep_flag | True
                single_bio_rep_flag = True

                data[time_key]['Total']['bio_reps'] = data[time_key][br_keys[0]]['Total']['tech_reps']
                data[time_key]['Total']['median'] = data[time_key][br_keys[0]]['Total']['median']
                data[time_key]['Total']['mean'] = data[time_key][br_keys[0]]['Total']['mean']
                data[time_key]['Total']['std'] = data[time_key][br_keys[0]]['Total']['std']
                data[time_key]['Total']['sem'] = data[time_key][br_keys[0]]['Total']['sem']
                data[time_key]['Total']['single_bio_rep'] = True

                data[time_key]['Live']['bio_reps'] = data[time_key][br_keys[0]]['Live']['tech_reps']
                data[time_key]['Live']['median'] = data[time_key][br_keys[0]]['Live']['median']
                data[time_key]['Live']['mean'] = data[time_key][br_keys[0]]['Live']['mean']
                data[time_key]['Live']['std'] = data[time_key][br_keys[0]]['Live']['std']
                data[time_key]['Live']['sem'] = data[time_key][br_keys[0]]['Live']['sem']
                data[time_key]['Live']['single_bio_rep'] = True

                data[time_key]['Dead']['bio_reps'] = data[time_key][br_keys[0]]['Dead']['tech_reps']
                data[time_key]['Dead']['median'] = data[time_key][br_keys[0]]['Dead']['median']
                data[time_key]['Dead']['mean'] = data[time_key][br_keys[0]]['Dead']['mean']
                data[time_key]['Dead']['std'] = data[time_key][br_keys[0]]['Dead']['std']
                data[time_key]['Dead']['sem'] = data[time_key][br_keys[0]]['Dead']['sem']
                data[time_key]['Dead']['single_bio_rep'] = True

            else:
                all_single_bio_rep_flag = False

                data[time_key]['Total']['bio_reps'] = temp_br_array_total
                data[time_key]['Total']['median'] = np.median(temp_br_array_total)
                data[time_key]['Total']['mean'] = np.mean(temp_br_array_total)
                data[time_key]['Total']['std'] = np.std(temp_br_array_total)
                data[time_key]['Total']['sem'] = np.std(temp_br_array_total) / len(temp_br_array_total)
                data[time_key]['Total']['single_bio_rep'] = False

                data[time_key]['Live']['bio_reps'] = temp_br_array_viable
                data[time_key]['Live']['median'] = np.median(temp_br_array_viable)
                data[time_key]['Live']['mean'] = np.mean(temp_br_array_viable)
                data[time_key]['Live']['std'] = np.std(temp_br_array_viable)
                data[time_key]['Live']['sem'] = np.std(temp_br_array_viable) / len(temp_br_array_viable)
                data[time_key]['Live']['single_bio_rep'] = False

                data[time_key]['Dead']['bio_reps'] = temp_br_array_dead
                data[time_key]['Dead']['median'] = np.median(temp_br_array_dead)
                data[time_key]['Dead']['mean'] = np.mean(temp_br_array_dead)
                data[time_key]['Dead']['std'] = np.std(temp_br_array_dead)
                data[time_key]['Dead']['sem'] = np.std(temp_br_array_dead) / len(temp_br_array_dead)
                data[time_key]['Dead']['single_bio_rep'] = False

            for col_counter in range(number_of_headers):
                if col_counter == 0:
                    means[row_counter, col_counter] = data[time_key]['Total']['mean']
                    stds[row_counter, col_counter] = data[time_key]['Total']['std']
                    sem[row_counter, col_counter] = data[time_key]['Total']['sem']
                elif col_counter == 1:
                    means[row_counter, col_counter] = data[time_key]['Live']['mean']
                    stds[row_counter, col_counter] = data[time_key]['Live']['std']
                    sem[row_counter, col_counter] = data[time_key]['Live']['sem']
                elif col_counter == 2:
                    means[row_counter, col_counter] = data[time_key]['Dead']['mean']
                    stds[row_counter, col_counter] = data[time_key]['Dead']['std']
                    sem[row_counter, col_counter] = data[time_key]['Dead']['sem']
                stds[row_counter, col_counter] = max(stds[row_counter, col_counter], 1e-16)
                sem[row_counter, col_counter] = max(sem[row_counter, col_counter], 1e-16)
            time[row_counter] = time_key
            n_val[row_counter] = len(data[time_key]['Total']['bio_reps'])

            row_counter += 1

        if time_input_units == 'Days':
            time = time * 24
            conversion_factor = 24
        elif time_input_units == 'Hours':
            conversion_factor = 1
        elif time_input_units == 'Minutes':
            time = time / 60
            conversion_factor = 1 / 60
        elif time_input_units == 'Seconds':
            time = time / (60 * 60)
            conversion_factor = 1 / (60 * 60)

            # print(tr_array[br_array[ti_array==time_key]==br_key])
        if (all_single_bio_rep_flag):
            print(
                "All of the time points had a single biological replicate, variability will be computed from technical replicates")
        elif (single_bio_rep_flag):
            print("Some data points had a single biological replicate, some others had more than one. "
                  "Note that the algorithm will try to fit data points with smaller variability. This may lead to some problems. "
                  "We recommend running experiments with more than one biological replicate. If this is preeliminary data "
                  "and the result of the parameter estimation seem incorrect, then try to use the same number of biological "
                  "replicates across different data points so the variability in the datapoints (e.g. variances) are compatible with each other.")
        if number_of_biological_replicates < 5:
            show_replicates = True

            # print(data[0])
            # print(tech_reps_time)
            # print(tech_reps_total)

    if template == "Total+Viability":
        y_init_dictionary = {
            "L": means[0, [1]],
            "T": means[0, [0]],
            "LD": means[0, [1, 2]],
            "live": means[0, [1]],
            "live_logistic": means[0, [1]],
            "live_dead": means[0, [1, 2]],
            "live_dead_logistic": means[0, [1, 2]],
            "total": means[0, [0]],
            "total_ODE": means[0, [0]],
            "total_logistic": means[0, [0]],
            "total_allee": means[0, [0]],
            "gompertz": means[0, [0]],
            "log_live_dead": means[0, [1, 2]],
        }

    elif template == "Total_Cells":
        y_init_dictionary = {
            "T": means[0, [0]],
            "total": means[0, [0]],
            "gompertz": means[0, [0]],
            "total_logistic": means[0, [0]],
            "total_allee": means[0, [0]],
            "logistic_gf": means[0, [0]],
        }

    else:
        print("An unexpected error happened. Program will error out now.")


        def PlotTechnicalReplicates(time, data, time_b, data_b_m, data_b_sem):
            if len(time) != len(data):
                warnings.warn("The length of 'time' must be the same length as 'data'")
                print(len(time), len(data))
                raise SystemExit
            [plt.plot(time_temp, tech_reps, 'b+') for (time_temp, tech_reps) in zip(time, data)]
            plt.plot(time_b, data_b_m, )

            plt.show()

            return

    f = open(out_dir_root + "/output/data_summary.csv", "w")
    for column in table_headers:
        if column in ["Days", "Hours", "Minutes", "Seconds"]:
            f.write(str(column) + ",")
        else:
            f.write(str(column) + "-mean,")
            f.write(str(column) + "-std,")
    f.write("n")
    f.write("\n")
    for row in range(len(time)):
        f.write("{:2.1f}".format(time[row] / conversion_factor) + ",")
        for i in range(number_of_headers):
            f.write("{:1.3g}".format(means[row][i]) + "," + "{:1.3g}".format(stds[row][i]) + ",")
        f.write("{:d}".format(int(n_val[row])))
        f.write("\n")
    f.close()

    data_summary = []
    for row in range(len(time)):
        temp_row = []
        temp_row.append("{:2.1f}".format(time[row]))
        for i in range(number_of_measurements):
            temp_row.append("{:1.3g}".format(means[row][i]))
            temp_row.append("{:1.3g}".format(stds[row][i]))
        data_summary.append(temp_row)

    f = open(out_dir_root + "/output/data_summary.txt", "w")
    f.write(DataTable(data=data_summary, headers=table_headers, template=template))
    f.close()

    if template == "Total+Viability":
        data_key = "live_dead"
        target_dictionary = {
            "L": means[:, [1]],
            "T": means[:, [0]],
            "LD": means[:, [1, 2]],
            "live": means[:, [1]],
            "live_logistic": means[:, [1]],
            "live_dead": means[:, [1, 2]],
            "live_dead_logistic": means[:, [1, 2]],
            "total": means[:, [0]],
            "total_logistic": means[:, [0]],
            "total_allee": means[:, [0]],
        }
        sigmas_dictionary = {
            "L": stds[:, [1]],
            "T": stds[:, [0]],
            "LD": stds[:, [1, 2]],
            "live": stds[:, [1]],
            "live_logistic": stds[:, [1]],
            "live_dead": stds[:, [1, 2]],
            "live_dead_logistic": stds[:, [1, 2]],
            "total": stds[:, [0]],
            "total_logistic": stds[:, [0]],
            "total_allee": stds[:, [0]],
        }
        std_error_dictionary = {
            "L": sem[:, [1]],
            "T": sem[:, [0]],
            "LD": sem[:, [1, 2]],
            "live": sem[:, [1]],
            "live_logistic": sem[:, [1]],
            "live_dead": sem[:, [1, 2]],
            "live_dead_logistic": sem[:, [1, 2]],
            "total": sem[:, [0]],
            "total_logistic": sem[:, [0]],
            "total_allee": sem[:, [0]],
        }
    elif template == "Total_Cells":
        data_key = "total"
        target_dictionary = {
            "T": means[:, [0]],
            "total": means[:, [0]],
            "total_logistic": means[:, [0]],
            "total_allee": means[:, [0]],
        }
        sigmas_dictionary = {
            "T": stds[:, [0]],
            "total": stds[:, [0]],
            "total_logistic": stds[:, [0]],
            "total_allee": stds[:, [0]],
        }
        std_error_dictionary = {
            "T": sem[:, [0]],
            "total": sem[:, [0]],
            "total_logistic": sem[:, [0]],
            "total_allee": sem[:, [0]],
        }

    ########################################################################################################################
    ########################################################################################################################


    fig_counter = 1
    target = target_dictionary[data_key]
    # sigmas = sigmas_dictionary[data_key] #Ploting the variance instead of the standard error
    sigmas = std_error_dictionary[data_key]  # Ploting the standard error instead of the variances

    if drop_values:
        # Drop all the points where the first diff is negative and/or the second diff is positive
        x = np.log(target[:, 0])
        first_diff = np.diff(x, n=1, axis=0)
        second_diff = np.diff(first_diff, n=1, axis=0)
        (a,) = (first_diff >= 0).ravel().nonzero()
        (b,) = (second_diff <= 0).ravel().nonzero()

        if len(a) == 0:
            print("These data does not behave like exponential growth, cells do not even increase in value.")
            a = [0]
        if len(b) == 0:
            print(
                "These data does not behave like exponential growth, cells appear to be growing close to logistic limits from the beggining of experiment.")
            b = [0]
        first_index = max(a[0], b[0])

        # Find the index when the cell population decreases by at least 10%
        (last_index,) = (np.diff(target[first_index:, 0]) < -0.1 * target[first_index:-1, 0]).ravel().nonzero()

        if last_index.size == 0:
            last_index = len(x) - 1  # If population doesn't decrease, then use all of the time points
        else:
            last_index = last_index[0] + first_index

        print("Dropping {:d} time points at the beginning and keeping up to time point number {:d}".format(first_index,
                                                                                                           last_index + 1))

        old_target = np.copy(target)
        target = target[first_index:last_index + 1, :]

        old_means = np.copy(means)
        means = means[first_index:last_index + 1, :]

        old_sem = np.copy(sem)
        sem = sem[first_index:last_index + 1, :]

        old_stds = np.copy(stds)
        stds = stds[first_index:last_index + 1, :]

        old_time = np.copy(time)
        time = time[first_index:last_index + 1]

        old_sigmas = np.copy(sigmas)
        sigmas = sigmas[first_index:last_index + 1, :]



    PlotData(time, target, sigmas, data_key, fig_counter, color_list)

    fig_counter += 1
    units = dict()
    models_chi = []
    models_mape = []
    models_chi_table = []
    models_mape_table = []

    def running_mean(x,N):
        cumsum = np.cumsum(np.insert(x,0,0))
        return (cumsum[N:]-cumsum[:-N])/N

    ######################### Start the Optimization ##########################

    # Compute a first guess for the growth rate:
    gr_init = np.log(target[-1, 0] / target[0, 0]) / (time[-1] - time[0])

    # First guess for the carrying capacity as twice the largest value measured:
    cc_init = max(target[:, 0] * 10)

    fit_summary_dict = {}

    function_name = "live"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        init_params.add('growth_rate', value=gr_init)
        init_params.add('seeding_cells', value=y_init_dictionary[function_name][0], min=0, vary=False)
        units.update(growth_rate="1/hours")
        units.update(seeding_cells="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "live_logistic"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        init_params.add('growth_rate', value=gr_init)
        init_params.add('carrying_capacity', value=cc_init, min=0)
        init_params.add('seeding_cells', value=y_init_dictionary[function_name][0], min=0, vary=False)
        units.update(growth_rate="1/hours")
        units.update(carrying_capacity="number of cells")
        units.update(seeding_cells="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "total"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        init_params.add('growth_rate', value=gr_init)
        init_params.add('seeding_cells', value=y_init_dictionary[function_name][0], min=0, vary=False)
        units.update(growth_rate="1/hours")
        units.update(seeding_cells="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "gompertz"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        init_params.add('growth_rate', value=gr_init, min=0)
        init_params.add('carrying_capacity', value=cc_init, min=0)
        init_params.add('seeding_cells', value=y_init_dictionary[function_name][0], min=0, vary=False)
        units.update(growth_rate="1/hours")
        units.update(carrying_capacity="number of cells")
        units.update(seeding_cells="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "total_logistic"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        init_params.add('growth_rate', value=gr_init)
        init_params.add('carrying_capacity', value=cc_init, min=0)
        init_params.add('seeding_cells', value=y_init_dictionary[function_name][0], min=0, vary=False)
        units.update(growth_rate="1/hours")
        units.update(carrying_capacity="number of cells")
        units.update(seeding_cells="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "logistic_gf"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        init_params.add('growth_rate', value=gr_init, min=0)
        init_params.add('carrying_capacity', value=cc_init, min=0)
        init_params.add('seeding_cells', value=y_init_dictionary[function_name][0], min=0, vary=False)
        units.update(growth_rate="1/hours")
        units.update(carrying_capacity="number of cells")
        units.update(seeding_cells="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=0.01*sem/sem, sem=0.01*sem/sem, color_list=color_list)
        fig_counter += 1

    function_name = "total_allee"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        init_params.add('growth_rate', value=gr_init, min=0)
        init_params.add('carrying_capacity', value=cc_init, min=0)
        init_params.add('seeding_cells', value=y_init_dictionary[function_name][0], min=0, vary=False)
        # init_params.add('minimum_population', value=0.11*y_init_dictionary[function_name][0], min=0, max=y_init_dictionary[function_name][0] - 1e-16)
        init_params.add('minimum_population', value=0.065, vary=False)
        units.update(growth_rate="1/hours")
        units.update(carrying_capacity="number of cells")
        units.update(seeding_cells="number of cells")
        units.update(minimum_population="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "live_dead"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        if (gr_init < 0):
            init_params.add('birth_rate', value=0, min=0)
            init_params.add('death_rate', value=-gr_init, min=0)
        else:
            init_params.add('birth_rate', value=gr_init * 0.9, min=0)
            init_params.add('death_rate', value=gr_init * 0.1, min=0)
        init_params.add('clearance_rate', value=0, min=0, vary=False)
        init_params.add('seeding_cells_live', value=y_init_dictionary[function_name][0], min=0, vary=False)
        init_params.add('seeding_cells_dead', value=y_init_dictionary[function_name][1], min=0, vary=False)
        units.update(birth_rate="1/hours")
        units.update(death_rate="1/hours")
        units.update(clearance_rate="1/hours")
        units.update(seeding_cells_live="number of cells")
        units.update(seeding_cells_dead="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "live_dead_logistic"
    if function_name in model_list:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("We are fitting the model named", function_name)
        # create a set of Parameters
        init_params = Parameters()
        if (gr_init < 0):
            init_params.add('birth_rate', value=0, min=0)
            init_params.add('death_rate', value=-gr_init, min=0)
        else:
            init_params.add('birth_rate', value=gr_init * 0.9, min=0)
            init_params.add('death_rate', value=gr_init * 0.1, min=0)
        init_params.add('clearance_rate', value=0, min=0, vary=False)
        init_params.add('carrying_capacity', value=cc_init, min=0)
        init_params.add('seeding_cells_live', value=y_init_dictionary[function_name][0], min=0, vary=False)
        init_params.add('seeding_cells_dead', value=y_init_dictionary[function_name][1], min=0, vary=False)
        units.update(birth_rate="1/hours")
        units.update(death_rate="1/hours")
        units.update(clearance_rate="1/hours")
        units.update(carrying_capacity="number of cells")
        units.update(seeding_cells_live="number of cells")
        units.update(seeding_cells_dead="number of cells")
        fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                           function_name=function_name, time=time, fig_counter=fig_counter,
                                                           means=means, stds=sem, sem=sem, color_list=color_list)
        fig_counter += 1

    function_name = "log_live_dead"
    if function_name in model_list:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("We are fitting the model named", function_name)
            # create a set of Parameters
            init_params = Parameters()
            init_params.add('birth_rate', value=gr_init * 0.9, min=0)
            init_params.add('death_rate', value=gr_init * 0.1, min=0)
            #init_params.add('clearance_rate', value=gr_init * 0.01, min=0)
            init_params.add('seeding_cells_live', value=y_init_dictionary[function_name][0], min=0, vary=False)
            init_params.add('seeding_cells_dead', value=y_init_dictionary[function_name][1], min=0, vary=False)
            units.update(birth_rate="1/hours")
            units.update(death_rate="1/hours")
            #units.update(clearance_rate="1/hours")
            units.update(seeding_cells_live="number of cells")
            units.update(seeding_cells_dead="number of cells")
            fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                               function_name=function_name, time=time,
                                                               fig_counter=fig_counter,
                                                               means=means, stds=sem, sem=sem, color_list=color_list)
            fig_counter += 1


    def PlotAll():
        # It assumes that we have acces to:
        # data, choose_model, fit_summary_dict
        number_of_subplots = 1
        fig, axs = plt.subplots(1, number_of_subplots, sharex=True)

        # First plot the data:
        if template == 'Total_Cells':
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Total']['single_bio_rep']:
                        for tech_rep in data[time_point]['Total']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Total']['mean'], '.',
                                         color=color_list[0], alpha=0.5)
                if True:  # not all_single_bio_rep_flag:
                    # axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.',
                    #              label='Measured Total cells', alpha=0.5)
                    axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt='.',
                                 label='Measured Total cells', alpha=0.5)
                    # axs.errorbar(time,target[:,0],yerr=sigmas[:,0],color=color_list[0],fmt=':.',label='Measured Total cells',alpha=0.5)
            elif True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.',
                             label='Measured Total cells')
        elif template == 'Total+Viability':
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Live']['single_bio_rep']:
                        for tech_rep in data[time_point]['Live']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Live']['mean'], '.',
                                         color=color_list[0], alpha=0.5)
                if True:  # not all_single_bio_rep_flag:
                    axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.',
                                 label='Measured Live cells', alpha=0.5)
            else:
                if True:  # not all_single_bio_rep_flag:
                    axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.',
                                 label='Measured Live cells')
        else:
            print(
                'An unexpected error happened [the template selecte id not one of the two valid options "Total_Cells", or "Total+Viability"]. CellPD will error out now.')

        axs.set_ylabel("Number of Cells")

        if template == 'Total+Viability':
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point]['Dead']['single_bio_rep']:
                        for tech_rep in data[time_point]['Dead']['bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, '.', color='#808080', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, str):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key]['Dead']['mean'], '.',
                                         color=color_list[1], alpha=0.5)
                if True:  # not all_single_bio_rep_flag:
                    axs.errorbar(time, target[:, 1], yerr=sigmas[:, 1], color=color_list[1], fmt=':.',
                                 label='Measured Dead cells', alpha=0.5)
            else:
                if True:  # not all_single_bio_rep_flag:
                    axs.errorbar(time, target[:, 1], yerr=sigmas[:, 1], color=color_list[1], fmt=':.',
                                 label='Measured Dead cells')

        # Plot all models in the same graph
        color_map = cmap.Paired
        number_of_models = len(model_list)
        index = 0
        for function_name in model_list:
            params = fit_summary_dict[function_name]['parameters']
            # Simulation time is a vector of 100 evenly spaced values in the interval [experiment_initial_time,experiemnt_final_time]
            t_sim = np.array(np.linspace(np.min(time), np.max(time), num=100))
            if function_name in ['live', 'total', 'gompertz', 'logistic_gf']:
                y_init = params['seeding_cells']
                y = choose_model[function_name](y_init, t_sim, params)
            elif function_name in ['live_logistic', 'total_logistic','total_allee']:
                y = odeint(choose_model[function_name], params['seeding_cells'], t_sim, args=(params,))
            elif function_name in ['live_dead', 'live_dead_logistic']:
                y = odeint(choose_model[function_name], [params['seeding_cells_live'], params['seeding_cells_dead']], t_sim,
                           args=(params,))
            elif function_name in ['log_live_dead']:
                y = choose_model[function_name]([params['seeding_cells_live'].value, params['seeding_cells_dead'].value], t_sim, params)
            else:
                print('The model {:s} is not supported.'.format(function_name))
            axs.plot(t_sim, y, '--', label=function_name, color=color_map(index/number_of_models) )
            index += 1

        plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
        plt.xlabel("time (Hours)")

        lgd_1 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        lgd_2 = axs.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

        suptitle = plt.suptitle("All Models", size=24, y=1.01)

        # Saving the figure in multiple formats
        out_dir = out_dir_root + "/output/data/"
        for format in format_list:
            fig.savefig(out_dir + 'all_models' + "." + format, dpi=75, format=format, bbox_inches='tight')
        plt.close(fig)

        return


    PlotAll()

    sorted_models = sorted(models_chi, key=lambda model: float(model[1]), reverse=False)
    sorted_models_2 = sorted(models_mape, key=lambda model: float(model[1].strip('%')), reverse=False)

    sorted_models_table = sorted(models_chi_table, key=lambda model: float(model[1]), reverse=False)
    sorted_models_2_table = sorted(models_mape_table, key=lambda model: model[1], reverse=False)

    # Writing the Digital Cell Line xml file

    # First concatenate all the estimated models
    models_concatenated = ""
    for model in model_list:
        f = open(out_dir_root + "/output/" + model + "/" + model + "_partial.xml")
        models_concatenated = models_concatenated + "\n" + f.read()
        f.close()

    xml_dictionary = {
        "MCDS_label": str(metadata_dictionary["cell_line"]["name"]),
        "MCDS_name": str(metadata_dictionary["cell_line"]["MultiCellDB"]),
        "cell_description": str(metadata_dictionary["cell_line"]["description"]),
        "tool_name": TOOL_NAME,
        "version": VERSION,
        "data_url": str(metadata_dictionary["cell_line"]["link"]),
        "short_date": get_time.strftime("%Y-%m-%d"),
        "long_date": TIME_STAMP,
        "user_ORCID": str(metadata_dictionary["user"]["orcid"]),
        "user_given_names": str(metadata_dictionary["user"]["given_names"]),
        "user_family_name": str(metadata_dictionary["user"]["family_name"]),
        "user_email": str(metadata_dictionary["user"]["email"]),
        "user_organization": str(metadata_dictionary["user"]["organization"]),
        "user_department": str(metadata_dictionary["user"]["department"]),
        "user_url": str(metadata_dictionary["user"]["website"]),
        "CLO_id": str(metadata_dictionary["cell_line"]["CLO"]).zfill(7),
        "BTO_id": str(metadata_dictionary["cell_line"]["BTO"]).zfill(7),
        "cell_species": str(metadata_dictionary["cell_line"]["organism"]),
        "cell_organ": str(metadata_dictionary["cell_line"]["organ"]),
        "cell_disease": str(metadata_dictionary["cell_line"]["disease"]),
        "cell_morphology": str(metadata_dictionary["cell_line"]["morphology"]),
        "uncertainty_note": uncertainty_note,
        "experiment_oxygenation_type": str(metadata_dictionary["cell_line"]["oxygenation_name"]),
        "experiment_oxygenation_measurement_type": str(metadata_dictionary["cell_line"]["oxygenation_measurement_type"]),
        "experiment_oxygenation": str(metadata_dictionary["cell_line"]["oxygenation_level"]),
        "cell_cycle_models": models_concatenated,

    }
    f = open(out_dir_root + "/output/" + str(metadata_dictionary["cell_line"]["MultiCellDB"]) + ".xml", "w")
    xml_template = open("files/DCL_Python_template.txt").read()
    f.write(xml_template.format(xml_dictionary))
    f.close()

    # Writing the overall report
    f = open(out_dir_root + "/overall_report.html", "w")

    html_dictionary = {
        "tool_name": TOOL_NAME,
        "version": VERSION,
        "data_table": open(out_dir_root + "/output/data_summary.txt").read(),
        "user_name": str(metadata_dictionary["user"]["given_names"]) + " " + str(
            metadata_dictionary["user"]["family_name"]),
        "date": get_time.strftime("%Y-%m-%d"),
        "user_data_table": tabulate(metadata_dictionary["user"]["list"], tablefmt="html", numalign="none", floatfmt="1.3g"),
        "experiment_data_table": tabulate(metadata_dictionary["cell_line"]["list"], tablefmt="html", numalign="none",
                                          floatfmt="1.3g"),
        "gof_table": tabulate(sorted_models,
                              ["Name", "&Chi;<sup>2</sup><sub style='position: relative; left: -.5em;'>&nu;</sub>",
                               "Warning"], tablefmt="html", floatfmt=".3g", numalign="none"),
        "models_table": tabulate(sorted_models_2, ["Name", "MAPE", "Warning"], tablefmt="html", floatfmt=".3g",
                                 numalign="none"),
        "data_caption": metadata_dictionary["cell_line"]["caption"],
        "citation_text": open("files/citation.txt").read(),
        "DCL_name": str(metadata_dictionary["cell_line"]["MultiCellDB"]),
        "cell_line_name": str(metadata_dictionary["cell_line"]["name"]),
    }

    f_2 = open(out_dir_root + "/output/rcs_ranking.csv", "w")
    f_2.write("Name,Reduced_Chi_Squared,Warning,Param,mean,SEM\n")
    for model_row in sorted_models_table:
        f_2.write("{:s},".format(model_row[0]))
        f_2.write("{:.3g},".format(model_row[1]))
        f_2.write("{:s}\n".format(model_row[2]))
        function_name = model_row[3]
        params = fit_summary_dict[function_name]['result'].params
        for key in params:
            f_2.write("{:s},".format(key))
            f_2.write("{:.3g},".format(params[key].value))
            f_2.write("{:.3g},".format(params[key].stderr))
        f_2.write('\n')
    f_2.close()

    f_2 = open(out_dir_root + "/output/mape_ranking.csv", "w")
    f_2.write("Name,MAPE,Warning,Param,mean,SEM\n")
    for model_row in sorted_models_2_table:
        f_2.write("{:s},".format(model_row[0]))
        f_2.write("{:.3g},".format(model_row[1]))
        f_2.write("{:s},".format(model_row[2]))

        function_name = model_row[3]
        params = fit_summary_dict[function_name]['result'].params
        for key in params:
            f_2.write("{:s},".format(key))
            f_2.write("{:.3g},".format(params[key].value))
            f_2.write("{:.3g},".format(params[key].stderr))
        f_2.write('\n')
    f_2.close()

    f_2 = open(out_dir_root + "/output/mape_summary.csv", "w")
    f_2.write("Name,MAPE,Warning,Param,mean,SEM\n")
    for model_row in models_mape_table:
        f_2.write("{:s},".format(model_row[0]))
        f_2.write("{:.3g},".format(model_row[1]))
        f_2.write("{:s},".format(model_row[2]))

        function_name = model_row[3]
        params = fit_summary_dict[function_name]['result'].params
        for key in params:
            f_2.write("{:s},".format(key))
            f_2.write("{:.3g},".format(params[key].value))
            f_2.write("{:.3g},".format(params[key].stderr))
        f_2.write('\n')
    f_2.close()

    html_template = open("files/index_template.html").read()
    f.write(html_template.format(html_dictionary))
    f.close()
    shutil.copy("files/style.css", out_dir_root + "/output/")

    # clean up the working folder
    os.remove("matplotlibrc")

    # creating the zip file with the ouputs of CellPD
    # shutil.make_archive('CellPD_output', 'zip', root_dir=None, base_dir=out_dir_root)
    # if os.path.isfile(out_dir_root + "/output/CellPD_output.zip"):
    #     os.remove(out_dir_root + "/output/CellPD_output.zip")
    # shutil.move("CellPD_output.zip", out_dir_root + "/output/")

    if OPEN_BROWSER:
        new = 2  # open in a new tab, if possible
        # open an HTML file on my own (Windows) computer
        url = "overall_report.html"
        webbrowser.open('file://' + os.path.realpath(out_dir_root + "/" + url), new=new)

    if HOLD_CMD:
        input("Press enter to exit, please :) \n")

elif conditions_type == 'Multiple environmental conditions':
    import shutil
    shutil.copy("files/matplotlibrc", "matplotlibrc")

    def iter_rows(ws):
        for row in ws.iter_rows():
            yield [cell.value for cell in row]

    def copy_worksheet(ws_in,ws_out):
        for row in list(iter_rows(ws_in)):
            ws_out.append(row)

    def file_len(fname, header=False):
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        if header:
            return i
        else:
            return i + 1

    def ReadSummary1(fname):
        file_length = file_len(fname, header=True)
        line_a = np.chararray(file_length, unicode=True, itemsize=200)
        drug_a = np.chararray(file_length, unicode=True, itemsize=200)
        concentration_a = np.zeros(file_length)
        n_a = np.zeros(file_length)
        gr_a = np.zeros(file_length)
        gr_sem_a = np.zeros(file_length)

        index = 0
        with open(fname) as f:
            f.readline()
            for row in f.readlines():
                temp = row.strip('\n').split(sep=',')
                # print(fname)
                line_a[index] = temp[0]
                drug_a[index] = temp[1]
                concentration_a[index] = temp[2]
                n_a[index] = temp[3]
                gr_a[index] = temp[4]
                gr_sem_a[index] = temp[5]
                index += 1
        return (line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,)

    def ReadSummary2(fname):
        file_length = file_len(fname, header=True)
        line_a = np.chararray(file_length, unicode=True, itemsize=200)
        drug_a = np.chararray(file_length, unicode=True, itemsize=200)
        concentration_a = np.zeros(file_length)
        n_a = np.zeros(file_length)
        gr_a = np.zeros(file_length)
        gr_sem_a = np.zeros(file_length)
        cc_a = np.zeros(file_length)
        cc_sem_a = np.zeros(file_length)


        index = 0
        with open(fname) as f:
            f.readline()
            for row in f.readlines():
                temp = row.strip('\n').split(sep=',')
                line_a[index] = temp[0]
                drug_a[index] = temp[1]
                concentration_a[index] = temp[2]
                n_a[index] = temp[3]
                gr_a[index] = temp[4]
                gr_sem_a[index] = temp[5]
                cc_a[index] = temp[6]
                cc_sem_a[index] = temp[7]
                index += 1
        return (line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,cc_a,cc_sem_a,)

    def ReadSummary3(fname):
        file_length = file_len(fname, header=True)
        line_a = np.chararray(file_length, unicode=True, itemsize=200)
        drug_a = np.chararray(file_length, unicode=True, itemsize=200)
        concentration_a = np.zeros(file_length)
        n_a = np.zeros(file_length)
        live_a = np.zeros(file_length)
        live_sem_a = np.zeros(file_length)
        dead_a = np.zeros(file_length)
        dead_sem_a = np.zeros(file_length)
        clear_a = np.zeros(file_length)
        clear_sem_a = np.zeros(file_length)


        index = 0
        with open(fname) as f:
            f.readline()
            for row in f.readlines():
                temp = row.strip('\n').split(sep=',')
                line_a[index] = temp[0]
                drug_a[index] = temp[1]
                concentration_a[index] = temp[2]
                n_a[index] = temp[3]
                live_a[index] = temp[4]
                live_sem_a[index] = temp[5]
                dead_a[index] = temp[6]
                dead_sem_a[index] = temp[7]
                clear_a[index] = temp[8]
                clear_sem_a[index] = temp[9]
                index += 1
        return (line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a)

    def ReadSummary4(fname):
        file_length = file_len(fname, header=True)
        line_a = np.chararray(file_length, unicode=True, itemsize=200)
        drug_a = np.chararray(file_length, unicode=True, itemsize=200)
        concentration_a = np.zeros(file_length)
        n_a = np.zeros(file_length)
        live_a = np.zeros(file_length)
        live_sem_a = np.zeros(file_length)
        dead_a = np.zeros(file_length)
        dead_sem_a = np.zeros(file_length)
        clear_a = np.zeros(file_length)
        clear_sem_a = np.zeros(file_length)
        cc_a = np.zeros(file_length)
        cc_sem_a = np.zeros(file_length)

        index = 0
        with open(fname) as f:
            f.readline()
            for row in f.readlines():
                temp = row.strip('\n').split(sep=',')
                line_a[index] = temp[0]
                drug_a[index] = temp[1]
                concentration_a[index] = temp[2]
                n_a[index] = temp[3]
                live_a[index] = temp[4]
                live_sem_a[index] = temp[5]
                dead_a[index] = temp[6]
                dead_sem_a[index] = temp[7]
                clear_a[index] = temp[8]
                clear_sem_a[index] = temp[9]
                cc_a[index] = temp[10]
                cc_sem_a[index] = temp[11]
                index += 1
        return (line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a,cc_a,cc_sem_a,)

    import sys
    import argparse
    import os
    import matplotlib.pyplot as plt
    import matplotlib.cm as cmap

    # if not len(sys.argv) > 1:
    #     print("***************************************")
    #     print("***************************************")
    #     file = input("Please type the name of the input file:")
    #     #file = 'Synthetic'

    # else:
    #     parser = argparse.ArgumentParser()
    #     parser.add_argument("name", help="specify input file name")
    #     args = parser.parse_args()
    #     file = args.name
    #     print("Using input file name '", file, "' as selected")

    # if ('.xlsx' not in file) or ('.xls' not in file):
    #     if os.path.isfile(file + ".xlsx"):
    #         print("Adding '.xlsx' to the filename")
    #         file = file + ".xlsx"
    #     elif os.path.isfile(file + ".xls"):
    #         print("Adding '.xls' to the filename")
    #         file = file + ".xls"

    cell_line = file.replace('.xlsx','')

    import numpy as np
    from openpyxl import load_workbook, Workbook
    from subprocess import call
    import subprocess

    print(file)

    wb = load_workbook(filename=file)
    time_units = wb.get_sheet_by_name("Preferences").cell("B8").value
    sheet_type = wb.get_sheet_by_name("Metadata").cell("B29").value
    ws = wb.get_sheet_by_name(sheet_type)

    if sheet_type == 'Total+Viability':
        min_number_of_columns = 5
    elif sheet_type == 'Total_Cells':
        min_number_of_columns = 4
    else:
        print(sheet_type,'is not currently supported by CellPD. Goodbye!')
        min_number_of_columns = None
        exit()

    number_of_rows = ws.max_row - 1
    first_row = True
    second_row = True
    extra_cols = np.empty(0)
    for row in list(iter_rows(ws)):
        if first_row:
            headers = row
            extra_cols_headers = headers[min_number_of_columns:len(row)]
            first_row = False
            continue
        if second_row:
            extra_cols = np.asarray(row[min_number_of_columns:len(row)],dtype='str').flatten()
            second_row = False
            continue
        temp_cols = np.asarray(row[min_number_of_columns:len(row)],dtype='str').flatten()

        extra_cols = np.vstack((extra_cols,temp_cols))


    unique_conditions = np.vstack({tuple(row) for row in extra_cols})
    print(unique_conditions)

    # input(extra_cols_headers)



    # for row in unique_conditions:
    #     for element in row:
    #         element = str(element).replace("\n","")

    n_rows = unique_conditions.shape[0]
    n_cols = unique_conditions.shape[1]
    new_mat = np.chararray((n_rows, n_cols), itemsize=200, unicode=True)

    for row in range(n_rows):
        for col in range(n_cols):
            new_mat[row,col] = str(unique_conditions[row,col])

    # print(new_mat)
    # input(unique_conditions)
    unique_conditions = new_mat
    #sorting unique conditions

    unique_conditions = unique_conditions[unique_conditions[:, 1].argsort(kind='mergesort')]
    unique_conditions = unique_conditions[unique_conditions[:, 0].argsort(kind='mergesort')]

    # for line in unique_conditions:
    #     print('\n')
    #     print(line)
    #     print('\n')
    #     print(line[0],line[1])
    #     input()

    # input(unique_conditions)



    if CREATE_INPUTS:
        filelist = open(cell_line+'_created_files.txt','w')
        # Now go though each of the unique conditions, create a different CellPD file in the folder named after the input file
        if not os.path.exists(cell_line+'/'):
                os.makedirs(cell_line+'/')
        for condition in unique_conditions:
            print(condition)
            str_condition = cell_line+'_'+str(condition).replace('[','').replace(']','').replace(' ','_').replace("'",'')
            str_condition = str_condition.replace("__",'_').replace("\n","")
            wb = load_workbook(filename=file)

            out_wb = Workbook()
            out_ws = out_wb.active
            out_ws.title = 'Metadata'
            copy_worksheet(wb.get_sheet_by_name('Metadata'),out_ws)
            out_ws.cell('B32').value = 'Single environmental condition'

            out_ws = out_wb.create_sheet(title='Preferences')
            copy_worksheet(wb.get_sheet_by_name('Preferences'),out_ws)
            out_ws.cell('B9').value = cell_line+'/'+str_condition

            ws = wb.get_sheet_by_name(sheet_type)
            out_ws = out_wb.create_sheet(title=sheet_type)
            first_row = True
            for row in list(iter_rows(ws)):
                if first_row:
                    out_ws.append(row)
                    first_row = False
                    continue
                test_index = 0
                test = True
                for test_columns in row[min_number_of_columns:len(row)]:
                    # len_of_str = len(condition[test_index])
                    # print(len_of_str)
                    # temp = '{:.3f}'.format(test_columns)
                    # str_temp = np.chararray(test_columns,itemsize='5', unicode=True)
                    test = test and (str(test_columns) == str(condition[test_index]))
                    # print(test_columns,condition[test_index])
                    #print(condition[test_index])
                    #print(str(test_columns))
                    #input(test)
                    test_index += 1
                if test:
                    out_ws.append(row)
                    # print(row)
            out_wb.save(filename=cell_line+'/'+str_condition+'.xlsx')
            filelist.write(cell_line+'/'+str_condition+'.xlsx\n')
        filelist.close()

    if CALL_CELLPD:
        # Then call CellPD on each of those
        i = 1
        for condition in unique_conditions:
            if condition[0] == 'STATOXliplatin':
                pass
            else:
                str_condition = cell_line + '_' + str(condition).replace('[', '').replace(']', '').replace(' ','_').replace("'", '')
                str_condition = str_condition.replace("__", '_').replace("\n","")
                filename = cell_line+'/'+str_condition+'.xlsx'
                print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                print('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
                print('Condition',i,'of',len(unique_conditions))
                print('Working with filename',filename)
                print('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
                print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                print(os.path.isfile(filename))

                #  These lines are just trying to figure out what version of python is installed (if any) and will call CellPD accordingly
                proc = subprocess.Popen('python3 --version', shell=True, stdout=subprocess.PIPE)
                tmp = proc.stdout.read()
                print('proc =',proc,'tmp=',tmp)
                if b'3.' not in tmp:
                    if b'2.' in tmp:
                        print('Why do you have python 2 associated with the python3 command?! ಠ_ಠ')
                        print('(╯°□°）╯︵ ┻━┻')
                        call('python3 CellPD27.py ' + filename, shell=True)
                    else:
                        proc2 = subprocess.Popen('python --version', shell=True, stdout=subprocess.PIPE)
                        tmp2 = proc2.stdout.read()
                        # output = tmp.strip(b'\n').strip(b'\r')
                        if b'3.' in tmp2:
                            call('python CellPD.py ' + filename, shell=True)
                            # print('Python 3 yay!')
                        elif b'2.' in tmp2:
                            call('python CellPD27.py ' + filename, shell=True)
                        else:
                            if os.name == 'nt':
                                print('doing it on Windows')
                                try:
                                    call('runCellPD.bat ' + filename, shell=True)
                                except:
                                    print('Could not do it on Windows :( -- Python is probally not installed correctly on this computer.')
                            else:
                                print('Python is probally not installed correctly on this computer.')
                else:
                    if b'3.' in tmp:
                        # print('using python 3!')
                        call('python3 CellPD.py ' + filename, shell=True)
                    else:
                        print('Python is probally not installed correctly on this computer.')
                # END OF "These lines are just trying to figure out what version of python is installed (if any) and will call CellPD accordingly"
                i += 1

    # Then concatenate those reports to present summary of net growth rate

    index = 0
    headers_string = str(extra_cols_headers).replace('[', '').replace(']', '').replace("' '", '').replace(', ', ',').replace("'", '')

    total_summary = open(cell_line + '/' + cell_line + '_total_summary' + '.txt', 'w')
    total_summary.write('line,'+ headers_string + ',n,net_gr,sem\n')

    total_logistic_summary = open(cell_line + '/' + cell_line + '_total_logistic_summary' + '.txt', 'w')
    total_logistic_summary.write('line,'+ headers_string +',n,net_gr,sem,carrying_capacity,sem\n')

    if sheet_type == 'Total+Viability':
        live_summary = open(cell_line + '/' + cell_line + '_live_summary' + '.txt', 'w')
        live_summary.write('line,'+ headers_string + ',n,net_gr,sem\n')

        live_logistic_summary = open(cell_line + '/' + cell_line + '_live_logistic_summary' + '.txt', 'w')
        live_logistic_summary.write('line,'+ headers_string +',n,net_gr,sem,carrying_capacity,sem\n')

        live_dead_summary = open(cell_line + '/' + cell_line + '_live_dead_summary' + '.txt', 'w')
        live_dead_summary.write('line,'+ headers_string + ',n,birth_rate,sem,death_rate,sem,clearance_rate,sem\n')

        live_dead_logistic_summary = open(cell_line + '/' + cell_line + '_live_dead_logistic_summary' + '.txt', 'w')
        live_dead_logistic_summary.write('line,'+ headers_string +',n,birth_rate,sem,death_rate,sem,clearance_rate,sem,carrying_capacity,sem\n')

    err_list_file = open(cell_line+'_errors.txt','w')

    for condition in unique_conditions:
        # print('Now working with condition',condition)
        # filename = cell_line+'/'+str(condition).replace('[','').replace(']','').replace("' '",'_').replace("'",'')
        condition_string = str(condition).replace('[', '').replace(']', '').replace("' '", ',').replace("'", '')

        str_condition = cell_line + '_' + str(condition).replace('[', '').replace(']', '').replace(' ', '_').replace("'", '')
        str_condition = str_condition.replace("__", '_').replace("\n","")
        filename = cell_line + '/' + str_condition
        try:
            with open(filename+'/output/data_summary.csv') as f:
                f.readline()
                n = np.inf
                for line in f:
                    n = min(n,int(line.strip('\n').split(sep=',')[-1]))
            with open(filename+'/output/mape_summary.csv') as f:

                # print('\tUsing filename ',filename)

                f.readline()  # header line
                if sheet_type == 'Total+Viability':
                    live = f.readline().split(sep=',')  #  live model
                    live_logistic = f.readline().split(sep=',')  # live_logistic
                    total = f.readline().split(sep=',')  #  total model
                    total_logistic = f.readline().split(sep=',')  # total_logistic
                    live_dead = f.readline().split(sep=',')  #  live_dead model
                    live_dead_logistic = f.readline().split(sep=',')  #  live_dead_logistic model
                    # input(live_logistic)

                    # if ((float(live_logistic[4])>1) | (float(live_logistic[5])>1)): # if the net growth rate is larger than 1
                    #     pass
                    # else:
                    if True:
                        live_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                        live_summary.write(str(live[4]) + ',' + str(live[5]) + '\n')

                        live_logistic_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                        live_logistic_summary.write(str(live_logistic[4]) + ',' + str(live_logistic[5]) + ',' +
                                                    str(live_logistic[7]) + ',' + str(live_logistic[8]) + '\n')

                        total_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                        total_summary.write(str(total[4]) + ',' + str(total[5]) + '\n')

                        total_logistic_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                        total_logistic_summary.write(str(total_logistic[4]) + ',' + str(total_logistic[5]) + ',' +
                                                     str(total_logistic[7]) + ',' + str(total_logistic[8]) + '\n')

                        # if sheet_type == 'Total+Viability':
                        if True:
                            live_dead_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                            live_dead_summary.write(str(live_dead[4]) + ',' + str(live_dead[5]) + ',' +
                                                    str(live_dead[7]) + ',' + str(live_dead[8]) + ',' +
                                                    str(live_dead[10]) + ',' + str(live_dead[11]) + '\n')

                            live_dead_logistic_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                            live_dead_logistic_summary.write(str(live_dead_logistic[4]) + ',' + str(live_dead_logistic[5]) + ',' +
                                                    str(live_dead_logistic[7]) + ',' + str(live_dead_logistic[8]) + ',' +
                                                    str(live_dead_logistic[10]) + ',' + str(live_dead_logistic[11]) + ',' +
                                                    str(live_dead_logistic[13]) + ',' + str(live_dead_logistic[14]) + '\n')
                                                    # str(live_dead_logistic[16]) + ',' + str(live_dead_logistic[17]) + '\n')
                elif sheet_type == 'Total_Cells':
                    total = f.readline().split(sep=',')  #  total model
                    total_logistic = f.readline().split(sep=',')  # total_logistic
                    # if ((float(total_logistic[4])>1) | (float(total_logistic[5])>1)): # if the net growth rate is larger than 1
                    #     pass
                    # else:
                    if True:
                        total_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                        total_summary.write(str(total[4]) + ',' + str(total[5]) + '\n')

                        total_logistic_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                        total_logistic_summary.write(str(total_logistic[4]) + ',' + str(total_logistic[5]) + ',' +
                                                     str(total_logistic[7]) + ',' + str(total_logistic[8]) + '\n')

        except FileNotFoundError:
            print("Could not load file "+filename+'/output/mape_summary.csv'+" it probably doesn't exist")
            err_list_file.write(filename+'\n')

    total_summary.close()
    total_logistic_summary.close()
    if sheet_type == 'Total+Viability':
        live_dead_summary.close()
        live_dead_logistic_summary.close()

    err_list_file.close()

    index += 1


    # Start of plotting

    # total_summary.close()
    # total_logistic_summary.close()
    # if sheet_type == 'Total+Viability':
    #     live_summary.close()
    #     live_logistic_summary.close()
    #     live_dead_summary.close()
    #     live_dead_logistic_summary.close()
    #
    # # live_summary = cell_line + '/' + cell_line + '_live_summary' + '.txt'
    # # (line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,) = ReadSummary1(live_summary)
    #
    # total_summary = cell_line + '/' + cell_line + '_total_summary' + '.txt'
    # (line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,) = ReadSummary1(total_summary)
    #
    # fig, axes = plt.subplots(1, 3, sharex=False, sharey=True)
    # print(cell_line)
    # print(line_a)
    #
    # color_map = cmap.Set1
    # line_counter = 0
    # if ~multiplot_line:
    #     no_drug_shift = 0
    #     no_index = 0
    #
    #     low_drug_shift = 0
    #     low_index = 0
    #
    #     high_drug_shift = 0
    #     high_index = 0
    #
    # for drug in list(np.unique(drug_a)):
    #
    #     if drug == 'STATOXliplatin':
    #         pass
    #     else:
    #         if drug == 'Drug_B':
    #             drug_color = 'r'
    #             drug_label = 'Drug B'
    #         elif drug == 'Drug_A':
    #             drug_color = 'b'
    #             drug_label = 'Drug A'
    #         else:
    #             # print('something wrong happened with the color')
    #             drug_label = drug
    #
    #         indices = drug_a==drug
    #         temp_drug = concentration_a[indices]
    #         sorted_indices = np.argsort(temp_drug)
    #         temp_drug = concentration_a[indices][sorted_indices]
    #         temp_gr = gr_a[indices][sorted_indices]
    #         temp_sem = gr_sem_a[indices][sorted_indices]
    #
    #         # plt.errorbar(x=temp_drug,y=temp_gr,yerr=temp_sem,label=drug_label,color=drug_color)
    #         if multiplot_line:
    #             ebar = plt.errorbar(x=temp_drug,y=temp_gr,yerr=temp_sem,label=drug)
    #             e_color = ebar[0].get_color()
    #             plt.scatter(x=temp_drug,y=temp_gr,marker='.', color=e_color,s=50)
    #         elif CROSS:
    #             if temp_drug == 0:
    #                 axes[0].errorbar(x=temp_drug[0]+no_drug_shift, y=temp_gr[0], yerr=temp_sem[0], label=drug, fmt='.')
    #                 no_drug_shift += 0.01
    #             elif temp_drug == 0.5:
    #                 axes[1].errorbar(x=temp_drug[0]+low_drug_shift, y=temp_gr[0], yerr=temp_sem[0], label=drug, fmt='.')
    #                 low_drug_shift += 0.01
    #             elif temp_drug == 5:
    #                 axes[2].errorbar(x=temp_drug[0]+high_drug_shift, y=temp_gr[0], yerr=temp_sem[0], label=drug, fmt='.')
    #                 high_drug_shift += 0.01
    #
    #         else:
    #             if temp_drug == 0:
    #                 axes[0].errorbar(x=temp_drug+no_drug_shift, y=temp_gr, yerr=temp_sem, label=drug, marker='.', linestyle='', color=color_map(no_index))
    #                 no_drug_shift += 0.01
    #                 no_index += 0.1
    #
    #             elif temp_drug == 0.5:
    #                 axes[1].errorbar(x=temp_drug+low_drug_shift, y=temp_gr, yerr=temp_sem, label=drug, marker='.', linestyle='', color=color_map(low_index))
    #                 low_drug_shift += 0.01
    #                 low_index += 0.1
    #
    #             elif temp_drug == 5:
    #                 axes[2].errorbar(x=temp_drug+high_drug_shift, y=temp_gr, yerr=temp_sem, label=drug, marker='.', linestyle='', color=color_map(high_index))
    #                 high_drug_shift += 0.01
    #                 high_index += 0.1
    #             # plt.scatter(x=temp_drug, y=temp_gr, marker='.', s=50,label=drug)
    #         # print(temp_drug)
    #         # print(temp_gr)
    #         # print(temp_sem)
    #         # print(drug_a[indices][sorted_indices])
    #
    #         line_counter += 1
    #     # plt.gca().set_ylim(ymin=-0.1,ymax=0.1)
    #
    #     axes[0].set_ylabel('Net growth rate')
    #     # plt.gca().set_xlabel('Drug concentration')
    #     axes[0].set_xlabel('OXA=0')
    #     axes[1].set_xlabel('OXA=0.5')
    #     axes[2].set_xlabel('OXA=5.0')
    #
    #     axes[0].get_xaxis().set_ticks([])
    #     axes[1].get_xaxis().set_ticks([])
    #     axes[2].get_xaxis().set_ticks([])
    #
    #     axes[0].set_xlim(xmin=-0.01, xmax=0+no_drug_shift)
    #     axes[1].set_xlim(xmin=0.49, xmax=0.5+low_drug_shift)
    #     axes[2].set_xlim(xmin=4.99, xmax=5+high_drug_shift)
    #
    #     if multiplot_line:
    #         lgd1 = axes[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1,
    #                               handletextpad=0, columnspacing=0.1, borderpad=0.4)
    #         lgd2 = axes[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1,
    #                               handletextpad=0, columnspacing=0.1, borderpad=0.4)
    #         lgd3 = axes[2].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1,
    #                               handletextpad=0, columnspacing=0.1, borderpad=0.4)
    #     elif CROSS:
    #         lgd1 = axes[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1,
    #                               handletextpad=0, columnspacing=0.1, borderpad=0.4, title='Crosslinker')
    #         lgd2 = axes[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1,
    #                               handletextpad=0, columnspacing=0.1, borderpad=0.4, title='Crosslinker')
    #         lgd3 = axes[2].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1,
    #                               handletextpad=0, columnspacing=0.1, borderpad=0.4, title='Crosslinker')
    #     else:
    #         lgd1 = axes[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1, handletextpad=0, columnspacing=0.1, borderpad=0.4)
    #         lgd2 = axes[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1, handletextpad=0, columnspacing=0.1, borderpad=0.4)
    #         lgd3 = axes[2].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, ncol=1, handletextpad=0, columnspacing=0.1, borderpad=0.4)
    #
    #     title = fig.suptitle(line_a[0].replace('_disc', '').replace('01_', '1%O2_').replace('20_', '20%O2_')+' Experiment')
    #
    # # fig.savefig(line_a[0]+'_net_growth_rate.png', bbox_extra_artists=[lgd1, lgd2, lgd3, title,], bbox_inches='tight')
    # fig.savefig(line_a[0]+'_net_growth_rate.png', dpi=75, format='png', bbox_extra_artists=[lgd1, lgd2, title, ], bbox_inches='tight')
    #
    # plt.clf()
    #
    # if sheet_type == 'Total+Viability':
    #
    #     live_dead_logistic_summary = cell_line + '/' + cell_line + '_live_dead_logistic_summary' + '.txt'
    #     (line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a,cc_a,cc_sem_a,) = ReadSummary4(live_dead_logistic_summary)
    #
    #     # live_dead_summary = cell_line + '/' + cell_line + '_live_dead_summary' + '.txt'
    #     # (line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a,) = ReadSummary3(live_dead_summary)
    #
    #     color_map = cmap.Dark2
    #     line_counter = 0
    #     fig, axs = plt.subplots(1, 2, sharex=True, sharey=False)
    #     drug_color = 'k'
    #     for drug in list(np.unique(drug_a)):
    #         if drug == 'STATOXliplatin':
    #             pass
    #         else:
    #             if drug == 'Drug_B':
    #                 drug_color = 'r'
    #                 drug_label = 'Drug B'
    #             elif drug == 'Drug_A':
    #                 drug_color = 'b'
    #                 drug_label = 'Drug A'
    #             else:
    #                 # print('something wrong happened with the color')
    #                 drug_label = drug
    #
    #             indices = drug_a==drug
    #             temp_drug = concentration_a[indices]
    #             sorted_indices = np.argsort(temp_drug)
    #             temp_drug = concentration_a[indices][sorted_indices]
    #             temp_birth = live_a[indices][sorted_indices]
    #             temp_birth_sem = live_sem_a[indices][sorted_indices]
    #             temp_death = dead_a[indices][sorted_indices]
    #             temp_death_sem = dead_sem_a[indices][sorted_indices]
    #
    #             # print(drug)
    #             # print(temp_drug)
    #             # print(temp_birth)
    #
    #             # axs[0].errorbar(x=temp_drug,y=temp_birth,yerr=temp_birth_sem,label=drug_label,color=drug_color)
    #             # axs[1].errorbar(x=temp_drug,y=temp_death,yerr=temp_death_sem,label=drug_label,color=drug_color)
    #
    #             axs[0].errorbar(x=temp_drug, y=temp_birth, yerr=temp_birth_sem, label=drug)
    #             axs[1].errorbar(x=temp_drug, y=temp_death, yerr=temp_death_sem, label=drug)
    #
    #             line_counter += 1
    #
    #         # axs[0].set_ylim(ymin=-0.01,ymax=0.08)
    #         # axs[1].set_ylim(ymin=-0.001,ymax=0.02)
    #         axs[0].set_xlim(xmin=-0.1,xmax=1.1)
    #         axs[0].set_ylabel('birth rate')
    #         axs[1].set_ylabel('death rate')
    #
    #         axs[0].set_xlabel('Drug concentration')
    #         axs[1].set_xlabel('Drug concentration')
    #         # axs[0].set_xscale('symlog')
    #         # axs[1].set_xscale('symlog')
    #
    #         lgd = axs[0].legend(loc='upper center', bbox_to_anchor=(1, -0.2), fancybox=True, shadow=True, ncol=3)
    #         title = fig.suptitle(line_a[0] + ' cell line')
    #
    #
    #     plt.savefig(line_a[0]+'_live_dead.png', bbox_extra_artists=[lgd, title,], bbox_inches='tight')
    #     line_counter = 0
    #
    #     bfile = open('percent_error_birth.txt','w')
    #     dfile = open('percent_error_death.txt','w')
    #     gfile = open('percent_error_growth.txt','w')
    #     bfile.write('Drug\tConcentration\tTrue birth rate\tEstimate\tDifference\tPercent Error\n')
    #     dfile.write('Drug\tConcentration\tTrue death rate\tEstimate\tDifference\tPercent Error\n')
    #     gfile.write('Drug\tConcentration\tTrue net growth rate\tEstimate\tDifference\tPercent Error\n')
    #     for drug in list(np.unique(drug_a)):
    #
    #         if drug == 'STATOXliplatin':
    #             pass
    #         else:
    #             if drug == 'Drug_B':
    #                 drug_color = 'r'
    #                 drug_label = 'Drug B'
    #             elif drug == 'Drug_A':
    #                 drug_color = 'b'
    #                 drug_label = 'Drug A'
    #             else:
    #                 print('something wrong happened with the color')
    #                 drug_label = drug
    #             indices = drug_a == drug
    #             temp_drug = concentration_a[indices]
    #             sorted_indices = np.argsort(temp_drug)
    #             temp_drug = concentration_a[indices][sorted_indices]
    #             temp_birth = live_a[indices][sorted_indices]
    #             temp_birth_sem = live_sem_a[indices][sorted_indices]
    #             temp_death = dead_a[indices][sorted_indices]
    #             temp_death_sem = dead_sem_a[indices][sorted_indices]
    #
    #             if drug == 'Drug_A':
    #                 cytostaticity = 1.0
    #                 cytotoxicity = 0.2
    #             elif drug == 'Drug_B':
    #                 cytostaticity = 0.2
    #                 cytotoxicity = 1.0
    #             else:
    #                 cytostaticity = 0.0
    #                 cytotoxicity = 0.0
    #
    #             # axs[0].plot(temp_drug,5.5e-2*(1 - cytostaticity*temp_drug),linestyle=':',color=drug_color)
    #             # axs[1].plot(temp_drug,0.5e-2*(1 + cytotoxicity* temp_drug),linestyle=':',color=drug_color)
    #
    #             axs[0].plot(temp_drug, 5.5e-2 * (1 - cytostaticity * temp_drug), linestyle=':',)
    #             axs[1].plot(temp_drug, 0.5e-2 * (1 + cytotoxicity * temp_drug), linestyle=':', )
    #
    #             for ind in list(sorted_indices):
    #                 bfile.write(drug_label+'\t')
    #                 bfile.write(str(temp_drug[ind])+'\t')
    #                 temp_b = 5.5e-2*(1-cytostaticity*temp_drug[ind])
    #                 bfile.write(str(temp_b)+'\t')
    #                 bfile.write(str(temp_birth[ind])+'\t')
    #                 bfile.write(str(abs(temp_birth[ind]-temp_b))+'\t')
    #                 bfile.write(str(100*abs(temp_birth[ind]-temp_b)/temp_b)+'\n')
    #
    #                 dfile.write(drug_label+'\t')
    #                 dfile.write(str(temp_drug[ind])+'\t')
    #                 temp_d = 0.5e-2*(1 + cytotoxicity* temp_drug[ind])
    #                 dfile.write(str(temp_d)+'\t')
    #                 dfile.write(str(temp_death[ind])+'\t')
    #                 dfile.write(str(abs(temp_death[ind]-temp_d))+'\t')
    #                 dfile.write(str(100*abs(temp_death[ind]-temp_d)/temp_d)+'\n')
    #
    #                 gfile.write(drug_label+'\t')
    #                 gfile.write(str(temp_drug[ind])+'\t')
    #                 temp_g = temp_b - temp_d
    #                 temp_growth = temp_birth[ind] - temp_death[ind]
    #                 gfile.write(str(temp_g)+'\t')
    #                 gfile.write(str(temp_growth)+'\t')
    #                 gfile.write(str(abs(temp_growth-temp_g))+'\t')
    #                 gfile.write(str(100*abs(temp_growth-temp_g)/temp_g)+'\n')
    #
    #             line_counter += 1
    #     plt.savefig(line_a[0]+'_live_dead_supplementary.png', bbox_extra_artists=[lgd, title,], bbox_inches='tight')
    #
    #
    # if os.path.isfile("matplotlibrc"):
    #     os.remove("matplotlibrc")

    # end of plotting

else:
    print('"',conditions_type,'" is not a valid input for the file named"',file,', sheet "Metadata", cell B32. Please choose one of the provided options')

