<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.00356 </td><td>inf         </td><td>1/hours        </td><td>281        </td></tr>
<tr><td>carrying_capacity</td><td>3.54e+09</td><td>inf         </td><td>number of cells</td><td>2.82e-10   </td></tr>
<tr><td>seeding_cells    </td><td>1.83e+05</td><td>inf         </td><td>number of cells</td><td>5.47e-06   </td></tr>
</table>