# -*- mode: python -*-

block_cipher = None


a = Analysis(['CellPD.py'],
             pathex=['C:\\Users\\EdwinFrancisco\\Dropbox (USC WCC-CAMM)\\CellPD_dev'],
             binaries=None,
             datas=None,
             hiddenimports=['scipy.linalg.cython_blas', 'scipy.linalg.cython_lapack'],
             hookspath=None,
             runtime_hooks=None,
             excludes=None,
             win_no_prefer_redirects=None,
             win_private_assemblies=None,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='CellPD',
          debug=True,
          strip=None,
          upx=True,
          console=True )
