<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.0214  </td><td>0.00614     </td><td>1/hours        </td><td>46.8       </td></tr>
<tr><td>carrying_capacity</td><td>1.57e+06</td><td>4.32e+05    </td><td>number of cells</td><td>6.35e-07   </td></tr>
<tr><td>seeding_cells    </td><td>4.49e+04</td><td>2.08e+04    </td><td>number of cells</td><td>2.23e-05   </td></tr>
</table>