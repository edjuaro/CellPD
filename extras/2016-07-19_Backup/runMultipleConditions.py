CALL_CELLPD = True
CREATE_INPUTS = True

import shutil
shutil.copy("files/matplotlibrc", "matplotlibrc")

def iter_rows(ws):
    for row in ws.iter_rows():
        yield [cell.value for cell in row]

def copy_worksheet(ws_in,ws_out):
    for row in list(iter_rows(ws_in)):
        ws_out.append(row)

def file_len(fname, header=False):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    if header:
        return i
    else:
        return i + 1

def ReadSummary1(fname):
    file_length = file_len(fname, header=True)
    line_a = np.chararray(file_length, unicode=True, itemsize=9)
    drug_a = np.chararray(file_length, unicode=True, itemsize=15)
    concentration_a = np.zeros(file_length)
    n_a = np.zeros(file_length)
    gr_a = np.zeros(file_length)
    gr_sem_a = np.zeros(file_length)

    index = 0
    with open(fname) as f:
        f.readline()
        for row in f.readlines():
            temp = row.strip('\n').split(sep=',')
            line_a[index] = temp[0]
            drug_a[index] = temp[1]
            concentration_a[index] = temp[2]
            n_a[index] = temp[3]
            gr_a[index] = temp[4]
            gr_sem_a[index] = temp[5]
            index += 1
    return (line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,)

def ReadSummary2(fname):
    file_length = file_len(fname, header=True)
    line_a = np.chararray(file_length, unicode=True, itemsize=9)
    drug_a = np.chararray(file_length, unicode=True, itemsize=15)
    concentration_a = np.zeros(file_length)
    n_a = np.zeros(file_length)
    gr_a = np.zeros(file_length)
    gr_sem_a = np.zeros(file_length)
    cc_a = np.zeros(file_length)
    cc_sem_a = np.zeros(file_length)


    index = 0
    with open(fname) as f:
        f.readline()
        for row in f.readlines():
            temp = row.strip('\n').split(sep=',')
            line_a[index] = temp[0]
            drug_a[index] = temp[1]
            concentration_a[index] = temp[2]
            n_a[index] = temp[3]
            gr_a[index] = temp[4]
            gr_sem_a[index] = temp[5]
            cc_a[index] = temp[6]
            cc_sem_a[index] = temp[7]
            index += 1
    return (line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,cc_a,cc_sem_a,)

def ReadSummary3(fname):
    file_length = file_len(fname, header=True)
    line_a = np.chararray(file_length, unicode=True, itemsize=9)
    drug_a = np.chararray(file_length, unicode=True, itemsize=15)
    concentration_a = np.zeros(file_length)
    n_a = np.zeros(file_length)
    live_a = np.zeros(file_length)
    live_sem_a = np.zeros(file_length)
    dead_a = np.zeros(file_length)
    dead_sem_a = np.zeros(file_length)
    clear_a = np.zeros(file_length)
    clear_sem_a = np.zeros(file_length)


    index = 0
    with open(fname) as f:
        f.readline()
        for row in f.readlines():
            temp = row.strip('\n').split(sep=',')
            line_a[index] = temp[0]
            drug_a[index] = temp[1]
            concentration_a[index] = temp[2]
            n_a[index] = temp[3]
            live_a[index] = temp[4]
            live_sem_a[index] = temp[5]
            dead_a[index] = temp[6]
            dead_sem_a[index] = temp[7]
            clear_a[index] = temp[8]
            clear_sem_a[index] = temp[9]
            index += 1
    return (line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a)

def ReadSummary4(fname):
    file_length = file_len(fname, header=True)
    line_a = np.chararray(file_length, unicode=True, itemsize=9)
    drug_a = np.chararray(file_length, unicode=True, itemsize=15)
    concentration_a = np.zeros(file_length)
    n_a = np.zeros(file_length)
    live_a = np.zeros(file_length)
    live_sem_a = np.zeros(file_length)
    dead_a = np.zeros(file_length)
    dead_sem_a = np.zeros(file_length)
    clear_a = np.zeros(file_length)
    clear_sem_a = np.zeros(file_length)
    cc_a = np.zeros(file_length)
    cc_sem_a = np.zeros(file_length)

    index = 0
    with open(fname) as f:
        f.readline()
        for row in f.readlines():
            temp = row.strip('\n').split(sep=',')
            line_a[index] = temp[0]
            drug_a[index] = temp[1]
            concentration_a[index] = temp[2]
            n_a[index] = temp[3]
            live_a[index] = temp[4]
            live_sem_a[index] = temp[5]
            dead_a[index] = temp[6]
            dead_sem_a[index] = temp[7]
            clear_a[index] = temp[8]
            clear_sem_a[index] = temp[9]
            cc_a[index] = temp[10]
            cc_sem_a[index] = temp[11]
            index += 1
    return (line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a,cc_a,cc_sem_a,)

import sys
import argparse
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cmap

if not len(sys.argv) > 1:
    print("***************************************")
    print("***************************************")
    # file = input("Please type the name of the input file:")
    file = 'Synthetic'

else:
    parser = argparse.ArgumentParser()
    parser.add_argument("name", help="specify input file name")
    args = parser.parse_args()
    file = args.name
    print("Using input file name '", file, "' as selected")

if ('.xlsx' not in file) or ('.xls' not in file):
    if os.path.isfile(file + ".xlsx"):
        print("Adding '.xlsx' to the filename")
        file = file + ".xlsx"
    elif os.path.isfile(file + ".xls"):
        print("Adding '.xls' to the filename")
        file = file + ".xls"

cell_line = file.strip('.xlsx')

import numpy as np
from openpyxl import load_workbook, Workbook
from subprocess import call

print(file)

wb = load_workbook(filename=file)
time_units = wb.get_sheet_by_name("Preferences").cell("B8").value
sheet_type = wb.get_sheet_by_name("Metadata").cell("B29").value
ws = wb.get_sheet_by_name(sheet_type)

if sheet_type == 'Total+Viability':
    min_number_of_columns = 5
elif sheet_type == 'Total_Cells':
    min_number_of_columns = 4
else:
    print(sheet_type,'is not currently supported by CellPD. Goodbye!')
    min_number_of_columns = None
    exit()

number_of_rows = ws.max_row - 1
first_row = True
second_row = True
extra_cols = np.empty(0)
for row in list(iter_rows(ws)):
    if first_row:
        headers = row
        extra_cols_headers = headers[min_number_of_columns:len(row)]
        first_row = False
        continue
    if second_row:
        extra_cols = np.asarray(row[min_number_of_columns:len(row)])
        second_row = False
        continue
    temp_cols = np.asarray(row[min_number_of_columns:len(row)])
    extra_cols = np.vstack((extra_cols,temp_cols))

unique_conditions = np.vstack({tuple(row) for row in extra_cols})

if CREATE_INPUTS:
    # Now go though each of the unique conditions, create a different CellPD file in the folder named after the input file
    if not os.path.exists(cell_line+'/'):
            os.makedirs(cell_line+'/')
    for condition in unique_conditions:
        print(condition)
        wb = load_workbook(filename=file)

        out_wb = Workbook()
        out_ws = out_wb.active
        out_ws.title = 'Metadata'
        copy_worksheet(wb.get_sheet_by_name('Metadata'),out_ws)
        out_ws.cell('B32').value = 'Single environmental condition'

        out_ws = out_wb.create_sheet(title='Preferences')
        copy_worksheet(wb.get_sheet_by_name('Preferences'),out_ws)
        out_ws.cell('B9').value = cell_line+'/'+str(condition).replace('[','').replace(']','').replace("' '",'_').replace("'",'')

        ws = wb.get_sheet_by_name(sheet_type)
        out_ws = out_wb.create_sheet(title=sheet_type)
        first_row = True
        for row in list(iter_rows(ws)):
            if first_row:
                out_ws.append(row)
                first_row = False
                continue
            test_index = 0
            test = True
            for test_columns in row[min_number_of_columns:len(row)]:

                test = test and (str(test_columns) == str(condition[test_index]))
                test_index += 1
            if test:
                out_ws.append(row)
                # print(row)
        out_wb.save(filename=cell_line+'/'+cell_line+'_'+str(condition).replace('[','').replace(']','').replace("' '",'_').replace("'",'')+'.xlsx')

if CALL_CELLPD:
    # Then call CellPD on each of those
    i = 1
    for condition in unique_conditions:
        if condition[0] == 'STATOXliplatin':
            pass
        else:
            filename = cell_line+'/'+cell_line+'_'+str(condition).replace('[','').replace(']','').replace("' '",'_').replace("'",'')+'.xlsx'
            print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            print('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
            print('Condition',i,'of',len(unique_conditions))
            print('Working with filename',filename)
            print('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
            print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            call('python CellPD.py '+filename)
            i += 1

# Then concatenate those reports to present summary of net growth rate

index = 0
headers_string = str(extra_cols_headers).replace('[', '').replace(']', '').replace("' '", '').replace(', ', ',').replace("'", '')

live_summary = open(cell_line + '/' + cell_line + '_live_summary' + '.txt', 'w')
live_summary.write('line,'+ headers_string + ',n,net_gr,sem\n')

live_logistic_summary = open(cell_line + '/' + cell_line + '_live_logistic_summary' + '.txt', 'w')
live_logistic_summary.write('line,'+ headers_string +',n,net_gr,sem,carrying_capacity,sem\n')

total_summary = open(cell_line + '/' + cell_line + '_total_summary' + '.txt', 'w')
total_summary.write('line,'+ headers_string + ',n,net_gr,sem\n')

total_logistic_summary = open(cell_line + '/' + cell_line + '_total_logistic_summary' + '.txt', 'w')
total_logistic_summary.write('line,'+ headers_string +',n,net_gr,sem,carrying_capacity,sem\n')

if sheet_type == 'Total+Viability':
    live_dead_summary = open(cell_line + '/' + cell_line + '_live_dead_summary' + '.txt', 'w')
    live_dead_summary.write('line,'+ headers_string + ',n,birth_rate,sem,death_rate,sem,clearance_rate,sem\n')

    live_dead_logistic_summary = open(cell_line + '/' + cell_line + '_live_dead_logistic_summary' + '.txt', 'w')
    live_dead_logistic_summary.write('line,'+ headers_string +',n,birth_rate,sem,death_rate,sem,clearance_rate,sem,carrying_capacity,sem\n')

for condition in unique_conditions:
    filename = cell_line+'/'+str(condition).replace('[','').replace(']','').replace("' '",'_').replace("'",'')
    condition_string = str(condition).replace('[', '').replace(']', '').replace("' '", ',').replace("'", '')
    try:
        with open(filename+'/output/data_summary.csv') as f:
            f.readline()
            n = np.inf
            for line in f:
                n = min(n,int(line.strip('\n').split(sep=',')[7]))
        with open(filename+'/output/mape_summary.csv') as f:
            f.readline()  # header line
            live = f.readline().split(sep=',')  #  live model
            live_logistic = f.readline().split(sep=',')  # live_logistic
            total = f.readline().split(sep=',')  #  total model
            total_logistic = f.readline().split(sep=',')  # total_logistic
            # if sheet_type == 'Total+Viability':
            if True:
                live_dead = f.readline().split(sep=',')  #  live_dead model
                live_dead_logistic = f.readline().split(sep=',')  #  live_dead_logistic model
            # input(live_logistic)

            if ((float(live_logistic[4])>1) | (float(live_logistic[5])>1)): # if the net growth rate is larger than 1
                pass
            else:
                live_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                live_summary.write(str(live[4]) + ',' + str(live[5]) + '\n')

                live_logistic_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                live_logistic_summary.write(str(live_logistic[4]) + ',' + str(live_logistic[5]) + '\n')

                total_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                total_summary.write(str(total[4]) + ',' + str(total[5]) + '\n')

                total_logistic_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                total_logistic_summary.write(str(total_logistic[4]) + ',' + str(total_logistic[5]) + '\n')

                # if sheet_type == 'Total+Viability':
                if True:
                    live_dead_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                    live_dead_summary.write(str(live_dead[4]) + ',' + str(live_dead[5]) + ',' +
                                            str(live_dead[7]) + ',' + str(live_dead[8]) + ',' +
                                            str(live_dead[10]) + ',' + str(live_dead[11]) +'\n')

                    live_dead_logistic_summary.write(cell_line + ',' + condition_string + ',' + str(n) + ',')
                    live_dead_logistic_summary.write(str(live_dead_logistic[4]) + ',' + str(live_dead_logistic[5]) + ',' +
                                            str(live_dead_logistic[7]) + ',' + str(live_dead_logistic[8]) + ',' +
                                            str(live_dead_logistic[10]) + ',' + str(live_dead_logistic[11]) + ',' +
                                            str(live_dead_logistic[13]) + ',' + str(live_dead_logistic[14]) + '\n')
    except FileNotFoundError:
        print("Could not load fine "+filename+'/output/mape_summary.csv'+" it probably doesn't exist")

index += 1
live_summary.close()
live_logistic_summary.close()
total_summary.close()
total_logistic_summary.close()
live_dead_summary.close()
live_dead_logistic_summary.close()

# live_summary = cell_line + '/' + cell_line + '_live_summary' + '.txt'
# (line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,) = ReadSummary1(live_summary)

total_summary = cell_line + '/' + cell_line + '_total_summary' + '.txt'
(line_a,drug_a,concentration_a,n_a,gr_a,gr_sem_a,) = ReadSummary1(total_summary)

color_map = cmap.Dark2
line_counter = 0
for drug in list(np.unique(drug_a)):

    if drug == 'STATOXliplatin':
        pass
    else:
        if drug == 'Drug_B':
            drug_color = 'r'
            drug_label = 'Drug B'
        elif drug == 'Drug_A':
            drug_color = 'b'
            drug_label = 'Drug A'
        else:
            print('something wrong happened with the color')

        indices = drug_a==drug
        temp_drug = concentration_a[indices]
        sorted_indices = np.argsort(temp_drug)
        temp_drug = concentration_a[indices][sorted_indices]
        temp_gr = gr_a[indices][sorted_indices]
        temp_sem = gr_sem_a[indices][sorted_indices]

        plt.errorbar(x=temp_drug,y=temp_gr,yerr=temp_sem,label=drug_label,color=drug_color)
        # print(temp_drug)
        # print(temp_gr)
        # print(temp_sem)
        # print(drug_a[indices][sorted_indices])

        line_counter += 1
    # plt.gca().set_ylim(ymin=-0.1,ymax=0.1)
    plt.gca().set_ylabel('Net growth rate')
    plt.gca().set_xlabel('Drug concentration')
    plt.gca().set_xlim(xmin=-0.1,xmax=0.9)
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2), fancybox=True, shadow=True, ncol=2)
    title = plt.suptitle(line_a[0]+' cell line')


plt.savefig(line_a[0]+'_net_growth_rate.png', bbox_extra_artists=[lgd, title,], bbox_inches='tight')

plt.clf()

live_dead_logistic_summary = cell_line + '/' + cell_line + '_live_dead_logistic_summary' + '.txt'
(line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a,cc_a,cc_sem_a,) = ReadSummary4(live_dead_logistic_summary)

# live_dead_summary = cell_line + '/' + cell_line + '_live_dead_summary' + '.txt'
# (line_a,drug_a,concentration_a,n_a,live_a,live_sem_a,dead_a,dead_sem_a,clear_a,clear_sem_a,) = ReadSummary3(live_dead_summary)

color_map = cmap.Dark2
line_counter = 0
fig, axs = plt.subplots(1, 2, sharex=True, sharey=False)
drug_color = 'k'
for drug in list(np.unique(drug_a)):
    if drug == 'STATOXliplatin':
        pass
    else:
        if drug == 'Drug_B':
            drug_color = 'r'
            drug_label = 'Drug B'
        elif drug == 'Drug_A':
            drug_color = 'b'
            drug_label = 'Drug A'
        else:
            print('something wrong happened with the color')

        indices = drug_a==drug
        temp_drug = concentration_a[indices]
        sorted_indices = np.argsort(temp_drug)
        temp_drug = concentration_a[indices][sorted_indices]
        temp_birth = live_a[indices][sorted_indices]
        temp_birth_sem = live_sem_a[indices][sorted_indices]
        temp_death = dead_a[indices][sorted_indices]
        temp_death_sem = dead_sem_a[indices][sorted_indices]

        # print(drug)
        # print(temp_drug)
        # print(temp_birth)

        axs[0].errorbar(x=temp_drug,y=temp_birth,yerr=temp_birth_sem,label=drug_label,color=drug_color)
        axs[1].errorbar(x=temp_drug,y=temp_death,yerr=temp_death_sem,label=drug_label,color=drug_color)

        line_counter += 1

    # axs[0].set_ylim(ymin=-0.01,ymax=0.08)
    # axs[1].set_ylim(ymin=-0.001,ymax=0.02)
    axs[0].set_xlim(xmin=-0.1,xmax=1.1)
    axs[0].set_ylabel('birth rate')
    axs[1].set_ylabel('death rate')

    axs[0].set_xlabel('Drug concentration')
    axs[1].set_xlabel('Drug concentration')
    # axs[0].set_xscale('symlog')
    # axs[1].set_xscale('symlog')

    lgd = axs[0].legend(loc='upper center', bbox_to_anchor=(1, -0.2), fancybox=True, shadow=True, ncol=3)
    title = fig.suptitle(line_a[0] + ' cell line')


plt.savefig(line_a[0]+'_live_dead.png', bbox_extra_artists=[lgd, title,], bbox_inches='tight')
line_counter = 0

bfile = open('percent_error_birth.txt','w')
dfile = open('percent_error_death.txt','w')
gfile = open('percent_error_growth.txt','w')
bfile.write('Drug\tConcentration\tTrue birth rate\tEstimate\tDifference\tPercent Error\n')
dfile.write('Drug\tConcentration\tTrue death rate\tEstimate\tDifference\tPercent Error\n')
gfile.write('Drug\tConcentration\tTrue net growth rate\tEstimate\tDifference\tPercent Error\n')
for drug in list(np.unique(drug_a)):

    if drug == 'STATOXliplatin':
        pass
    else:
        if drug == 'Drug_B':
            drug_color = 'r'
            drug_label = 'Drug B'
        elif drug == 'Drug_A':
            drug_color = 'b'
            drug_label = 'Drug A'
        else:
            print('something wrong happened with the color')
        indices = drug_a == drug
        temp_drug = concentration_a[indices]
        sorted_indices = np.argsort(temp_drug)
        temp_drug = concentration_a[indices][sorted_indices]
        temp_birth = live_a[indices][sorted_indices]
        temp_birth_sem = live_sem_a[indices][sorted_indices]
        temp_death = dead_a[indices][sorted_indices]
        temp_death_sem = dead_sem_a[indices][sorted_indices]

        if drug == 'Drug_A':
            cytostaticity = 1.0
            cytotoxicity = 0.2
        elif drug == 'Drug_B':
            cytostaticity = 0.2
            cytotoxicity = 1.0

        axs[0].plot(temp_drug,5.5e-2*(1 - cytostaticity*temp_drug),linestyle=':',color=drug_color)
        axs[1].plot(temp_drug,0.5e-2*(1 + cytotoxicity* temp_drug),linestyle=':',color=drug_color)

        for ind in list(sorted_indices):
            bfile.write(drug_label+'\t')
            bfile.write(str(temp_drug[ind])+'\t')
            temp_b = 5.5e-2*(1-cytostaticity*temp_drug[ind])
            bfile.write(str(temp_b)+'\t')
            bfile.write(str(temp_birth[ind])+'\t')
            bfile.write(str(abs(temp_birth[ind]-temp_b))+'\t')
            bfile.write(str(100*abs(temp_birth[ind]-temp_b)/temp_b)+'\n')

            dfile.write(drug_label+'\t')
            dfile.write(str(temp_drug[ind])+'\t')
            temp_d = 0.5e-2*(1 + cytotoxicity* temp_drug[ind])
            dfile.write(str(temp_d)+'\t')
            dfile.write(str(temp_death[ind])+'\t')
            dfile.write(str(abs(temp_death[ind]-temp_d))+'\t')
            dfile.write(str(100*abs(temp_death[ind]-temp_d)/temp_d)+'\n')

            gfile.write(drug_label+'\t')
            gfile.write(str(temp_drug[ind])+'\t')
            temp_g = temp_b - temp_d
            temp_growth = temp_birth[ind] - temp_death[ind]
            gfile.write(str(temp_g)+'\t')
            gfile.write(str(temp_growth)+'\t')
            gfile.write(str(abs(temp_growth-temp_g))+'\t')
            gfile.write(str(100*abs(temp_growth-temp_g)/temp_g)+'\n')

        line_counter += 1
plt.savefig(line_a[0]+'_live_dead_supplementary.png', bbox_extra_artists=[lgd, title,], bbox_inches='tight')

if os.path.isfile("matplotlibrc"):
    os.remove("matplotlibrc")