<table>
<tr><th>Parameter         </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>birth_rate        </td><td>0.113   </td><td>0           </td><td>1/hours        </td><td>8.81       </td></tr>
<tr><td>death_rate        </td><td>9.77e-05</td><td>0           </td><td>1/hours        </td><td>1.02e+04   </td></tr>
<tr><td>clearance_rate    </td><td>0.152   </td><td>0           </td><td>1/hours        </td><td>6.59       </td></tr>
<tr><td>seeding_cells_live</td><td>21.7    </td><td>0           </td><td>number of cells</td><td>0.0462     </td></tr>
<tr><td>seeding_cells_dead</td><td>2       </td><td>0           </td><td>number of cells</td><td>0.5        </td></tr>
</table>