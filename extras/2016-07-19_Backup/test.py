for time in linspace(0,end_time):
    # Sample an n-dimensional Gaussian RV
    for i in span(n):
        gen_power[i] = Gauss(0, sigma[i])
    nodes[time] = IEEE(14,gen_power)
#Test gor Gaussianness
Gauss_test(nodes)