<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.04    </td><td>0.000393    </td><td>1/hours        </td><td>25         </td></tr>
<tr><td>carrying_capacity</td><td>6.48e+04</td><td>4.17e+03    </td><td>number of cells</td><td>1.54e-05   </td></tr>
</table>