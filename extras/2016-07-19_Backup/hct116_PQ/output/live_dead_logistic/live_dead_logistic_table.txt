parameter,value,std. err.,units,invval
birth_rate,0.181,0,1/hours,5.52
death_rate,7.38e-05,0,1/hours,1.35e+04
clearance_rate,0.127,0,1/hours,7.89
carrying_capacity,5.2e+04,0,number of cells,1.92e-05
seeding_cells_live,10.6,0,number of cells,0.0944
seeding_cells_dead,2,0,number of cells,0.5
