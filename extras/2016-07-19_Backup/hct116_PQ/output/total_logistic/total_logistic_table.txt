parameter,value,std. err.,units,invval
growth_rate,0.0377,0.00125,1/hours,26.5
carrying_capacity,1.63e+05,3.63e+04,number of cells,6.15e-06
seeding_cells,5.36e+03,123,number of cells,0.000187
