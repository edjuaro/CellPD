<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.00473 </td><td>inf         </td><td>1/hours        </td><td>211        </td></tr>
<tr><td>carrying_capacity</td><td>1.76e+07</td><td>inf         </td><td>number of cells</td><td>5.69e-08   </td></tr>
<tr><td>seeding_cells    </td><td>1.28e+04</td><td>inf         </td><td>number of cells</td><td>7.79e-05   </td></tr>
</table>