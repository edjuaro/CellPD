def file_len(fname, header=False):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    if header:
        return i
    else:
        return i + 1

import csv
import numpy as np
from subprocess import call
from openpyxl import load_workbook
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cmap
from matplotlib import gridspec


CREATE_INPUT_FILES = False
RUN_CELLPD = False
CONCATENATE_REPORTS = False
VARY_DRUG = False
VARY_OXYGEN = False
THREE_D_PLOTS = True

n_rows = file_len('_Experiment_concatenation.csv', header=True)

cell_line = np.zeros(n_rows,dtype='S6')
o2 = np.zeros(n_rows)
oxa = np.zeros(n_rows)
bio_id = np.zeros(n_rows,dtype='S10')
tech_id = np.zeros(n_rows,dtype='S4')
days = np.zeros(n_rows)
total = np.zeros(n_rows)
live = np.zeros(n_rows)

with open('_Experiment_concatenation.csv', newline='') as csvfile:
    csvfile.readline()  #This effectively removes the header
    all_data = csv.reader(csvfile, delimiter=',')
    i = 0
    for row in all_data:
        cell_line[i] = row[1]
        o2[i] = row[20]
        oxa[i] = row[21]
        bio_id[i] = row[15]
        tech_id[i] = row[16]
        days[i] = row[17]
        total[i] = row[18]
        live[i] = row[19]
        i += 1

lines = np.unique(cell_line)
oxygen_cons = np.unique(o2)
drug_cons = np.unique(oxa)

if CREATE_INPUT_FILES:
    # Create input files for CellPD
    print('Creating the input files for CellPD [this ought to take a few seconds]')
    for (current_line,current_o2,current_oxa) in [(current_line,current_o2,current_oxa) for current_line in lines for current_o2 in oxygen_cons for current_oxa in drug_cons]:
        indices = (cell_line ==current_line) & (o2==current_o2) & (oxa == current_oxa)
        input_filename = '_template.xlsx'
        dest_filename = 'CellPD_inputs_outputs/'+str(current_line,encoding='utf-8')+'_o2='+str(current_o2)+'_oxa='+str(current_oxa)
        wb = load_workbook(filename=input_filename)
        wb.get_sheet_by_name("Preferences").cell("B9").value = dest_filename
        wb.get_sheet_by_name("Metadata").cell("D11").value = str(current_line,encoding='utf-8')+'_o2='+str(current_o2)+'_oxa='+str(current_oxa)
        wb.get_sheet_by_name("Metadata").cell("D12").value = str(current_line,encoding='utf-8')
        if current_line == b'Caco2':
            wb.get_sheet_by_name("Metadata").cell("D16").value = '0002172'
            wb.get_sheet_by_name("Metadata").cell("D17").value = '0000195'
        elif current_line == b'HCT116':
            wb.get_sheet_by_name("Metadata").cell("D16").value = '0003665'
            wb.get_sheet_by_name("Metadata").cell("D17").value = '0001109'
        elif current_line == b'HT29':
            wb.get_sheet_by_name("Metadata").cell("D16").value = '0004283'
            wb.get_sheet_by_name("Metadata").cell("D17").value = '0000182'
        else:
            print("This should have not happened!", current_line)

        wb.get_sheet_by_name("Metadata").cell("D23").value = current_o2/100.0
        wb.get_sheet_by_name("Metadata").cell("D25").value = str(current_line,encoding='utf-8')+' Cells, grown in '+ str(current_o2) + '% O2 treated with '+ str(current_oxa) +'uM of Oxaloplatin.'
        index = 0
        for time_point in days[indices]:
            wb.get_sheet_by_name("Total+Viability").append([str(bio_id[indices][index],encoding='utf-8'), str(tech_id[indices][index],encoding='utf-8'),days[indices][index],total[indices][index],live[indices][index]])
            index += 1
            # wb.get_sheet_by_name("Total+Viability").cell("A2").value = str(bio_id[indices],

        wb.save(filename = dest_filename+'.xlsx')

if RUN_CELLPD:
    # Now call CellPD for each of these cases:
    print('Calling CellPD for each environmental condition [this ought to take about a minute per call]')
    for (current_line,current_o2,current_oxa) in [(current_line,current_o2,current_oxa) for current_line in lines for current_o2 in oxygen_cons for current_oxa in drug_cons]:

        filename = 'CellPD_inputs_outputs/'+str(current_line,encoding='utf-8')+'_o2='+str(current_o2)+'_oxa='+str(current_oxa)
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
        print('Working with filename',filename)
        print('||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||')
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        call('python CellPD.py '+filename)

o2_dic = {
    0.1 : 'b',
    1.0 : 'r',
    5.0 : 'g',
    20.0: 'k',
}

if CONCATENATE_REPORTS:
    # fig, axs = plt.subplots(1, number_of_subplots, sharex=True)
    # Now concatenate information
    print('Calling CellPD for each environmental condition [this ought to take about a minute per call]')
    # for (current_line,current_o2,current_oxa) in [(current_line,current_o2,current_oxa) for current_line in lines for current_o2 in oxygen_cons for current_oxa in drug_cons]:
    # for (current_line,current_o2,current_oxa) in [(current_line,current_o2,current_oxa) for current_line in lines for current_o2 in oxygen_cons for current_oxa in drug_cons]:
    for current_line in lines:
        # fig, axs = plt.subplots(1, 4, sharex=True, sharey='row',)
        index = 0
        summary = open(str(current_line,encoding='utf-8')+'.txt','w')
        summary.write('line,O2,Oxaloplatin,net_gr,sem,n\n')
        for current_o2 in oxygen_cons:
            for current_oxa in drug_cons:
                filename = 'CellPD_inputs_outputs/'+str(current_line,encoding='utf-8')+'_o2='+str(current_o2)+'_oxa='+str(current_oxa)
                try:
                    with open(filename+'/output/data_summary.csv') as f:
                        f.readline()
                        n = np.inf
                        for line in f:
                            n = min(n,int(line.strip('\n').split(sep=',')[7]))
                    with open(filename+'/output/mape_summary.csv') as f:
                        f.readline()  # header line
                        # f.readline()  #  live model
                        live_logistic = f.readline().split(sep=',')  # live_logistic
                        if ((float(live_logistic[4])>1) | (float(live_logistic[5])>1)):
                            pass
                        else:
                            # print(live_logistic[4],live_logistic[5])
                            # axs[index].errorbar(current_oxa,float(live_logistic[4]),yerr=float(live_logistic[5]),label=filename,fmt='.-'+o2_dic[float(current_o2)])
                            # axs[1,index].errorbar(current_oxa,float(live_logistic[7]),yerr=float(live_logistic[8]),label=filename,fmt='.-'+o2_dic[float(current_o2)])
                            # plt.legend()
                            summary.write(str(current_line,encoding='utf-8')+','+str(current_o2)+','+str(current_oxa)+','+str(live_logistic[4])+','+str(live_logistic[5])+','+str(n)+'\n')
                except FileNotFoundError:
                    print("Could not load fine "+filename+'/output/mape_summary.csv'+" it probably doesn't exist")
            index += 1
        summary.close()





    # for current_line in lines:
    #         fig, axs = plt.subplots(1, 6, sharex=True, sharey='row')
    #
    #         for current_o2 in oxygen_cons:
    #             index = 0
    #             for current_oxa in drug_cons:
    #                 filename = 'CellPD_inputs_outputs/'+str(current_line,encoding='utf-8')+'_o2='+str(current_o2)+'_oxa='+str(current_oxa)
    #                 try:
    #                     with open(filename+'/output/data_summary.csv') as f:
    #                         f.readline()
    #                         n = np.inf
    #                         for line in f:
    #                             n = min(n,int(line.strip('\n').split(sep=',')[7]))
    #                     with open(filename+'/output/mape_summary.csv') as f:
    #                         f.readline()  # header line
    #                         # f.readline()  #  live model
    #                         live_logistic = f.readline().split(sep=',')  # live_logistic
    #                         if ((float(live_logistic[4])>1) | (float(live_logistic[5])>1)):
    #                             pass
    #                         else:
    #                             # print(live_logistic[4],live_logistic[5])
    #                             # axs[index].errorbar(current_o2,float(live_logistic[4]),yerr=float(live_logistic[5]),label=filename,fmt='.-'+o2_dic[float(current_o2)])
    #                             # axs[1,index].errorbar(current_oxa,float(live_logistic[7]),yerr=float(live_logistic[8]),label=filename,fmt='.-'+o2_dic[float(current_o2)])
    #                             # plt.legend()
    #                 except FileNotFoundError:
    #                     print("Could not load fine "+filename+'/output/mape_summary.csv'+" it probably doesn't exist")
    #                 index += 1


o2 = np.zeros((3,24))
oxa = np.zeros((3,24))
gr = np.zeros((3,24))
sem = np.zeros((3,24))
n = np.zeros((3,24))


line_counter = 0
for current_line in lines:
    summary = open(str(current_line,encoding='utf-8')+'.txt')
    summary.readline()
    index = 0
    for line in summary:
        o2[line_counter,index] = line.strip('\n').split(sep=',')[1]
        oxa[line_counter,index] = line.strip('\n').split(sep=',')[2]
        gr[line_counter,index] = line.strip('\n').split(sep=',')[3]
        sem[line_counter,index] = line.strip('\n').split(sep=',')[4]
        n[line_counter,index] = line.strip('\n').split(sep=',')[5]
        index += 1
    #
    # ax.plt()
    # plt.show()
    line_counter += 1

color_map = cmap.Paired

if VARY_DRUG:
    line_counter = 0
    for current_line in lines:
        fig = plt.figure(figsize=(6, 6))
        ax = plt.subplot2grid((3,1), (0,0), rowspan=2)
        sp = plt.subplot2grid((3,1), (2,0), rowspan=1,sharex=ax)
        oxy_counter = 0
        for oxygen in np.unique(o2):
            o2_index = o2[line_counter]==oxygen
            # print(o2[line_counter,o2_index])
            ax.errorbar(x=oxa[line_counter,o2_index],y=gr[line_counter,o2_index],yerr=sem[line_counter,o2_index],fmt='.-',color=color_map(oxy_counter/len(np.unique(o2))),label='{:.2g}'.format(oxygen)+'% $O_2$')
            sp.errorbar(x=oxa[line_counter,o2_index],y=n[line_counter,o2_index],color=color_map(oxy_counter/len(np.unique(o2))))
            oxy_counter +=1
        ax.hlines(y=0,xmin=-1e-2,xmax=6,linestyles='dashed')

        ax.set_xscale('symlog',linthreshx=0.15)
        ticks = np.unique(oxa)
        ax.set_xticks(ticks)
        ax.set_xticklabels(ticks)
        ax.set_xlabel('Oxaloplatin [uM]')
        ax.set_xlim(xmin=-1e-2,xmax=6)
        ax.set_ylabel('Net Growth Rate (±SEM)')
        lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        # lgd = ax.legend(loc='best', fancybox=True, shadow=True)
        title = ax.set_title(str(current_line,encoding='utf-8'))

        sp.set_yticks([1,2,3,4])
        sp.set_yticklabels([1,2,3,4])
        sp.set_ylim(ymin=1,ymax=4.1)
        sp.set_ylabel('No. of bio. reps.')

        fig.savefig(str(current_line,encoding='utf-8')+'x=oxa',bbox_extra_artists=[lgd, title, ], bbox_inches='tight')
        plt.close()
        line_counter += 1

if VARY_OXYGEN:
    line_counter = 0
    for current_line in lines:
        fig = plt.figure(figsize=(6, 6))
        ax = plt.subplot2grid((3,1), (0,0), rowspan=2)
        sp = plt.subplot2grid((3,1), (2,0), rowspan=1,sharex=ax)
        oxa_counter = 0
        for drug in np.unique(oxa):
            oxa_index = oxa[line_counter]==drug
            # print(oxa[line_counter,oxa_index])
            ax.errorbar(x=o2[line_counter,oxa_index],y=gr[line_counter,oxa_index],yerr=sem[line_counter,oxa_index],fmt='.-',color=color_map(oxa_counter/len(np.unique(oxa))),label='{:.2g}'.format(drug)+'[uM]')
            sp.errorbar(x=o2[line_counter,oxa_index],y=n[line_counter,oxa_index],color=color_map(oxa_counter/len(np.unique(oxa))))
            oxa_counter +=1
        ax.hlines(y=0,xmin=0,xmax=21,linestyles='dashed')
    
        ax.set_xscale('symlog')
        ticks = np.unique(o2)
        ax.set_xticks(ticks)
        ax.set_xticklabels(ticks)
        ax.set_xlabel('% $O_2$')
        ax.set_xlim(xmin=0,xmax=21)
        ax.set_ylabel('Net Growth Rate (±SEM)')
        lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        # lgd = ax.legend(loc='best', fancybox=True, shadow=True)
        title = ax.set_title(str(current_line,encoding='utf-8'))
    
        sp.set_yticks([1,2,3,4])
        sp.set_yticklabels([1,2,3,4])
        sp.set_ylim(ymin=1,ymax=4.1)
        sp.set_ylabel('No. of bio. reps.')
    
        fig.savefig(str(current_line,encoding='utf-8')+'x=oxygen',bbox_extra_artists=[lgd, title, ], bbox_inches='tight')
        plt.close()
        line_counter += 1

if THREE_D_PLOTS:
    fig = plt.figure(figsize=(6, 6))
    ax = fig.gca(projection='3d')
    line_counter = 0

    for current_line in lines:

        ax.scatter(o2[line_counter,:],oxa[line_counter,:],gr[line_counter,:],color=color_map(line_counter/3),c=color_map(line_counter/3),label=str(current_line,encoding='utf-8'))

        ax.set_xscale('symlog')
        ticks = np.unique(o2)
        ax.set_xticks(ticks)
        ax.set_xticklabels(ticks)
        ax.set_xlabel('% $O_2$')
        ax.set_xlim(xmin=0,xmax=21)

        ax.set_yscale('symlog',linthreshx=0.15)
        ticks = np.unique(oxa)
        ax.set_yticks(ticks)
        ax.set_yticklabels(ticks)
        ax.set_ylabel('Oxaloplatin [uM]')
        ax.set_ylim(ymin=-1e-2,ymax=6)

        ax.set_zlabel('Net Growth Rate (±SEM)')
        lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        title = ax.set_title('3D!')

        # sp.set_yticks([1,2,3,4])
        # sp.set_yticklabels([1,2,3,4])
        # sp.set_ylim(ymin=1,ymax=4.1)
        # sp.set_ylabel('No. of bio. reps.')
        line_counter += 1

    fig.savefig('3D',bbox_extra_artists=[lgd, title, ], bbox_inches='tight')
    plt.show()
    plt.close()
