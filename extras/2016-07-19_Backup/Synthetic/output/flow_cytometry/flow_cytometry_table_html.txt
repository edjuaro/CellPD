<table>
<tr><th>Parameter                             </th><th>  Value</th><th>  Std. Error</th><th>units  </th><th>  Inv_value</th></tr>
<tr><td>r<sub>G<sub>0</sub>G<sub>1</sub></sub></td><td>0.614  </td><td>0.178       </td><td>1/hours</td><td>1.63       </td></tr>
<tr><td>r<sub>s</sub>                         </td><td>0.825  </td><td>0.239       </td><td>1/hours</td><td>1.21       </td></tr>
<tr><td>r<sub>G<sub>2</sub>M</sub>            </td><td>1.15   </td><td>0.332       </td><td>1/hours</td><td>0.871      </td></tr>
</table>