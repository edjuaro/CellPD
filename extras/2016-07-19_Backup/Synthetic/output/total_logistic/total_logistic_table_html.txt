<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.0405  </td><td>0.000211    </td><td>1/hours        </td><td>24.7       </td></tr>
<tr><td>carrying_capacity</td><td>1.35e+07</td><td>4.78e+05    </td><td>number of cells</td><td>7.43e-08   </td></tr>
</table>