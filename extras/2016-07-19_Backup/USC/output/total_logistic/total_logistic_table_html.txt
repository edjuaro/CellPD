<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.0355  </td><td>inf         </td><td>1/hours        </td><td>28.2       </td></tr>
<tr><td>carrying_capacity</td><td>7.81e+10</td><td>inf         </td><td>number of cells</td><td>1.28e-11   </td></tr>
<tr><td>seeding_cells    </td><td>1.13e+03</td><td>inf         </td><td>number of cells</td><td>0.000887   </td></tr>
</table>