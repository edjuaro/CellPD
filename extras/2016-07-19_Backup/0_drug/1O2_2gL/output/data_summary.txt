<table>
	<tr><th></th><th colspan="2">Live cells</th><th colspan="2">Dead cells</th></tr>
	<tr><th>Hours</th><th>Mean</th><th>STD</th><th>Mean</th><th>STD</th></tr>
	<tr><td>0.0</td><td>3.19e+03</td><td>81.8</td><td>3.02e+03</td><td>102</td></tr>
	<tr><td>48.0</td><td>1.15e+04</td><td>175</td><td>1.11e+04</td><td>166</td></tr>
	<tr><td>72.0</td><td>1.87e+04</td><td>560</td><td>1.81e+04</td><td>591</td></tr>
</table>