<table>
<tr><th>Parameter        </th><th>  Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.0178 </td><td>0.00635     </td><td>1/hours        </td><td>56.1       </td></tr>
<tr><td>carrying_capacity</td><td>1.4e+05</td><td>1.16e+06    </td><td>number of cells</td><td>7.15e-06   </td></tr>
<tr><td>seeding_cells    </td><td>2.5e+03</td><td>224         </td><td>number of cells</td><td>0.000401   </td></tr>
</table>