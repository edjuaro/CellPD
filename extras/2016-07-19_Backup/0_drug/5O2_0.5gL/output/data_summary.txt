<table>
	<tr><th></th><th colspan="2">Live cells</th><th colspan="2">Dead cells</th></tr>
	<tr><th>Hours</th><th>Mean</th><th>STD</th><th>Mean</th><th>STD</th></tr>
	<tr><td>0.0</td><td>3.2e+03</td><td>49.1</td><td>3.13e+03</td><td>56.8</td></tr>
	<tr><td>24.0</td><td>7.08e+03</td><td>79.3</td><td>6.94e+03</td><td>102</td></tr>
	<tr><td>48.0</td><td>1.6e+04</td><td>391</td><td>1.57e+04</td><td>354</td></tr>
	<tr><td>72.0</td><td>2.29e+04</td><td>706</td><td>2.26e+04</td><td>754</td></tr>
</table>