<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.0391  </td><td>0.00546     </td><td>1/hours        </td><td>25.6       </td></tr>
<tr><td>carrying_capacity</td><td>4.1e+04 </td><td>1.75e+04    </td><td>number of cells</td><td>2.44e-05   </td></tr>
<tr><td>seeding_cells    </td><td>3.16e+03</td><td>215         </td><td>number of cells</td><td>0.000317   </td></tr>
</table>