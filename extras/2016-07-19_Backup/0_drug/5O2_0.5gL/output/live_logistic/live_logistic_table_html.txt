<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.0402  </td><td>0.00619     </td><td>1/hours        </td><td>24.9       </td></tr>
<tr><td>carrying_capacity</td><td>3.92e+04</td><td>1.7e+04     </td><td>number of cells</td><td>2.55e-05   </td></tr>
<tr><td>seeding_cells    </td><td>3.07e+03</td><td>240         </td><td>number of cells</td><td>0.000326   </td></tr>
</table>