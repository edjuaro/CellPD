<table>
	<tr><th></th><th colspan="2">Live cells</th><th colspan="2">Dead cells</th></tr>
	<tr><th>Hours</th><th>Mean</th><th>STD</th><th>Mean</th><th>STD</th></tr>
	<tr><td>0.0</td><td>3.6e+03</td><td>19</td><td>3.5e+03</td><td>19.4</td></tr>
	<tr><td>24.0</td><td>6.81e+03</td><td>258</td><td>6.74e+03</td><td>259</td></tr>
	<tr><td>48.0</td><td>1.62e+04</td><td>471</td><td>1.61e+04</td><td>468</td></tr>
	<tr><td>72.0</td><td>3.19e+04</td><td>297</td><td>3.18e+04</td><td>309</td></tr>
</table>