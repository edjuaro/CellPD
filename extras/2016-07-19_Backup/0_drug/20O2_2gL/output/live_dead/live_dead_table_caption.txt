shows the list of parameters that achieve minimal Sum of Squared Errors (SSE) for the Live cells model (with cell death), MultiCellDS name: 'live_dead.' Sum of Squared Errors is 5.41e+03, Mean Absolute Percentage Error is 10.13%,and the Reduced Chi Squared Goodness of Fit is 902.</p><p><b>Warning:</b> Parameters 'death rate' and 'clearance rate' had a correlation larger than 0.9 
so they could not be estimated reliably at the same time, this also means the covariance 
matrix was singular, so no confidence intervals can be estimated. To address that problem 
'clearance rate' was fixed to 0 and the rest of the parameters were re-estimated. 
You may change this behavior if you want.