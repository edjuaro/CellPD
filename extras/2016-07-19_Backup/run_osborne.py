from subprocess import call
from os import rename
from shutil import copyfile


call('python cellpd.py --name osborne_atcc.xlsx')
rename('Osborne_ATCC\output\MCF_7.xml','Osborne_ATCC\output\MCF_7_atcc.xml')
copyfile('Osborne_ATCC\output\MCF_7_atcc.xml','MCF_7_atcc.xml')

call('python cellpd.py --name osborne_ko.xlsx')
rename('Osborne_KO\output\MCF_7.xml','Osborne_ATCC\output\MCF_7_ko.xml')
copyfile('Osborne_ATCC\output\MCF_7_ko.xml','MCF_7_ko.xml')

call('python cellpd.py --name osborne_mcf7.xlsx')
rename('Osborne_MCF7\output\MCF_7.xml','Osborne_ATCC\output\MCF_7_mcf7.xml')
copyfile('Osborne_ATCC\output\MCF_7_mcf7.xml','MCF_7_mcf7.xml')

call('python cellpd.py --name osborne_s.xlsx')
rename('Osborne_S\output\MCF_7.xml','Osborne_ATCC\output\MCF_7_s.xml')
copyfile('Osborne_ATCC\output\MCF_7_s.xml','MCF_7_s.xml')