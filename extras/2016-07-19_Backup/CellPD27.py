# from __future__ import division
# from __future__ import absolute_import
from subprocess import call
from datetime import datetime
import tzlocal
from io import open
from itertools import izip

local_tz = tzlocal.get_localzone()
TOOL_NAME = u"CellPD: Cell Line Phenotype Digitizer"
VERSION = 1.0
TIME_STAMP = local_tz.localize(datetime.now()).isoformat(sep='T')
print u"========================================"
print u"Setup completed on", TIME_STAMP
print u"{:s} (version {:1.1f}) will begin to run now.".format(TOOL_NAME, VERSION)
print u"========================================"

import os
import sys
import argparse

if not len(sys.argv) > 1:
    print u"***************************************"
    print u"***************************************"
    file = raw_input(u"Please type the name of the input file:")
    # print u"Using input file name '", file, u"' as selected"

else:
    parser = argparse.ArgumentParser()
    parser.add_argument(u"name", help=u"specify input file name")
    args = parser.parse_args()
    file = args.name
    print u"Using input file name '", file, u"' as selected"

if (u'.xlsx' not in file) or (u'.xls' not in file):
    if os.path.isfile(file + u".xlsx"):
        print u"Adding '.xlsx' to the filename"
        file = file + u".xlsx"
    elif os.path.isfile(file + u".xls"):
        print u"Adding '.xls' to the filename"
        file = file + u".xls"

import shutil

shutil.copy(u"files/matplotlibrc", u"matplotlibrc")

from lmfit import minimize, Parameters, report_fit
import numpy as np
import matplotlib.pyplot as plt
import copy
from scipy.integrate import odeint
from itertools import cycle
from tabulate import tabulate
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.compat import range

import webbrowser
import time as get_time
import pickle

# For debugging purposes
# from inspect import currentframe, getframeinfo
from inspect import getframeinfo, stack

# Set the seed for reproducibility
np.random.seed(0)

# Define some global variables
# format_list = ["png", "svg", "tiff", "jpg", "pdf", "eps"]  # PDF and TIFF causes problems in some machines
format_list = [u"png", u"svg", u"pdf"]
drop_values = False
full_output = True

OPEN_BROWSER = True
HOLD_CMD = False

uncertainty_note = u''

tic = get_time.time()


def tic():
    tic = get_time.time()
    return


def toc():
    caller = getframeinfo(stack()[1][0])
    global tic
    elapsed = get_time.time() - tic
    print u"Elapsed ", elapsed, u"Currently on line", caller.lineno
    tic = get_time.time()
    return


## Define all the models
def live(y, t, params):
    # Live Cell counts only
    # L = y[0]
    # growth_rate = params["growth_rate"].value
    # dL = growth_rate*L
    return np.reshape(params[u"seeding_cells"].value * np.exp(params[u"growth_rate"].value * (t - min(t))), (len(t), 1))


def live_ODE(y, t, params):
    # Live Cell counts only
    L = y[0]

    growth_rate = params[u"growth_rate"].value

    dL = growth_rate * L

    return dL


def live_logistic_ODE(y, t, params):
    # Live Cell counts only
    L = y[0]

    growth_rate = params[u"growth_rate"].value
    carrying_capacity = params[u"carrying_capacity"].value
    #
    # C =  -(1-(carrying_capacity/L0)/carrying_capacity)
    # return carrying_capacity/(1 - np.exp(-growth_rate*t-carrying_capacity*C))
    #
    dL = growth_rate * (1 - L / carrying_capacity) * L
    #
    return dL


def live_logistic(y, t, params):
    # Live Cell counts only
    L = y[0]

    growth_rate = params[u"growth_rate"].value
    carrying_capacity = params[u"carrying_capacity"].value

    dL = growth_rate * (1 - L / carrying_capacity) * L

    return dL


def gompertz(y, t, params): # Not being use in this version
    carrying_capacity = params[u"carrying_capacity"].value
    growth_rate = params[u"growth_rate"].value

    b = np.log(carrying_capacity / params[u"seeding_cells"].value)

    return np.reshape(carrying_capacity * np.exp(-b * np.exp(-growth_rate * (t - min(t)))), (len(t), 1))

def logistic_gf(y, t, params):
    carrying_capacity = params[u"carrying_capacity"].value
    growth_rate = params[u"growth_rate"].value
    y_0 = params[u"seeding_cells"].value

    lag = carrying_capacity/(4*growth_rate)*(np.log(carrying_capacity/y_0 - 1)-2)

    return np.reshape( carrying_capacity / ( t/t + np.exp( (4*growth_rate/carrying_capacity)* (lag - (t - min(t))) +2 ) ) ,( len(t), 1))


def live_dead(y, t, params):
    # Live Cell Counts + Viability
    # Requires four parameters: [birth_rate, death_rate, clearance_rate, carrying_capacity]
    L = y[0]
    D = y[1]

    birth_rate = params[u"birth_rate"].value
    death_rate = params[u"death_rate"].value
    clearance_rate = params[u"clearance_rate"].value

    dL = birth_rate * L - death_rate * L
    dD = death_rate * L - clearance_rate * D

    return [dL, dD]
    # return [max(0,dL),max(0,dD)]


def live_dead_logistic(y, t, params):
    # Live Cell Counts + Viability
    # Requires four parameters: [birth_rate, death_rate, clearance_rate, carrying_capacity]
    L = y[0]
    D = y[1]

    birth_rate = params[u"birth_rate"].value
    death_rate = params[u"death_rate"].value
    clearance_rate = params[u"clearance_rate"].value
    carrying_capacity = params[u"carrying_capacity"].value

    dL = birth_rate * (1 - L / carrying_capacity) * L - death_rate * L
    dD = death_rate * L - clearance_rate * D

    return [dL, dD]
    # return [max(0,dL),max(0,dD)]


def total(y, t, params):
    # Live Cell Counts + Viability but we only track Total Cells
    # T_0 = y[0]
    # growth_rate = params["growth_rate"].value
    # dT = growth_rate*T
    return np.reshape(params[u"seeding_cells"].value * np.exp(params[u"growth_rate"].value * (t - min(t))), (len(t), 1))


def total_ODE(y, t, params):
    # Live Cell Counts + Viability but we only track Total Cells
    T = y[0]

    growth_rate = params[u"growth_rate"].value

    dT = growth_rate * T

    return dT


def total_logistic(y, t, params):
    # Live Cell Counts + Viability but we only track Total Cells
    T = y[0]

    growth_rate = params[u"growth_rate"].value
    carrying_capacity = params[u"carrying_capacity"].value

    dT = growth_rate * (1 - T / carrying_capacity) * T

    # minimum_population = y[0]*0.06

    # dT = growth_rate * (1- minimum_population/T) *(1 - T / carrying_capacity) * T

    return dT
    # return max(0,dT)

def total_allee(y, t, params): # Not being used in this version
    # Live Cell Counts + Viability but we only track Total Cells
    T = y[0]

    growth_rate = params[u"growth_rate"].value
    carrying_capacity = params[u"carrying_capacity"].value
    minimum_population = params[u"minimum_population"].value

    dT = growth_rate * (1- minimum_population/T) *(1 - T / carrying_capacity) * T

    return dT


# Create some global variables
# create a dictionary of functions
choose_model = {
    u"live": live,
    u"live_logistic": live_logistic,
    u"live_dead": live_dead,
    u"live_dead_logistic": live_dead_logistic,
    u"total": total,
    u"gompertz": gompertz,
    u"logistic_gf": logistic_gf,
    u"total_logistic": total_logistic,
    u"total_allee": total_allee,
}


def MyError(params, function_name, time, y_init, target, sigmas):
    # This function computes the (Weighted Relative) Square Error

    if function_name in [u'live', u'total', u'gompertz',u'logistic_gf']:
        y = choose_model[function_name](params[u'seeding_cells'], time, params)
        y.shape = target.shape
    elif function_name in [u'live_logistic', u'total_logistic',u'total_allee']:
        y = odeint(choose_model[function_name], params[u'seeding_cells'], time, args=(params,))
    elif function_name in [u'live_dead', u'live_dead_logistic']:
        y = odeint(choose_model[function_name], [params[u'seeding_cells_live'], params[u'seeding_cells_live']], time,
                   args=(params,))
    else:
        print u'The model [:s] is not supported.'.format(function_name)

    # print(y,target,time)

    z = copy.deepcopy(y)

    errors = np.divide(z - target, sigmas)
    errors.shape = errors.shape[0] * errors.shape[1]

    return errors


def MAPE(params, function_name, time, y_init, target, sigmas):
    if function_name in [u'live', u'total', u'gompertz', u'logistic_gf']:
        y = choose_model[function_name](y_init, time, params)
        y.shape = target.shape
    elif function_name in [u'live_logistic', u'total_logistic',u'total_allee']:
        y = odeint(choose_model[function_name], params[u'seeding_cells'], time, args=(params,))
    elif function_name in [u'live_dead', u'live_dead_logistic']:
        y = odeint(choose_model[function_name], [params[u'seeding_cells_live'], params[u'seeding_cells_live']], time,
                   args=(params,))
    else:
        print u'The model "{:s}" is not supported.'.format(function_name)

    z = copy.deepcopy(y)

    errors = abs(100 * np.divide(z - target, target))
    errors.shape = errors.shape[0] * errors.shape[1]
    MAPE = sum(errors) / len(errors)

    return MAPE


# Define more global variables:
name_dictionary = {
    u"live": u"Live cells model",
    u"live_logistic": u"Live cells model (with logistic limits)",
    u"live_dead": u"Live cells model (with cell death)",
    u"live_dead_logistic": u"Live cells model (with cell death and logistic limits)",
    u"total": u"Total cells model",
    u"total_ODE": u"Total cells model (integrated ODE)",
    u"total_logistic": u"Total cells model (with logistic limits)",
    u"total_allee": u"Gompertzian model",
    u"gompertz": u"Gompertzian model",
    u"logistic_gf": u"Logistic model implemented by grofit",
}
title_dictionary = {
    u"live": u"Live cells model",
    u"live_logistic": u"Live cells model (with logistic limits)",
    u"live_dead": u"Live cells model (with cell death)",
    u"live_dead_logistic": u"Live cells model (with cell death and logistic limits)",
    u"total": u"Total cells model",
    u"total_ODE": u"Total cells model (ODE)",
    u"total_logistic": u"Total cells model (with logistic limits)",
    u"total_allee": u"total_allee",
    u"gompertz": u"Gompertzian model",
    u"logistic_gf": u"Logistic model implemented by grofit",
}
legend_dictionary = {
    u"live": [u"Live Cells"],
    u"live_logistic": [u"Live Cells"],
    u"live_dead": [u"Live Cells", u"Dead Cells"],
    u"live_dead_logistic": [u"Live Cells", u"Dead Cells"],
    u"total": [u"Total Cells"],
    u"total_ODE": [u"Total Cells (ODE)"],
    u"total_logistic": [u"Total Cells"],
    u"total_allee": [u"Total Cells"],
    u"gompertz": [u"Total Cells"],
    u"logistic_gf": [u"Total Cells"],

}
sheet_dictionary = {
    u"live": u"Live",
    u"live_logistic": u"Live_logistic",
    u"live_dead": u"Live+Dead",
    u"live_dead_logistic": u"Live+Dead_logistic",
    u"total": u"Total_Cells",
    u"total_ODE": u"Total_Cells",
    u"total_logistic": u"Total_logistic",
    u"total_allee": u"Total_logistic",
    u"gompertz": u"Total_Cells",
    u"logistic_gf": u"Total_Cells",
}
description_dictionary = {
    u"live": u"This is an exponential model that describes the growth of the live cells.",
    u"live_logistic": u"This is an exponential model that describes the growth of the live cells "
                     u"with logistic limitations.",
    u"live_dead": u"This is an exponential model that describes the growth of the live cells "
                 u"and accounts for apoptosis.",
    u"live_dead_logistic": u"This is an exponential model that describes the growth of the live cells "
                          u"with logistic limitations, it also accounts for apoptosis",
    u"total": u"This is an exponential model that describes the growth of the live and apoptosing cells combined.",
    u"total_ODE": u"This is an exponential model that describes the growth of the live and apoptosing cells combined "
                 u"by integrating an Ordinary Differential Equation (ODE)",
    u"total_logistic": u"This is an exponential model that describes the growth of the live"
                      u" and apoptosing cells combined accounting for logistic limitations.",
    u"gompertz": u"Gompertzian model [write a description here]",
    u"total_allee": u"Gompertzian model [write a description here]",
    u"logistic_gf": u"Logistic model implemented by grofit",
}
model_dictionary = {
    u"live": ur"\begin{equation}"
            u"\n"
            ur"\frac{d[\rm{Live}]}{dt} = [\rm{growth\_rate}][\rm{Live}]"
            u"\n"
            ur"\end{equation}"
            u"\n",
    u"live_logistic": ur"\begin{equation}"
                     u"\n"
                     ur"\frac{d[\rm{Live}]}{dt} = [\rm{growth\_rate}]\left(1-\frac{[\rm{Live}]}{{[\rm{max\_density}]}}\right)[\rm{Live}]"
                     u"\n"
                     ur"\end{equation}"
                     u"\n",
    u"live_dead": ur"\begin{eqnarray}"
                 u"\n"
                 ur"\frac{d[\rm{Live}]}{dt} &=& [\rm{birth\_rate}][\rm{Live}] - [\rm{death\_rate}][\rm{Live}]\\"
                 u"\n"
                 ur"\frac{d[\rm{Dead}]}{dt} &=& [\rm{death\_rate}][\rm{Live}] - [\rm{clearance\_rate}][\rm{Dead}]\\"
                 u"\n"
                 ur"\end{eqnarray}"
                 u"\n",
    u"live_dead_logistic": ur"\begin{eqnarray}"
                          u"\n"
                          ur"\frac{d[\rm{Live}]}{dt} &=& [\rm{birth\_rate}]\left(1-\frac{[\rm{Live}]}{[\rm{max\_density}]}\right)[\rm{Live}] - [\rm{death\_rate}][\rm{Live}]\\"
                          u"\n"
                          ur"\frac{d[\rm{Dead}]}{dt} &=& [\rm{death\_rate}][\rm{Live}] - [\rm{clearance\_rate}][\rm{Dead}]\\"
                          u"\n"
                          ur"\end{eqnarray}"
                          u"\n",
    u"total": ur"\begin{eqnarray}"
             u"\n"
             ur"\frac{d[Total]}{dt} = [\rm{growth\_rate}][Total]"
             u"\n"
             ur"\end{eqnarray}"
             u"\n",
    u"total_ODE": ur"\begin{eqnarray}"
                 u"\n"
                 ur"\frac{d[Total]}{dt} = [\rm{growth\_rate}][Total]"
                 u"\n"
                 ur"\end{eqnarray}"
                 u"\n",
    u"total_logistic": ur"\begin{eqnarray}"
                      u"\n"
                      ur"\frac{d[Total]}{dt} = [\rm{growth\_rate}]\left(1-\frac{[Total]}{[\rm{max\_density}]}\right)[\rm{Total}]"
                      u"\n"
                      ur"\end{eqnarray}"
                      u"\n",
    u"gompertz": u"[Insert equation]",
    u"total_allee": u"[Insert equation]",
    u"logistic_gf": u"[insert equation line 412]",
}

rate = {
    u"growth_rate": u"growth_phase",
    u"birth_rate": u"birth_phase",
    u"carrying_capacity": u"cell_cycle_arrest",
    u"death_rate": u"death_phase",
    u"clearance_rate": u"cell_death/duration",
    u"seeding_cells": u"seeding_cells",
    u"minimum_population": u"minimum_population",
}
phase = {
    u"growth_rate": u"growth_phase",
    u"birth_rate": u"birth_phase",
    u"carrying_capacity": u"cell_cycle_arrest",
    u"death_rate": u"death_phase",
    u"clearance_rate": u"cell_death/duration",
    u"total": u"growth_phase",
    u"total_logistic": u"growth_phase",
    u"total_allee": u"growth_phase",
    u"live": u"birth_phase",
    u"live_logistic": u"birth_phase",
    u"live_dead": u"birth_phase",
    u"live_dead_logistic": u"birth_phase",
    u"seeding_cells": u"seeding_cells",
    u"gompertz": u"",
    u"seeding_cells_live": u"seeding_cells_live",
    u"seeding_cells_dead": u"seeding_cells_dead",
    u"minimum_population": u"minimum_population",
    u"logistic_gf": u"",
}


def CustomPlot(function_name, y_init, time, params, result, units, target, sigmas, fig_counter, correlation_flag,
               stderr_flag, missing_errorbar_flag, color_list):


    if not os.path.exists(out_dir_root + u"/output/" + function_name + u"/"):
        os.makedirs(out_dir_root + u"/output/" + function_name + u"/")
    out_dir = out_dir_root + u"/output/" + function_name + u"/"

    src = u"files/" + function_name + u"/"
    src_files = os.listdir(src)
    for file_name in src_files:
        full_file_name = os.path.join(src, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy(full_file_name, out_dir)

    if template == u"Total+Viability":
        color_dictionary = {
            u"live": color_list[0],
            u"live_logistic": color_list[0],
            u"live_dead": [color_list[i] for i in [0, 1]],
            u"live_dead_logistic": [color_list[i] for i in [0, 1]],
            u"total": color_list[0],
            u"total_logistic": color_list[0],
            u"total_allee": color_list[0],
        }
        markers_dictionary = {
            u"live": (u'^'),
            u"live_logistic": (u'^'),
            u"live_dead": (u'^', u'v'),
            u"live_dead_logistic": (u'^', u'v'),
            u"total": (u'D'),
            u"total_logistic": (u'd'),
            u"total_allee": (u'd'),
        }

    elif template == u"Total_Cells":
        color_dictionary = {
            u"total": color_list[0],
            u"total_logistic": color_list[0],
            u"total_allee": color_list[0],
            u"gompertz": color_list[0],
            u"logistic_gf": color_list[0],
        }
        markers_dictionary = {
            u"total": (u'D'),
            u"total_logistic": (u'd'),
            u"total_allee": (u'd'),
            u"gompertz": (u'd'),
            u"logistic_gf": (u'd'),
        }

    # Simulation time is a vector of 100 evenly spaced values in the interval [experiment_initial_time,experiemnt_final_time]
    t_sim = np.array(np.linspace(np.min(time), np.max(time), num=100))

    # Integrate the ODE model in the interval defined above, using the parameters and initial conditions provided in the function call
    if function_name in [u'live', u'total', u'gompertz', u'logistic_gf']:
        y = choose_model[function_name](params[u'seeding_cells'].value, t_sim, params)
    elif function_name in [u'live_logistic', u'total_logistic',u'total_allee']:
        y = odeint(choose_model[function_name], params[u'seeding_cells'].value, t_sim, args=(params,))
    elif function_name in [u'live_dead', u'live_dead_logistic']:
        y = odeint(choose_model[function_name],
                   [params[u'seeding_cells_live'].value, params[u'seeding_cells_live'].value], t_sim, args=(params,))
    else:
        print u'The model [:s] is not supported.'.format(function_name)

    lines = (u"-", u"--", u"-.", u":")  # to be used in the B&W plots

    number_of_subplots = 1

    fig, axs = plt.subplots(1, number_of_subplots, sharex=True, sharey=u'none', squeeze=True)

    # Matplotlib handles figures with no subplots differently.

    if (function_name == u"total") or (function_name == u"total_logistic") or (function_name == u'total_allee'):
        axs.plot(t_sim, y[:, 0], color=color_list[0], label=u'Total cells (fitted)')
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u'.', label=u'Total cells')
        if show_replicates == True:
            print u"Showing Replicates"
            for time_point in data.keys():
                if data[time_point][u'Total'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Total'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    # print('More than 1 Bio Rep')
                    for br_key in data[time_point].keys():

                        if not isinstance(br_key, unicode):

                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Total'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)

    else:
        axs.plot(t_sim, y[:, 0], color=color_list[0], label=u'Live cells (fitted)')
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u'.', label=u'Live cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Live'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Live'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Live'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)

    axs.set_ylabel(u"Number of Cells")

    if y.shape[1] == 2:
        axs.plot(t_sim, y[:, 1], color=color_list[1], label=u'Dead cells (fitted)')
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 1], yerr=sigmas[:, 1], color=color_list[1], fmt=u'.', label=u'Dead cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Dead'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Dead'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Dead'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)

    plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
    plt.xlabel(u"time (Hours)")

    lgd_1 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
    lgd_2 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

    title = plt.suptitle(title_dictionary[function_name], size=20.0, y=1.02)

    # Saving the figure in multiple formats
    for format in format_list:
        fig.savefig(out_dir + function_name + u"." + format, dpi=300, format=format,
                    bbox_extra_artists=[lgd_1, lgd_2, title, ], bbox_inches=u'tight')

    if full_output:
        # Saving the figure data as a pickle file
        pickle.dump((t_sim, y,), open(out_dir + function_name + u"_output.p", u"wb"))

        # Make the same plots in log-linear space
        plt.gca().set_yscale(u'log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + function_name + u"_loglinear." + format, dpi=300, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, title), bbox_inches=u'tight')

        plt.close(fig)

        # Now making plots in B&G
        fig, axs = plt.subplots(1, number_of_subplots, sharex=True, sharey=u'none', squeeze=True)
        plt.rc(u"axes", color_cycle=u"k")

        time_2 = np.array(np.linspace(np.min(time), np.max(time), num=len(time) * 2))
        if function_name in [u'live', u'total', u'gompertz', u'logistic_gf']:
            y_2 = choose_model[function_name](params[u'seeding_cells'].value, time_2, params)
        elif function_name in [u'live_logistic', u'total_logistic',u'total_allee']:
            y_2 = odeint(choose_model[function_name], params[u'seeding_cells'].value, time_2, args=(params,))
        elif function_name in [u'live_dead', u'live_dead_logistic']:
            y_2 = odeint(choose_model[function_name],
                        [params[u'seeding_cells_live'].value, params[u'seeding_cells_live'].value], time_2, args=(params,))
        else:
            print u'The model [:s] is not supported.'.format(function_name)
        

        ROW = 0  # live/total cells
        if (function_name == u"total") or (function_name == u"total_logistic") or (function_name == u'total_allee'):
            axs.plot(time_2, y_2[:, ROW], linewidth=0.0, linestyle=lines[ROW],
                     marker=markers_dictionary[function_name][ROW], markerfacecolor=u'none', markeredgewidth=1.5,
                     label=u'Total cells (fitted)')
            axs.plot(t_sim, y[:, ROW], linewidth=1.5, linestyle=lines[ROW], marker=None)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label=u'Total cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point][u'Total'][u'single_bio_rep']:
                        for tech_rep in data[time_point][u'Total'][u'bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, unicode):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Total'][u'mean'], u'.',
                                         color=u'#000000', alpha=0.5)
        else:
            axs.plot(time_2, y_2[:, ROW], linewidth=0.0, linestyle=lines[ROW],
                     marker=markers_dictionary[function_name][ROW], markerfacecolor=u'none', markeredgewidth=1.5,
                     label=u'Live cells (fitted)')
            axs.plot(t_sim, y[:, ROW], linewidth=1.5, linestyle=lines[ROW], marker=None)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label=u'Live cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point][u'Live'][u'single_bio_rep']:
                        for tech_rep in data[time_point][u'Live'][u'bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, unicode):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Live'][u'mean'], u'.',
                                         color=u'#000000', alpha=0.5)

        axs.set_ylabel(u"Number of Cells")

        if y.shape[1] == 2:  # Dead cells
            ROW = 1
            axs.plot(time_2, y_2[:, ROW], linewidth=0.0, linestyle=lines[ROW],
                     marker=markers_dictionary[function_name][ROW], markerfacecolor=u'none', markeredgewidth=1.5,
                     label=u'Dead cells (fitted)')
            axs.plot(t_sim, y[:, ROW], linewidth=1.5, linestyle=lines[ROW], marker=None)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label=u'Dead cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point][u'Dead'][u'single_bio_rep']:
                        for tech_rep in data[time_point][u'Dead'][u'bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, unicode):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Dead'][u'mean'], u'.',
                                         color=u'#000000', alpha=0.5)

        plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
        plt.xlabel(u"time (Hours)")

        lgd_1 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        lgd_2 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

        title = plt.suptitle(title_dictionary[function_name], size=20.0, y=1.01)

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + function_name + u"_BW." + format, dpi=300, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, title,), bbox_inches=u'tight')

        # Make the same plots in log-linear space
        plt.gca().set_yscale(u'log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + function_name + u"_loglinear_BW." + format, dpi=300, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, title,), bbox_inches=u'tight')

    plt.close(fig)

    # Writing the figure captions to a text file
    dest_filename = out_dir + function_name + u"_caption.txt"
    errors = MyError(params, function_name, time, y_init, target, sigmas)
    SSE = sum(errors ** 2)
    mape = MAPE(params, function_name, time, y_init, target, sigmas)
    f = open(dest_filename, u"w")
    f.write(ur"Shows the GoF of the " + name_dictionary[function_name] + u",MultiCellDS name: '" + function_name + u".' ")
    f.write(u"Sum of Squared Errors (SSE) is {:.3g}, ".format(SSE))
    f.write(u"the Mean Absolute Percentage Error (MAPE) is {:2.2f}%,".format(mape))
    if result.chisqr < 1e-16:
        f.write(u"and the Reduced Chi-Squared Goodness of Fit (GoF) is {:.3g}.".format(result.chisqr))
    else:
        f.write(u"and the Reduced Chi-Squared Goodness of Fit (GoF) is {:.3g}.".format(result.redchi))
    f.close()

    # Writing the figure titles to a separate text file
    dest_filename = out_dir + function_name + u"_title.txt"
    f = open(dest_filename, u"w")
    f.write(title_dictionary[function_name])
    f.close()

    # Writing the model names with correct LaTeX formatting
    dest_filename = out_dir + function_name + u"_latex_title.txt"
    f = open(dest_filename, u"w")
    f.write(name_dictionary[function_name].replace(u" ", u"\_"))
    f.close()

    # Writing the model description with correct LaTeX formatting
    dest_filename = out_dir + function_name + u"_description.txt"
    f = open(dest_filename, u"w")
    f.write(description_dictionary[function_name])
    f.close()


    # Now output the parameters as an excel file
    dest_filename = out_dir + function_name + u"_parameters.xlsx"
    wb = Workbook()
    ws = wb.active
    ws.title = sheet_dictionary[function_name]
    ws.append([title_dictionary[function_name] + u"Parameter Estimation Results"])
    ws.append([])
    headers = [u"Parameter", u"Value", u"Std. Error", u"units", u"Inv_value"]
    temp_list = []
    for key, value in params.items():
        if value.value == 0:
            temp_list.append([key, value.value, value.stderr, units[key], u"inf"])
        else:
            temp_list.append([key, value.value, value.stderr, units[key], 1 / value.value])
    ws.append(headers)
    for temp_row in temp_list:
        ws.append(temp_row)
    ws.append([])
    ws.append([])
    wb.save(filename=dest_filename)

    # Writing the standalone latex file which contains the parameter table
    dest_filename = out_dir + function_name + u"_table.tex"
    f = open(dest_filename, u"w")
    table = tabulate(temp_list, headers, tablefmt=u"latex", floatfmt=u".3g")
    f.write(table)
    f.close()

    # Writing the file that contains the html code for the parameter table
    dest_filename = out_dir + function_name + u"_table_html.txt"
    f = open(dest_filename, u"w")
    table = tabulate(temp_list, headers, tablefmt=u"html", floatfmt=u".3g", numalign=u"none")
    f.write(table)
    f.close()

    # Writing the standalone latex file which contains the model equations
    dest_filename = out_dir + function_name + u"_model.tex"
    f = open(dest_filename, u"w")
    f.write(model_dictionary[function_name])
    f.close()

    # Writing a csv file [to be used by LaTeX]
    dest_filename = out_dir + function_name + u"_table.csv"
    f = open(dest_filename, u"w")
    f.write(u"parameter,value,std. err.,units,invval\n")
    for key, value in params.items():
        f.write(u"{:s},".format(key.replace(u"_", u"\_")))  # for LaTeX purposes
        f.write(u"{:.3g},".format(value.value))
        f.write(u"{:.3g},".format(value.stderr))
        f.write(u"{:s},".format(units[key]))
        if value.value == 0.0:
            f.write(u"{:s}\n".format(u"inf"))
        else:
            f.write(u"{:.3g}\n".format(1 / value.value))
    f.close()

    # Writing a txt file
    dest_filename = out_dir + function_name + u"_table.txt"
    f = open(dest_filename, u"w")
    f.write(u"parameter,value,std. err.,units,invval\n")
    for key, value in params.items():
        f.write(u"{:s},".format(key))
        f.write(u"{:.3g},".format(value.value))
        f.write(u"{:.3g},".format(value.stderr))
        f.write(u"{:s},".format(units[key]))
        if value.value == 0.0:
            f.write(u"{:s}\n".format(u"inf"))
        else:
            f.write(u"{:.3g}\n".format(1 / value.value))
    f.close()

    # Write the or append the xml file that contains the MCDS models:
    model_xml_dictionary = {
        u"model_name": function_name,
        u"model_number": unicode(fig_counter - 2),
        u"param_name": u"test",
        u"param_number": 0.,
        u"param_units": u"test",
        u"param_stderr": 0.,
        u"param_value": 1.,

        u"death_type": u"Apoptosis",
        u"death_units": u"Hours",
        u"death_stderr": 12.,
        u"death_value": 24.,

        u"clearance_units": u"Hours",
        u"clearance_stderr": 12.,
        u"clearance_value": 24.,

        u"arrest_units": u"Squared_Microns",
        u"arrest_stderr": 100.,
        u"arrest_value": 1000.,
    }

    counter = 0
    dest_filename = out_dir + function_name + u"_partial.xml"
    if u"death_rate" in params.keys():
        f = open(dest_filename, u"w")
        f.write(
            u'\t\t\t\t<cell_cycle model="{0[model_name]:s}" ID="{0[model_number]:s}">\n'.format(model_xml_dictionary))
        # each cell cycle has the same 'background death rate'
        model_xml_dictionary[u"death_type"] = u"Apoptosis"
        model_xml_dictionary[u"death_units"] = units[u"death_rate"]
        model_xml_dictionary[u"death_stderr"] = params[u"death_rate"].stderr
        model_xml_dictionary[u"death_value"] = params[u"death_rate"].value

        if u"carrying_capacity" in params.keys():
            model_xml_dictionary[u"arrest_units"] = units[u"carrying_capacity"]
            model_xml_dictionary[u"arrest_stderr"] = params[u"carrying_capacity"].stderr
            model_xml_dictionary[u"arrest_value"] = params[u"carrying_capacity"].value
            for key, value in params.items():
                if key not in [u"carrying_capacity", u"death_rate",u"seeding_cells",u"seeding_cells_live",u"seeding_cells_dead"]:
                    if key == u"clearance_rate":
                        # don't write to the DCL yet.
                        model_xml_dictionary[u"clearance_units"] = units[key]
                        model_xml_dictionary[u"clearance_stderr"] = value.stderr
                        model_xml_dictionary[u"clearance_value"] = value.value
                    else:
                        model_xml_dictionary[u"param_name"] = phase[key]
                        model_xml_dictionary[u"param_number"] = counter
                        model_xml_dictionary[u"param_units"] = units[key]
                        model_xml_dictionary[u"param_stderr"] = value.stderr
                        model_xml_dictionary[u"param_value"] = value.value
                        if value.value == 0:
                            model_xml_dictionary[u"param_inv_value"] = float(u"inf")
                        else:
                            model_xml_dictionary[u"param_inv_value"] = 1 / value.value
                        f.write(u'\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'
                                u'\t\t\t\t\t\t<birth_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                u'\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</birth_rate>\n'
                                u'\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                                u'\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</duration>\n'
                                u'\t\t\t\t\t\t<death_rate units="{0[death_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[death_stderr]:1.3g}">\n'
                                u'\t\t\t\t\t\t{0[death_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</death_rate>\n'
                                u'\t\t\t\t\t\t<cell_cycle_arrest>\n'
                                u'\t\t\t\t\t\t\t<condition type="maximum_cell_density" units="{0[arrest_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[arrest_stderr]:1.3g}">\n'
                                u'\t\t\t\t\t\t\t{0[arrest_value]:1.3g}\n'
                                u'\t\t\t\t\t\t\t</condition>\n'
                                u'\t\t\t\t\t\t</cell_cycle_arrest>\n'
                                u'\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                        counter += 1
        else:  # No cell arrest
            for key, value in params.items():
                if key == u"clearance_rate":
                    # don't write to the DCL yet.
                    model_xml_dictionary[u"clearance_units"] = units[key]
                    model_xml_dictionary[u"clearance_stderr"] = value.stderr
                    model_xml_dictionary[u"clearance_value"] = value.value

                else:
                    if key not in [u"carrying_capacity", u"death_rate",u"seeding_cells",u"seeding_cells_live",u"seeding_cells_dead"]:
                        model_xml_dictionary[u"param_name"] = phase[key]
                        model_xml_dictionary[u"param_number"] = counter
                        model_xml_dictionary[u"param_units"] = units[key]
                        model_xml_dictionary[u"param_stderr"] = value.stderr
                        model_xml_dictionary[u"param_value"] = value.value
                        if value.value == 0:
                            model_xml_dictionary[u"param_inv_value"] = float(u"inf")
                        else:
                            model_xml_dictionary[u"param_inv_value"] = 1 / value.value
                        f.write(u'\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'
                                u'\t\t\t\t\t\t<birth_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                u'\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</birth_rate>\n'
                                u'\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                                u'\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</duration>\n'
                                u'\t\t\t\t\t\t<death_rate units="{0[death_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[death_stderr]:1.3g}">\n'
                                u'\t\t\t\t\t\t{0[death_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</death_rate>\n'
                                u'\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                        counter += 1

        f.write(u'\t\t\t\t</cell_cycle>\n')
        f.write(u'\t\t\t\t<cell_death type="{0[death_type]:s}">\n'
                u'\t\t\t\t\t<duration units="{0[clearance_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[clearance_stderr]:1.3g}">\n'
                u'\t\t\t\t\t{0[clearance_value]:1.3g}\n'
                u'\t\t\t\t\t</duration>\n'
                u'\t\t\t\t</cell_death>'.format(model_xml_dictionary))
        f.close()
    else:  # no cell death
        f = open(dest_filename, u"w")
        f.write(
            u'\t\t\t\t<cell_cycle model="{0[model_name]:s}" ID="{0[model_number]:s}">\n'.format(model_xml_dictionary))

        if u"carrying_capacity" in params.keys():
            model_xml_dictionary[u"arrest_units"] = units[u"carrying_capacity"]
            model_xml_dictionary[u"arrest_stderr"] = params[u"carrying_capacity"].stderr
            model_xml_dictionary[u"arrest_value"] = params[u"carrying_capacity"].value
            for key, value in params.items():
                if key not in [u"carrying_capacity", u"death_rate",u"seeding_cells",u"seeding_cells_live",u"seeding_cells_dead"]:
                    model_xml_dictionary[u"param_name"] = phase[key]
                    model_xml_dictionary[u"param_number"] = counter
                    model_xml_dictionary[u"param_units"] = units[key]
                    model_xml_dictionary[u"param_stderr"] = value.stderr
                    model_xml_dictionary[u"param_value"] = value.value
                    if value.value == 0:
                        model_xml_dictionary[u"param_inv_value"] = float(u"inf")
                    else:
                        model_xml_dictionary[u"param_inv_value"] = 1 / value.value
                    f.write(u'\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'.format(
                        model_xml_dictionary))
                    if function_name in [u'live']:
                        f.write(
                            u'\t\t\t\t\t\t<birt_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                            u'\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                            u'\t\t\t\t\t\t</birt_rate>\n'.format(model_xml_dictionary))
                    else:
                        f.write(
                            u'\t\t\t\t\t\t<net_birth_rate  units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                            u'\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                            u'\t\t\t\t\t\t</net_birth_rate >\n'.format(model_xml_dictionary))
                    f.write(u'\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                            u'\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                            u'\t\t\t\t\t\t</duration>\n'
                            u'\t\t\t\t\t\t<cell_cycle_arrest>\n'
                            u'\t\t\t\t\t\t\t<condition type="maximum_cell_density" units="{0[arrest_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[arrest_stderr]:1.3g}">\n'
                            u'\t\t\t\t\t\t\t{0[arrest_value]:1.3g}\n'
                            u'\t\t\t\t\t\t\t</condition>\n'
                            u'\t\t\t\t\t\t</cell_cycle_arrest>\n'
                            u'\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                    counter += 1
        else:  # No cell arrest
            for key, value in params.items():
                if key not in [u"carrying_capacity", u"death_rate",u"seeding_cells",u"seeding_cells_live",u"seeding_cells_dead"]:
                    if key == u"clearance_rate":
                        # don't write to the DCL yet.
                        model_xml_dictionary[u"clearance_units"] = units[key]
                        model_xml_dictionary[u"clearance_stderr"] = value.stderr
                        model_xml_dictionary[u"clearance_value"] = value.value
                    else:
                        model_xml_dictionary[u"param_name"] = phase[key]
                        model_xml_dictionary[u"param_number"] = counter
                        model_xml_dictionary[u"param_units"] = units[key]
                        model_xml_dictionary[u"param_stderr"] = value.stderr
                        model_xml_dictionary[u"param_value"] = value.value
                        if value.value == 0:
                            model_xml_dictionary[u"param_inv_value"] = float(u"inf")
                        else:
                            model_xml_dictionary[u"param_inv_value"] = 1 / value.value
                        f.write(u'\t\t\t\t\t<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">\n'.format(
                            model_xml_dictionary))
                        if function_name in [u'live']:
                            f.write(
                                u'\t\t\t\t\t\t<birt_rate units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                u'\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</birt_rate>\n'.format(model_xml_dictionary))
                        else:
                            f.write(
                                u'\t\t\t\t\t\t<net_birth_rate  units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">\n'
                                u'\t\t\t\t\t\t{0[param_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</net_birth_rate >\n'.format(model_xml_dictionary))
                        f.write(u'\t\t\t\t\t\t<duration units="hours" measurement_type="inferred">\n'
                                u'\t\t\t\t\t\t{0[param_inv_value]:1.3g}\n'
                                u'\t\t\t\t\t\t</duration>\n'
                                u'\t\t\t\t\t</cell_cycle_phase>\n'.format(model_xml_dictionary))
                        counter += 1
        f.write(u'\t\t\t\t</cell_cycle>')
        f.close()

    # Writing the table captions to a text file
    dest_filename = out_dir + function_name + u"_table_caption.txt"
    f = open(dest_filename, u"w")
    f.write(ur"shows the list of parameters that achieve minimal Sum of Squared Errors (SSE) for the " + name_dictionary[
        function_name] +
            u", MultiCellDS name: '" + function_name + u".' ")
    f.write(u"Sum of Squared Errors is {:.3g}".format(SSE) + u", Mean Absolute Percentage Error is {:2.2f}".format(
        mape) + u"%,")
    if result.chisqr < 1e-16:
        f.write(u"and the Reduced Chi Squared Goodness of Fit is {:.3g}.".format(result.chisqr))
    else:
        f.write(u"and the Reduced Chi Squared Goodness of Fit is {:.3g}.".format(result.redchi))
    if correlation_flag:
        f.write(
            u"</p><p><b>Warning:</b> Parameters 'death rate' and 'clearance rate' had a correlation larger than 0.9 \n"
            u"so they could not be estimated reliably at the same time, this also means the covariance \n"
            u"matrix was singular, so no confidence intervals can be estimated. To address that problem \n"
            u"'clearance rate' was fixed to 0 and the rest of the parameters were re-estimated. \n"
            u"You may change this behavior if you want.")
    if missing_errorbar_flag:
        f.write(
            u"</p><p><b>Warning:</b> The parameter estimate standard errors could not be estimated. This model may not be appropiate.\n")
    if stderr_flag:
        if SSE < 1e-16:
            stderr_flag = False
            f.write(
                u"</p><p><b>Note:</b>Parameter estimate standard errors may have not been estimated correctly because the SSE is too small ({:.3g})\n".format(
                    SSE))
        else:
            f.write(
                u"</p><p><b>Warning:</b> At least one parameter estimate standard error is of the same order of magnitude as its corresponding parameter\n."
                u"This model may not be appropiate.\n")



    if (len(target[:, 0])/max(time) > 2): # More than 2 samples per hour
        print u"*********************************"
        x = np.log(target[:, 0])
        first_diff = np.diff(x, n=1, axis=0)
        time_diff = np.diff(time, n=1, axis=0)
        # print("Max growth rate")
        # print("This is used by cellGrowth:Max growth rate with 3 hours of smoothing",max(running_mean(first_diff/time_diff,round(3/np.mean(time_diff)))))
    # if True:
        f.write(u"</p><p><b>Note:</b> High sampling rate was detected ({:d} samples in {:2.2f} hours). You may consider using this maximum growth rate instead:\n"
                u"<b>max_growth_rate</b>={:2.3g} (computed using a 3 hour window smoothing)".format(len(target[:, 0]),max(time),max(running_mean(first_diff/time_diff,round(3/np.mean(time_diff))))))
        # print('window:',round(3/np.mean(time_diff)),'dt=',np.mean(time_diff))
        # print("</p><p><b>Note:</b> High sampling rate was detected ({:d} samples in {:2.2f} hours). You may consider using this maximum growth rate instead:\n"
        #         "<b>max_growth_rate</b>={:2.3g} (computed using a 3 hour window smoothing)".format(len(target[:, 0]),max(time),max(running_mean(first_diff/time_diff,round(3/np.mean(time_diff))))))
    f.close()
    #max(first_diff/time_diff)

    warning = u''
    if stderr_flag:
        if correlation_flag:
            if missing_errorbar_flag:
                warning = u'(u+c+e)'
            else:
                warning = u'(u+c)'
        else:
            if missing_errorbar_flag:
                warning = u'(u+e)'
            else:
                warning = u'(u)'
    else:
        if correlation_flag:
            if missing_errorbar_flag:
                warning = u'(c+e)'
            else:
                warning = u'(c)'
        else:
            if missing_errorbar_flag:
                warning = u'(e)'
            else:
                warning = u''

    if result.chisqr < 1e-16:
        models_chi.append((u'<a href="' + u"./output/" + function_name + u"/" + function_name + u'_report.html">' +
                           title_dictionary[function_name] + u"</a>", u"{:2.3f}".format(result.chisqr), warning))
    else:
        models_chi.append((u'<a href="' + u"./output/" + function_name + u"/" + function_name + u'_report.html">' +
                           title_dictionary[function_name] + u"</a>", u"{:2.3f}".format(result.redchi), warning))

    models_mape.append((u'<a href="' + u"./output/" + function_name + u"/" + function_name + u'_report.html">' +
                        title_dictionary[function_name] + u"</a>", u"{:.2f}%".format(mape), warning))

    if result.chisqr < 1e-16:
        models_chi_table.append([title_dictionary[function_name], result.chisqr, warning])
    else:
        models_chi_table.append([title_dictionary[function_name], result.redchi, warning])
    models_mape_table.append([title_dictionary[function_name], mape, warning, ])

    # Writing the report for this model
    f = open(out_dir + function_name + u"_report.html", u"w")

    number_of_equations = {
        u"live": u"1",
        u"live_logistic": u"1",
        u"live_dead": u"2",
        u"live_dead_logistic": u"2",
        u"total": u"1",
        u"total_ODE": u"1",
        u"total_logistic": u"1",
        u"total_allee": u"1",
        u"gompertz": u"1",
        u"logistic_gf": u"1",
    }

    report_template = open(u"files/report_template.html").read()

    # Drop all the points where the first diff is negative and/or the second diff is positive
    x = np.log(target[:, 0])
    first_diff = np.diff(x, n=1, axis=0)
    second_diff = np.diff(first_diff, n=1, axis=0)
    (a,) = (first_diff >= 0).ravel().nonzero()
    (b,) = (second_diff <= 0).ravel().nonzero()


    if len(a) == 0:
        print u"These data does not behave like exponential growth, cells do not even increase in value."
        a = [0]
    if len(b) == 0:
        print u"These data does not behave like exponential growth, cells appear to be growing close to logistic limits from the beggining of experiment."
        b = [0]
    first_index = max(a[0], b[0])

    # report cell doubling times
    if u"growth_rate" in params.keys():
        exponential_doubling_time = np.log(2) / params[u"growth_rate"]
        naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
        naive_doubling_time = np.log(2) / naive_growth_rate
        growth_rate = np.log(target[-1, 0] / target[first_index, 0]) / ((time[-1] - time[first_index]))
        doubling_time = np.log(2) / growth_rate
    elif u"birth_rate" in params.keys():
        exponential_doubling_time = np.log(2) / (params[u"birth_rate"] - params[u"death_rate"])
        naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
        naive_doubling_time = np.log(2) / naive_growth_rate
        growth_rate = np.log(target[-1, 0] / target[first_index, 0]) / ((time[-1] - time[first_index]))
        doubling_time = np.log(2) / growth_rate
    else:
        if u"death_rate" in params.keys():
            # net_cell_cycle_length = 1/(params["r_G0G1"]-params["death_rate"])+1/(params["r_S"]-params["death_rate"])+1/(params["r_G2M"]-params["death_rate"])
            net_cell_cycle_length = 1 / (params[u"r_G2M"] * np.mean((y[:, 4] / y[:, 0])) - params[u"death_rate"])
            exponential_doubling_time = np.log(2) * (net_cell_cycle_length)
            naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
            naive_doubling_time = np.log(2) / naive_growth_rate
            growth_rate = np.log(target[-1, 0] / target[first_index, 0]) / ((time[-1] - time[first_index]))
            doubling_time = np.log(2) / growth_rate
        else:
            # net_cell_cycle_length = 1/params["r_G0G1"]+1/params["r_S"]+1/params["r_G2M"]
            net_cell_cycle_length = 1 / (params[u"r_G2M"] * np.mean((y[:, 3] / y[:, 0])))
            exponential_doubling_time = np.log(2) * net_cell_cycle_length
            naive_growth_rate = np.log(target[-1, 0] / target[0, 0]) / ((time[-1] - time[0]))
            naive_doubling_time = np.log(2) / naive_growth_rate
            # growth_rate = np.log(target[-1,0]/target[first_index,0])/((time[-1]-time[first_index]))
            growth_rate = np.log(target[-1, 0] / target[1, 0]) / ((time[-1] - time[1]))
            doubling_time = np.log(2) / growth_rate
    first_growth_rate = np.log(target[first_index + 1, 0] / target[first_index, 0]) / (
    (time[first_index + 1] - time[first_index]))
    # first_growth_rate = np.log(target[2,0]/target[1,0])/((time[2]-time[1]))
    first_doubling_time = np.log(2) / first_growth_rate
    if len(target) < 3:
        second_growth_rate = np.log(target[first_index + 1, 0] / target[first_index, 0]) / (
        (time[first_index + 1] - time[first_index]))
    else:
        second_growth_rate = np.log(target[first_index + 2, 0] / target[first_index + 1, 0]) / (
        (time[first_index + 2] - time[first_index + 1]))
    second_doubling_time = np.log(2) / second_growth_rate
    first_naive_growth_rate = np.log(target[1, 0] / target[0, 0]) / ((time[1] - time[0]))
    first_naive_doubling_time = np.log(2) / first_naive_growth_rate
    temp = y[:, 0] / y[0, 0]
    (c,) = (temp >= 2).ravel().nonzero()

    # print("*********************************")
    # x = np.log(target[:, 0])
    # first_diff = np.diff(x, n=1, axis=0)
    # time_diff = np.diff(time, n=1, axis=0)
    # second_diff = np.diff(first_diff, n=1, axis=0)
    # print("Max growth rate")
    # print("Max growth rate with {:2.2f} hours of smoothing (~1 doubling)".format(exponential_doubling_time),max(running_mean(first_diff/time_diff,exponential_doubling_time)))
    # print("Max growth rate with {:2.2f} hours of smoothing (~2 doublings)".format(2*exponential_doubling_time),max(running_mean(first_diff/time_diff,2*exponential_doubling_time)))
    # print("Max growth rate with {:2.2f} hours of smoothing (~3 doublings)".format(3*exponential_doubling_time),max(running_mean(first_diff/time_diff,3*exponential_doubling_time)))
    # print("*********************************")

    if c.shape[0] == 0:
        first_simulation_doubling_time = float(u"inf")
    else:
        first_simulation_doubling_time = t_sim[c[0]] - t_sim[0]

    # Write this into an html document:
    report_dictionary = {
        u"tool_name": TOOL_NAME,
        u"version": VERSION,
        u"model_name": title_dictionary[function_name],
        u"MCDS_name": function_name,
        u"model_equations": u"./" + function_name + u"_plane",
        u"number_of_equations": number_of_equations[function_name],
        u"citation_text": open(u"files/citation.txt").read(),
        u"model_description": open(u"files/" + function_name + u"/" + function_name + u"_description.txt").read(),
        u"parameters_table": table,
        u"table_caption": open(out_dir + function_name + u"_table_caption.txt").read(),
        u"model_caption": open(out_dir + function_name + u"_caption.txt").read(),
        u"exponential_doubling_time": exponential_doubling_time,
        u"naive_doubling_time": naive_doubling_time,
        u"doubling_time": doubling_time,
        u"first_doubling_time": first_doubling_time,
        u"second_doubling_time": second_doubling_time,
        u"first_naive_growth_rate": first_naive_doubling_time,
        u"first_simulation_doubling_time": first_simulation_doubling_time,
        u"cell_line_name": unicode(metadata_dictionary[u"cell_line"][u"name"]),
    }

    f.write(report_template.format(report_dictionary))
    f.close()

    return


def PlotData(time, target, sigmas, function_name, fig_counter, color_list):
    markers_dictionary = {
        u"live": (u':^'),
        u"live_logistic": (u':^'),
        u"live_dead": (u':^', u':v'),
        u"live_dead_logistic": (u':^', u':v'),
        u"total": (u':D'),
        u"total_logistic": (u':d'),
        u"total_allee": (u':d'),
    }

    if not os.path.exists(out_dir_root + u"/output/data/"):
        os.makedirs(out_dir_root + u"/output/data/")
    out_dir = out_dir_root + u"/output/data/"
    lines = (u"-", u"--", u"-.", u":")

    number_of_subplots = 1

    fig, axs = plt.subplots(1, number_of_subplots, sharex=True)

    if (function_name == u"total") or (function_name == u"total_logistic") or (function_name == u'total_allee'):
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Total'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Total'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Total'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)
        if True:  # not all_single_bio_rep_flag:
            # axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.', label='Total cells')
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u'.', label=u'Total cells')
    else:
        if True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u':.', label=u'Live cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Live'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Live'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Live'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)

    axs.set_ylabel(u"Number of Cells")

    if target.shape[1] == 2:
        axs.errorbar(time, target[:, 1], yerr=sigmas[:, 1], color=color_list[1], fmt=u':.', label=u'Dead cells')
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Dead'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Dead'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Dead'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)

    plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
    plt.xlabel(u"time (Hours)")

    lgd_1 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
    lgd_2 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

    suptitle = plt.suptitle(u"Data provided", size=24, y=1.01)

    # Saving the figure in multiple formats
    for format in format_list:
        fig.savefig(out_dir + u"data." + format, dpi=300, format=format, bbox_extra_artists=(lgd_1, lgd_2, suptitle,),
                    bbox_inches=u'tight')

    if full_output:
        # Saving the figure data as a pickle file
        pickle.dump((time, target, sigmas,), open(out_dir + u"Data.p", u"wb"))

        # Make the same plots in log-linear space
        plt.gca().set_yscale(u'log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + u"data_loglinear." + format, dpi=300, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, suptitle,), bbox_inches=u'tight')

        # toc()

        plt.close(fig)

        # Now making plots in B&G
        plt.rc(u"axes", color_cycle=u"k")
        fig, axs = plt.subplots(number_of_subplots, sharex=True)

        ROW = 0  # live/total cells
        if (function_name == u"total") or (function_name == u"total_logistic") or (function_name == u"total_allee"):
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label=u'Total cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point][u'Total'][u'single_bio_rep']:
                        for tech_rep in data[time_point][u'Total'][u'bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, unicode):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Total'][u'mean'], u'.',
                                         color=u'#000000', alpha=0.5)
        else:
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label=u'Live cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point][u'Live'][u'single_bio_rep']:
                        for tech_rep in data[time_point][u'Live'][u'bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, unicode):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Live'][u'mean'], u'.',
                                         color=u'#000000', alpha=0.5)

        axs.set_ylabel(u"Number of Cells")

        if target.shape[1] == 2:  # Dead cells
            ROW = 1
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, ROW], yerr=sigmas[:, ROW], fmt=markers_dictionary[function_name][ROW],
                             label=u'Dead cells')
            if show_replicates == True:
                for time_point in data.keys():
                    if data[time_point][u'Dead'][u'single_bio_rep']:
                        for tech_rep in data[time_point][u'Dead'][u'bio_reps']:
                            axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#000000', alpha=0.5)
                    else:
                        for br_key in data[time_point].keys():
                            if not isinstance(br_key, unicode):
                                axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Dead'][u'mean'], u'.',
                                         color=u'#000000', alpha=0.5)

        plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
        plt.xlabel(u"time (Hours)")

        lgd_1 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
        lgd_2 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

        plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
        plt.xlabel(u"time (Hours)")

        suptitle = plt.suptitle(u"Data provided", size=24, y=1.01)

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + u"data_BW." + format, dpi=300, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, suptitle,), bbox_inches=u'tight')

        # Make the same plots in log-linear space
        plt.gca().set_yscale(u'log')

        # Saving the figure in multiple formats
        for format in format_list:
            fig.savefig(out_dir + u"data_loglinear_BW." + format, dpi=300, format=format,
                        bbox_extra_artists=(lgd_1, lgd_2, suptitle,), bbox_inches=u'tight')

    plt.close(fig)

    return


def CustomTable(function_name, params, report):
    # This function creates a xlsx table of the parameters

    headers = [u"Parameter", u"Value", u"Std. Error"]
    rows = []
    for key, value in params.items():
        rows.append([key, value.value, value.stderr])

    wb = Workbook()
    dest_filename = u'Parameter_Estimates.xlsx'

    ws1 = wb.active
    ws1.title = u"Parameters"

    ws1.append(headers)

    for row in xrange(2, len(rows)):
        ws1.append(rows[row])

    ws2 = wb.create_sheet(title=u"LaTeX")
    ws2.append([u"You can also create a table in Latex using this code:"])
    ws2.append([tabulate(rows, headers, tablefmt=u"latex")])

    wb.save(filename=dest_filename)

    return headers, rows


def CustomOutput(function_name, params):
    # This function creates a XLSX file with a custom output (including parameter estimates)

    sheet_dictionary = {
        u"live": u"Live",
        u"live_logistic": u"Live_logistic",
        u"live_dead": u"Live+Dead",
        u"live_dead_logistic": u"Live+Dead_logistic",
        u"total": u"Total_Cells",
        u"total_logistic": u"Total_logistic",
        u"total_allee": u"Total_logistic",
    }

    dest_filename = u'output.xlsx'
    wb = load_workbook(filename=dest_filename)

    ws1 = wb.create_sheet(title=sheet_dictionary[function_name])

    ws1.append([function_name])
    ws1.append([])

    headers = [u"Parameter", u"Value", u"Std. Error"]
    temp_list = []
    for key, value in params.items():
        temp_list.append([key, value.value, value.stderr])

    ws1.append(headers)

    for i in xrange(0, len(temp_list)):
        ws1.append(temp_list[i])

    ws1.append([])
    ws1.append([])

    ws1.append([u"You can also create a table in Latex using this code:"])
    ws1.append([tabulate(temp_list, headers, tablefmt=u"latex")])

    wb.save(filename=dest_filename)

    return


def CustomEstimation(init_params, units, function_name, time, fig_counter, means, stds, sem, color_list):
    if template == u"Total+Viability":
        # y_init_dictionary ={
        #     "L": means[0,[1]],
        #     "T": means[0,[0]],
        #     "LD": means[0,[1,2]],
        #     "live" : means[0,[1]],
        #     "live_logistic" : means[0,[1]],
        #     "live_dead" : means[0,[1,2]],
        #     "live_dead_logistic" : means[0,[1,2]],
        #     "total" : means[0,[0]],
        #     "total_ODE" : means[0,[0]],
        #     "total_logistic" : means[0,[0]],
        # }
        target_dictionary = {
            u"L": means[:, [1]],
            u"T": means[:, [0]],
            u"LD": means[:, [1, 2]],
            u"live": means[:, [1]],
            u"live_logistic": means[:, [1]],
            u"live_dead": means[:, [1, 2]],
            u"live_dead_logistic": means[:, [1, 2]],
            u"total": means[:, [0]],
            u"total_ODE": means[:, [0]],
            u"total_logistic": means[:, [0]],
            u"total_allee": means[:, [0]],
        }
        sigmas_dictionary = {
            u"L": stds[:, [1]],
            u"T": stds[:, [0]],
            u"LD": stds[:, [1, 2]],
            u"live": stds[:, [1]],
            u"live_logistic": stds[:, [1]],
            u"live_dead": stds[:, [1, 2]],
            u"live_dead_logistic": stds[:, [1, 2]],
            u"total": stds[:, [0]],
            u"total_ODE": stds[:, [0]],
            u"total_logistic": stds[:, [0]],
            u"total_allee": stds[:, [0]],
        }
        std_error_dictionary = {
            u"L": sem[:, [1]],
            u"T": sem[:, [0]],
            u"LD": sem[:, [1, 2]],
            u"live": sem[:, [1]],
            u"live_logistic": sem[:, [1]],
            u"live_dead": sem[:, [1, 2]],
            u"live_dead_logistic": sem[:, [1, 2]],
            u"total": sem[:, [0]],
            u"total_ODE": sem[:, [0]],
            u"total_logistic": sem[:, [0]],
            u"total_allee": sem[:, [0]],
        }
    elif template == u"Total_Cells":
        # y_init_dictionary ={
        #     "T": means[0,[0]],
        #     "total" : means[0,[0]],
        #     "gompertz" : means[0,[0]],
        #     "total_logistic" : means[0,[0]],
        # }
        target_dictionary = {
            u"T": means[:, [0]],
            u"total": means[:, [0]],
            u"gompertz": means[:, [0]],
            u"total_logistic": means[:, [0]],
            u"total_allee": means[:, [0]],
            u"logistic_gf": means[:, [0]],
        }
        sigmas_dictionary = {
            u"T": stds[:, [0]],
            u"total": stds[:, [0]],
            u"gompertz": stds[:, [0]],
            u"total_logistic": stds[:, [0]],
            u"total_allee": stds[:, [0]],
            u"logistic_gf": stds[:, [0]],
        }
        std_error_dictionary = {
            u"T": sem[:, [0]],
            u"total": sem[:, [0]],
            u"gompertz": sem[:, [0]],
            u"total_logistic": sem[:, [0]],
            u"total_allee": sem[:, [0]],
            u"logistic_gf": sem[:, [0]],
        }

    correlation_flag = False  # this will be set to True if something goes wrong
    missing_errorbar_flag = False
    # Make a copy of the parameters
    params = copy.deepcopy(init_params)

    # Do fit, here with leastsq model (Levenberg-Marquardt method)

    # Define population initial values
    target = target_dictionary[function_name]
    sigmas = std_error_dictionary[function_name]

    # sigmas = sigmas_dictionary[function_name] # Using standard deviation instead of standard error
    # sigmas = np.power(sigmas,2) # Using variance instead of standard deviation
    y_init = y_init_dictionary[function_name]

    # Invoke the minimizer
    result = minimize(MyError, params, args=(function_name, time, y_init, target, sigmas), method=u'leastsq')

    # write error report
    if (result.errorbars == False):

        print result.var_map

        for key in result.var_map:
            print result.params[key]

        correls = {}
        min_correl = 0.9
        for i, name in enumerate(result.var_map):
            par = result.params[name]

            if not par.vary:
                continue
            if hasattr(par, u'correl') and par.correl is not None:
                for name2 in result.var_map[i + 1:]:
                    if (name != name2 and name2 in par.correl and
                                abs(par.correl[name2]) > min_correl):
                        correls[u"%s, %s" % (name, name2)] = par.correl[name2]
        if any(correls):
            correlation_flag = True
        else:
            missing_errorbar_flag = True
            print u'We could not estimate the standard error of the parameter estimates. This model [{:s}] may not be the correct one to use'.format(
                    function_name)

    # else:
    print u'The estimated parameters parameters are:'
    report_fit(result.params, show_correl=False)
    print u'--------------------'
    # print("The initial error was\t%".format(MAPE(init_params,function_name,time,y_init,target,sigmas)))
    final_error = MAPE(params, function_name, time, y_init, target, sigmas)
    init_error = MAPE(init_params, function_name, time, y_init, target, sigmas)

    print u"The final Mean Absolute Percentage Error (MAPE) is {:2.2f}%".format(final_error),u'(init= {:2.2f}%)'.format(init_error)

    if init_error < final_error:
        print u'\tNote that the algorithm minimizes the Sum of Squared Errors (SSE), this sometimes leads to an increase in MAPE'
        print u"\tThe final SSE is {:2.2g}".format(sum(MyError(params, function_name, time, y_init, target, sigmas) ** 2)), u'(init= {:2.2g})'.format(sum(MyError(init_params, function_name, time, y_init, target, sigmas)) ** 2)

    global uncertainty_note

    # Check if any parameter has a standard error of the same order of magnitude the value of the parameter, ignore parameter values == 0.0
    stderr_flag = False
    for key in params:
        if params[key].value != 0.0:
            if np.log10(params[key].stderr / params[key].value) >= 0:
                stderr_flag = True
                uncertainty_note = uncertainty_note + \
                                   u'//cell_cycle[@model="' + function_name + u'"]/cell_cycle_phase[@name="' + phase[
                                       function_name] + u'"]/' + rate[key]

    # plot results
    CustomPlot(function_name=function_name, y_init=y_init, time=time, params=params, result=result, units=units,
               target=target, sigmas=std_error_dictionary[function_name], fig_counter=fig_counter,
               correlation_flag=correlation_flag, stderr_flag=stderr_flag, color_list=color_list,
               missing_errorbar_flag=missing_errorbar_flag)

    fit_summary = {}
    fit_summary[u'function_name'] = function_name
    fit_summary[u'y_init'] = y_init
    fit_summary[u'parameters'] = params
    fit_summary[u'result'] = result
    return fit_summary


def ReadMetadata(ws):
    user = {
        u"orcid": ws.cell(u"D2").value,
        u"given_names": ws.cell(u"D3").value,
        u"family_name": ws.cell(u"D4").value,
        u"email": ws.cell(u"D5").value,
        u"website": ws.cell(u"D6").value,
        u"organization": ws.cell(u"D7").value,
        u"department": ws.cell(u"D8").value,
        u"list": [
            [u"Given names", ws.cell(u"D3").value, ],
            [u"Family name", ws.cell(u"D4").value, ],
            [u"Email", ws.cell(u"D5").value, ],
            [u"Website", ws.cell(u"D6").value, ],
            [u"Organization", ws.cell(u"D7").value, ],
            [u"Department", ws.cell(u"D8").value, ],
            [u"ORCID", ws.cell(u"D2").value, ],
        ],
    }
    cell_line = {
        u"link": ws.cell(u"D9").value,
        u"citation": ws.cell(u"D10").value,
        u"MultiCellDB": ws.cell(u"D11").value,
        u"name": ws.cell(u"D12").value,
        u"synonyms": ws.cell(u"D13").value,
        u"origins": ws.cell(u"D14").value,
        u"description": ws.cell(u"D15").value,
        u"CLO": ws.cell(u"D16").value,
        u"BTO": ws.cell(u"D17").value,
        u"organism": ws.cell(u"D18").value,
        u"organ": ws.cell(u"D19").value,
        u"disease": ws.cell(u"D20").value,
        u"morphology": ws.cell(u"D21").value,
        u"oxygenation_name": ws.cell(u"D22").value,
        u"oxygenation_level": ws.cell(u"D23").value,
        u"oxygenation_measurement_type": ws.cell(u"D24").value,
        u"time": TIME_STAMP,
        u"caption": ws.cell(u"D26").value,
        u"list": [
            [u"Link to data", ws.cell(u"D9").value, ],
            [u"Citation information", ws.cell(u"D10").value, ],
            [u"MultiCellDB ID", ws.cell(u"D11").value, ],
            [u"Cell line name", ws.cell(u"D12").value, ],
            [u"Synonyms", ws.cell(u"D13").value, ],
            [u"Cell line origins", ws.cell(u"D14").value, ],
            [u"Brief description", ws.cell(u"D15").value, ],
            [u"CLO ID", ws.cell(u"D16").value, ],
            [u"BTO ID", ws.cell(u"D17").value, ],
            [u"Organism", ws.cell(u"D18").value, ],
            [u"Organ", ws.cell(u"D19").value, ],
            [u"Disease", ws.cell(u"D20").value, ],
            [u"Morphology", ws.cell(u"D21").value, ],
            [u"oxygenation level name", ws.cell(u"D22").value, ],
            [u"oxygenation level", ws.cell(u"D23").value, ],
            [u"oxygenation Measurement Type", ws.cell(u"D24").value, ],
            [u"Time of creation", TIME_STAMP, ],
        ]
    }
    metadata_dictionary = {
        u"user": user,
        u"cell_line": cell_line,
    }

    return metadata_dictionary


def ApproximateColor(cell_theme):
    if cell_theme == 0:
        print u"White was set to #FFFFFF."
        hex_val = u"#FFFFFF"
    elif cell_theme == 1:
        print u"Black was set to #000000."
        hex_val = u"#000000"
    elif cell_theme == 2:
        print u"Light Gray was set to #E7E6E6."
        hex_val = u"#E7E6E6"
    elif cell_theme == 3:
        print u"Dark Gray was set to #44546A."
        hex_val = u"#44546A"
    elif cell_theme == 4:
        print u"Sky Blue was set to #5B9BD5."
        hex_val = u"#5B9BD5"
    elif cell_theme == 5:
        print u"Pumpkin Orange was set to #ED7D31."
        hex_val = u"#ED7D31"
    elif cell_theme == 6:
        print u"Gray was set to #A5A5A5."
        hex_val = u"#A5A5A5"
    elif cell_theme == 7:
        print u"Yellow was set to #FFC000."
        hex_val = u"#FFC000"
    elif cell_theme == 8:
        print u"Light Blue was set to #4472C4."
        hex_val = u"#4472C4"
    elif cell_theme == 9:
        print u"Green was set to #70AD47."
        hex_val = u"#70AD47"
    else:
        print u"This color is not recognized, using Aggie Maroon instead #500000."
        hex_val = u"#500000"
    return hex_val


def GetCellColor(cell):
    if cell.fill.start_color.type == u"theme":
        cell_color = ApproximateColor(cell.fill.start_color.theme)
        theme_flag = True
    else:
        cell_color = u'#' + cell.fill.start_color.rgb[2:8]
        theme_flag = False
    return cell_color, theme_flag


def ReadColors(wb):
    ws = wb.get_sheet_by_name(u"Preferences")
    custom_colors_flag = ws.cell(u"B1").value
    live_color = u"#FFFFFF"  # Setting it to white to initialize the variable!
    dead_color = u"#FFFFFF"  # Setting it to white to initialize the variable!
    complete_color_list = [live_color, dead_color, ]
    custom_color_list = []
    theme_flag = False

    if custom_colors_flag == u"Use default colors":
        print u"Using default colors."
        live_color = u"#0000ff"
        dead_color = u"#500000"
        complete_color_list = [live_color, dead_color, ]
    elif custom_colors_flag == u"Use old default colors":
        print u"Using old default colors."
        live_color = u"#7570B3"
        dead_color = u"#000000"
        complete_color_list = [live_color, dead_color, ]
    elif custom_colors_flag == u"Use custom HEX colors":
        print u"Using the rgb HEX colors provided."
        live_color = ws.cell(u"B2").value
        dead_color = ws.cell(u"B3").value
        complete_color_list = [live_color, dead_color, ]
    elif custom_colors_flag == u"Use cell fill color":
        live_color, temp_flag = GetCellColor(ws.cell(u"B2"))
        theme_flag = theme_flag or temp_flag
        dead_color, temp_flag = GetCellColor(ws.cell(u"B3"))
        theme_flag = theme_flag or temp_flag
        complete_color_list = [live_color, dead_color, ]
        if theme_flag:
            print u"\nUnable to convert excel 'Theme Colors' to the proper RGB, using approximate colors instead, as listed above.\n"
            u"Please set your custom colors using excel's 'Standard Colors' or by selecting rgb values using excel's 'Custom Colors...', "
            u"not by picking from the 'Theme colors' list.\n"
            u"For greatest control over the colors, select 'Use custom HEX colors' and pick your hexadecimal values.\n"
            u"Remember to include the '#' sign. You can get those codes from a site like http://www.w3schools.com/tags/ref_colorpicker.asp\n"
    else:
        print u"This situation shouldn't have happend, this situation should have not happened!\n"
        u"The program will error out now. Please check the entry on cell 'B1' under the Preferences sheet.\n"
        u"Goodbye!"

    # The following is a temporary solution. Python is not processing the '#' symbol well.
    complete_color_list = [s.replace(u'#', u'') for s in complete_color_list]
    live_color = live_color.replace(u'#', u'')
    dead_color = dead_color.replace(u'#', u'')
    # End of temporary solution


    if any(current_color == u"#FFFFFF" for current_color in complete_color_list) or \
            any(current_color == u"FFFFFF" for current_color in complete_color_list):
        print u"Warning: one of the selected colors is white."

    template = wb.get_sheet_by_name(u"Metadata").cell(u"B29").value
    # From the complete color list, select only the colors that will be used
    if template == u"Total+Viability":
        custom_color_list = [u'#' + live_color, u'#' + dead_color, ]
    elif template == u"Total_Cells":
        custom_color_list = [u'#' + live_color, ]

    return custom_color_list


def DataTable(data, headers, template):
    # This is a custom function to write the data table, particularly due to the headers

    if template == u"Total_Cells":
        html_text = u"<table>\n\t<tr>"

        for column in headers:
            if column in [u"Days", u"Hours", u"Minutes", u"Seconds"]:
                html_text += u"<th></th>"
            # elif column != "Total cells":
            else:
                html_text += u'<th colspan="2">' + unicode(column) + u"</th>"
        html_text += u"</tr>\n\t<tr>"
        for column in headers:
            if column in [u"Days", u"Hours", u"Minutes", u"Seconds"]:
                html_text += u"<th>" + unicode(column) + u"</th>"
            # elif column != "Total cells":
            else:
                html_text += u"<th>" + u"Mean" + u"</th>" + u"<th>" + u"STD" + u"</th>"
        html_text += u"</tr>\n"
        # now write the cells
        for row in data:
            html_text += u"\t<tr>"
            for col in row:
                html_text += u"<td>" + unicode(col) + u"</td>"
            html_text += u"</tr>\n"
        html_text += u"</table>"
    else:
        html_text = u"<table>\n\t<tr>"

        for column in headers:
            if column in [u"Days", u"Hours", u"Minutes", u"Seconds"]:
                html_text += u"<th></th>"
            elif column != u"Total cells":
                # else:
                html_text += u'<th colspan="2">' + unicode(column) + u"</th>"
        html_text += u"</tr>\n\t<tr>"
        for column in headers:
            if column in [u"Days", u"Hours", u"Minutes", u"Seconds"]:
                html_text += u"<th>" + unicode(column) + u"</th>"
            elif column != u"Total cells":
                # else:
                html_text += u"<th>" + u"Mean" + u"</th>" + u"<th>" + u"STD" + u"</th>"
        html_text += u"</tr>\n"
        # now write the cells
        for row in data:
            html_text += u"\t<tr>"
            for col in row:
                html_text += u"<td>" + unicode(col) + u"</td>"
            html_text += u"</tr>\n"
        html_text += u"</table>"

    return html_text


#######################################################################
######################## Beginning of file ############################
#######################################################################
# Read target to be fitted


# Removing a matplotib warning about tight_layout
import warnings

warnings.filterwarnings(u"ignore", category=UserWarning, module=u"matplotlib")

# load the Microsoft Excel file

show_replicates = True

f = load_workbook(file)
print u"Successfully read the data file named ", file, u"."
out_dir_root = f.get_sheet_by_name(u"Preferences").cell(u"B9").value
print u"Creating the output directory called", out_dir_root

# Create the output directory if it doesn't exists
if not os.path.exists(out_dir_root):
    os.makedirs(out_dir_root)
if not os.path.exists(out_dir_root + u"/output/"):
    os.makedirs(out_dir_root + u"/output/")

# ws is worksheet that contains the metadata
ws = f.get_sheet_by_name(u"Metadata")
metadata_dictionary = ReadMetadata(ws)

print u"Hello", metadata_dictionary[u"user"][u"given_names"], u"\n"

# Reading the graph colors
color_list = ReadColors(f)
# Reading the rest of the preferences

time_input_units = f.get_sheet_by_name(u"Preferences").cell(u"B8").value
if time_input_units == u'Days':
    conversion_factor = 24
elif time_input_units == u'Hours':
    conversion_factor = 1
elif time_input_units == u'Minutes':
    conversion_factor = 1 / 60
elif time_input_units == u'Seconds':
    conversion_factor = 1 / (60 * 60)
else:
    print u'Time unit "{:s}" not supported'.format(time_input_units)

# Now save data into a standard table
# ws is worksheet that contains the data
template = f.get_sheet_by_name(u"Metadata").cell(u"B29").value
template_type = f.get_sheet_by_name(u"Metadata").cell(u"B31").value
ws = f.get_sheet_by_name(template)
print u"Now working with", ws

# time_measurement = f.get_sheet_by_name("Preferences").cell("B8").value
time_measurement = u"Hours"
# Depending on the template, set up the variables:
if template == u"Total+Viability":
    number_of_measurements = 2
    number_of_headers = 3
    if template_type == u"Means+SE":
        data_cols = [1, 4, ]
    else:
        data_cols = [3, 4, ]
    table_headers = [time_measurement, u"Total cells", u"Live cells", u"Dead cells"]
    model_list = [u"live", u"live_logistic",
                  u"total", u"total_logistic",
                  u"live_dead", u"live_dead_logistic", ]
    # model_list = [  "live_dead"] # For Debugging purposes
elif template == u"Total_Cells":
    number_of_measurements = 1
    number_of_headers = 1
    if template_type == u"Means+SE":
        data_cols = [1, ]
    else:
        data_cols = [3, ]
    table_headers = [time_measurement, u"Total cells"]
    model_list = [  u"total",u"total_logistic",]

else:
    print u"An unexpected error happened. Program will error out now."
data_points = len(ws.columns[0]) - 1  # Read how many columns are there in the file, this is the number of time points\
# row_count = ws.get_highest_row() - 1

# Check if the bottom rows are emtpy, if so throw them away
while (ws.columns[0][data_points].value == None):
    data_points -= 1

data = {}

if template_type == u"Means+SE":

    show_replicates = False

    # Reading in the data in OpenPyXL cell format
    time_cells = ws.columns[0][2:data_points + 1]
    time = np.array([i.value for i in time_cells])
    means = np.zeros((len(np.unique(time)), number_of_headers))
    stds = np.zeros((len(np.unique(time)), number_of_headers))
    sem = np.zeros((len(np.unique(time)), number_of_headers))

    replicate_values = []

    data_col_dictionary = {
        0: 1,  # total
        1: 4,  # live
        2: 0,  # dead -- not to be used
    }

    col_index = 0
    # for current_data_col in data_cols:
    for current_data_header in xrange(number_of_headers):
        current_data_col = data_col_dictionary[current_data_header]
        mean_cells = ws.columns[current_data_col][2:data_points + 1]
        sem_cells = ws.columns[current_data_col + 1][2:data_points + 1]
        stds_cells = ws.columns[current_data_col + 2][2:data_points + 1]

        for current_row in xrange(len(mean_cells)):
            if col_index in [0, 1]:  # Total cells and Live Cells
                means[current_row, col_index] = mean_cells[current_row].value
                stds[current_row, col_index] = stds_cells[current_row].value
                sem[current_row, col_index] = sem_cells[current_row].value
            else:
                total_mean_cells = ws.columns[1][2:data_points + 1]
                total_sem_cells = ws.columns[2][2:data_points + 1]
                total_stds_cells = ws.columns[3][2:data_points + 1]
                live_mean_cells = ws.columns[4][2:data_points + 1]
                live_sem_cells = ws.columns[5][2:data_points + 1]
                live_stds_cells = ws.columns[6][2:data_points + 1]
                if col_index == 2:  # dead cells
                    means[current_row, col_index] = total_mean_cells[current_row].value - live_mean_cells[
                        current_row].value
                    sem[current_row, col_index] = total_sem_cells[current_row].value * (
                    total_mean_cells[current_row].value - live_mean_cells[current_row].value) / total_mean_cells[
                                                      current_row].value
                    stds[current_row, col_index] = total_stds_cells[current_row].value * (
                    total_mean_cells[current_row].value - live_mean_cells[current_row].value) / total_mean_cells[
                                                       current_row].value
                else:
                    print u"This should have not happened."
            stds[current_row, col_index] = max(stds[current_row, col_index], 1e-16)
            sem[current_row, col_index] = max(sem[current_row, col_index], 1e-16)

        col_index += 1

    if time_input_units == u'Days':
        time = time * 24
        conversion_factor = 24
    elif time_input_units == u'Hours':
        conversion_factor = 1
    elif time_input_units == u'Minutes':
        time = time / 60
        conversion_factor = 1 / 60
    elif time_input_units == u'Seconds':
        time = time / (60 * 60)
        conversion_factor = 1 / (60 * 60)
# elif template_type == "Technical replicates for each biological replicates":
else:
    print u'Technical replicates and biological replicates'
    # Reading in the data in OpenPyXL cell format
    time_cells = ws.columns[2][1:data_points + 1]
    biological_replicate_cells = ws.columns[0][1:data_points + 1]
    technical_replicate_cells = ws.columns[1][1:data_points + 1]
    data_cells_total = ws.columns[3][1:data_points + 1]
    if number_of_headers == 1:
        data_cells_viable = data_cells_total
    else:
        data_cells_viable = ws.columns[4][1:data_points + 1]

    # Converting the cell arrays to NumPy arrays
    ti_array = np.array([i.value for i in time_cells])
    br_array = np.array([i.value for i in biological_replicate_cells])
    tr_array = np.array([i.value for i in technical_replicate_cells])
    to_array = np.array([i.value for i in data_cells_total])
    vi_array = np.array([i.value for i in data_cells_viable])
    de_array = to_array - vi_array

    time = np.empty(len(np.unique(ti_array)))
    means = np.empty((len(np.unique(ti_array)), number_of_headers))
    stds = np.empty((len(np.unique(ti_array)), number_of_headers))
    sem = np.empty((len(np.unique(ti_array)), number_of_headers))

    tech_reps_time = []
    tech_reps_total = []
    tech_reps_viable = []

    bio_reps_time = []
    bio_reps_total_mean = []
    bio_reps_total_sem = []
    bio_reps_viable = []

    test_time = []

    time_keys, indices, counts = np.unique(ti_array, return_index=True, return_counts=True, )

    number_of_biological_replicates = 0
    number_of_technical_replicates = 0

    single_bio_rep_flag = False
    all_single_bio_rep_flag = False

    row_counter = 0
    for time_key in time_keys:
        test_time.append(time_key)
        data[time_key] = {}
        br_keys, tech_replicate_counts = np.unique(br_array[ti_array == time_key], return_counts=True)

        temp_br_array_total = np.empty(len(br_keys))
        temp_br_array_viable = np.empty(len(br_keys))
        temp_br_array_dead = np.empty(len(br_keys))
        temp_br_index = 0

        number_of_biological_replicates = max([number_of_biological_replicates, len(br_keys)])
        number_of_technical_replicates = max([number_of_technical_replicates, max(tech_replicate_counts)])
        # print(bio_rep,tech_rep)
        data[time_key][u'Total'] = {}
        data[time_key][u'Live'] = {}
        data[time_key][u'Dead'] = {}

        for br_key in br_keys:
            data[time_key][br_key] = {}
            data[time_key][br_key][u'Total'] = {}
            data[time_key][br_key][u'Live'] = {}
            data[time_key][br_key][u'Dead'] = {}

            # print(br_key)
            # print(br_array==br_key)
            # print(ti_array==time_key)
            # print(np.logical_and((br_array==br_key),(ti_array==time_key)))
            # print((br_array==br_key)&(ti_array==time_key),'\n')

            # print((br_array==br_key and (ti_array==time_key)))
            tech_reps_time.append(ti_array[((br_array == br_key) & (ti_array == time_key))])
            tech_reps_total.append(to_array[((br_array == br_key) & (ti_array == time_key))])
            tech_reps_viable.append(vi_array[((br_array == br_key) & (ti_array == time_key))])

            bio_reps_time.append(time_key)
            bio_reps_total_mean.append(np.mean(to_array[((br_array == br_key) & (ti_array == time_key))]))
            bio_reps_total_sem.append(np.std(to_array[((br_array == br_key) & (ti_array == time_key))]) / len(
                to_array[((br_array == br_key) & (ti_array == time_key))]))
            bio_reps_viable = []

            data[time_key][br_key][u'Total'][u'median'] = np.median(
                to_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Total'][u'mean'] = np.mean(to_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Total'][u'std'] = np.std(to_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Total'][u'sem'] = data[time_key][br_key][u'Total'][u'std'] / len(
                to_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Total'][u'tech_reps'] = to_array[((br_array == br_key) & (ti_array == time_key))]
            data[time_key][br_key][u'Total'][u'tech_reps_time'] = ti_array[
                ((br_array == br_key) & (ti_array == time_key))]
            temp_br_array_total[temp_br_index] = np.mean(to_array[((br_array == br_key) & (ti_array == time_key))])

            data[time_key][br_key][u'Live'][u'median'] = np.median(
                vi_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Live'][u'mean'] = np.mean(vi_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Live'][u'std'] = np.std(vi_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Live'][u'sem'] = data[time_key][br_key][u'Live'][u'std'] / len(
                vi_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Live'][u'tech_reps'] = vi_array[((br_array == br_key) & (ti_array == time_key))]
            data[time_key][br_key][u'Live'][u'tech_reps_time'] = ti_array[((br_array == br_key) & (ti_array == time_key))]
            temp_br_array_viable[temp_br_index] = np.mean(vi_array[((br_array == br_key) & (ti_array == time_key))])

            data[time_key][br_key][u'Dead'][u'median'] = np.median(
                de_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Dead'][u'mean'] = np.mean(de_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Dead'][u'std'] = np.std(de_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Dead'][u'sem'] = data[time_key][br_key][u'Dead'][u'std'] / len(
                de_array[((br_array == br_key) & (ti_array == time_key))])
            data[time_key][br_key][u'Dead'][u'tech_reps'] = de_array[((br_array == br_key) & (ti_array == time_key))]
            data[time_key][br_key][u'Dead'][u'tech_reps_time'] = ti_array[((br_array == br_key) & (ti_array == time_key))]
            temp_br_array_dead[temp_br_index] = np.mean(de_array[((br_array == br_key) & (ti_array == time_key))])

            temp_br_index += 1
        if len(br_keys) == 1:  # Time 'time_key' has only one biological replicate
            # We take its variability from the technical replicates

            all_single_bio_rep_flag = all_single_bio_rep_flag | True
            single_bio_rep_flag = True

            data[time_key][u'Total'][u'bio_reps'] = data[time_key][br_keys[0]][u'Total'][u'tech_reps']
            data[time_key][u'Total'][u'median'] = data[time_key][br_keys[0]][u'Total'][u'median']
            data[time_key][u'Total'][u'mean'] = data[time_key][br_keys[0]][u'Total'][u'mean']
            data[time_key][u'Total'][u'std'] = data[time_key][br_keys[0]][u'Total'][u'std']
            data[time_key][u'Total'][u'sem'] = data[time_key][br_keys[0]][u'Total'][u'sem']
            data[time_key][u'Total'][u'single_bio_rep'] = True

            data[time_key][u'Live'][u'bio_reps'] = data[time_key][br_keys[0]][u'Live'][u'tech_reps']
            data[time_key][u'Live'][u'median'] = data[time_key][br_keys[0]][u'Live'][u'median']
            data[time_key][u'Live'][u'mean'] = data[time_key][br_keys[0]][u'Live'][u'mean']
            data[time_key][u'Live'][u'std'] = data[time_key][br_keys[0]][u'Live'][u'std']
            data[time_key][u'Live'][u'sem'] = data[time_key][br_keys[0]][u'Live'][u'sem']
            data[time_key][u'Live'][u'single_bio_rep'] = True

            data[time_key][u'Dead'][u'bio_reps'] = data[time_key][br_keys[0]][u'Dead'][u'tech_reps']
            data[time_key][u'Dead'][u'median'] = data[time_key][br_keys[0]][u'Dead'][u'median']
            data[time_key][u'Dead'][u'mean'] = data[time_key][br_keys[0]][u'Dead'][u'mean']
            data[time_key][u'Dead'][u'std'] = data[time_key][br_keys[0]][u'Dead'][u'std']
            data[time_key][u'Dead'][u'sem'] = data[time_key][br_keys[0]][u'Dead'][u'sem']
            data[time_key][u'Dead'][u'single_bio_rep'] = True

        else:
            all_single_bio_rep_flag = False

            data[time_key][u'Total'][u'bio_reps'] = temp_br_array_total
            data[time_key][u'Total'][u'median'] = np.median(temp_br_array_total)
            data[time_key][u'Total'][u'mean'] = np.mean(temp_br_array_total)
            data[time_key][u'Total'][u'std'] = np.std(temp_br_array_total)
            data[time_key][u'Total'][u'sem'] = np.std(temp_br_array_total) / len(temp_br_array_total)
            data[time_key][u'Total'][u'single_bio_rep'] = False

            data[time_key][u'Live'][u'bio_reps'] = temp_br_array_viable
            data[time_key][u'Live'][u'median'] = np.median(temp_br_array_viable)
            data[time_key][u'Live'][u'mean'] = np.mean(temp_br_array_viable)
            data[time_key][u'Live'][u'std'] = np.std(temp_br_array_viable)
            data[time_key][u'Live'][u'sem'] = np.std(temp_br_array_viable) / len(temp_br_array_viable)
            data[time_key][u'Live'][u'single_bio_rep'] = False

            data[time_key][u'Dead'][u'bio_reps'] = temp_br_array_dead
            data[time_key][u'Dead'][u'median'] = np.median(temp_br_array_dead)
            data[time_key][u'Dead'][u'mean'] = np.mean(temp_br_array_dead)
            data[time_key][u'Dead'][u'std'] = np.std(temp_br_array_dead)
            data[time_key][u'Dead'][u'sem'] = np.std(temp_br_array_dead) / len(temp_br_array_dead)
            data[time_key][u'Dead'][u'single_bio_rep'] = False

        for col_counter in xrange(number_of_headers):
            if col_counter == 0:
                means[row_counter, col_counter] = data[time_key][u'Total'][u'mean']
                stds[row_counter, col_counter] = data[time_key][u'Total'][u'std']
                sem[row_counter, col_counter] = data[time_key][u'Total'][u'sem']
            elif col_counter == 1:
                means[row_counter, col_counter] = data[time_key][u'Live'][u'mean']
                stds[row_counter, col_counter] = data[time_key][u'Live'][u'std']
                sem[row_counter, col_counter] = data[time_key][u'Live'][u'sem']
            elif col_counter == 2:
                means[row_counter, col_counter] = data[time_key][u'Dead'][u'mean']
                stds[row_counter, col_counter] = data[time_key][u'Dead'][u'std']
                sem[row_counter, col_counter] = data[time_key][u'Dead'][u'sem']
            stds[row_counter, col_counter] = max(stds[row_counter, col_counter], 1e-16)
            sem[row_counter, col_counter] = max(sem[row_counter, col_counter], 1e-16)
        time[row_counter] = time_key

        row_counter += 1

    if time_input_units == u'Days':
        time = time * 24
        conversion_factor = 24
    elif time_input_units == u'Hours':
        conversion_factor = 1
    elif time_input_units == u'Minutes':
        time = time / 60
        conversion_factor = 1 / 60
    elif time_input_units == u'Seconds':
        time = time / (60 * 60)
        conversion_factor = 1 / (60 * 60)

        # print(tr_array[br_array[ti_array==time_key]==br_key])
    if (all_single_bio_rep_flag):
        print u"All of the time points had a single biological replicate, variability will be computed from technical replicates"
    elif (single_bio_rep_flag):
        print u"Some data points had a single biological replicate, some others had more than one. "
        u"Note that the algorithm will try to fit data points with smaller variability. This may lead to some problems. "
        u"We recommend running experiments with more than one biological replicate. If this is preeliminary data "
        u"and the result of the parameter estimation seem incorrect, then try to use the same number of biological "
        u"replicates across different data points so the variability in the datapoints (e.g. variances) are compatible with each other."
    if number_of_biological_replicates < 5:
        show_replicates = True

        # print(data[0])
        # print(tech_reps_time)
        # print(tech_reps_total)

if template == u"Total+Viability":
    y_init_dictionary = {
        u"L": means[0, [1]],
        u"T": means[0, [0]],
        u"LD": means[0, [1, 2]],
        u"live": means[0, [1]],
        u"live_logistic": means[0, [1]],
        u"live_dead": means[0, [1, 2]],
        u"live_dead_logistic": means[0, [1, 2]],
        u"total": means[0, [0]],
        u"total_ODE": means[0, [0]],
        u"total_logistic": means[0, [0]],
        u"total_allee": means[0, [0]],
        u"gompertz": means[0, [0]],
    }

elif template == u"Total_Cells":
    y_init_dictionary = {
        u"T": means[0, [0]],
        u"total": means[0, [0]],
        u"gompertz": means[0, [0]],
        u"total_logistic": means[0, [0]],
        u"total_allee": means[0, [0]],
        u"logistic_gf": means[0, [0]],
    }

else:
    print u"An unexpected error happened. Program will error out now."


    def PlotTechnicalReplicates(time, data, time_b, data_b_m, data_b_sem):
        if len(time) != len(data):
            warnings.warn(u"The length of 'time' must be the same length as 'data'")
            print len(time), len(data)
            raise SystemExit
        [plt.plot(time_temp, tech_reps, u'b+') for (time_temp, tech_reps) in izip(time, data)]
        plt.plot(time_b, data_b_m, )

        plt.show()

        return

f = open(out_dir_root + u"/output/data_summary.csv", u"w")
for column in table_headers:
    if column in [u"Days", u"Hours", u"Minutes", u"Seconds"]:
        f.write(unicode(column) + u",")
    else:
        f.write(unicode(column) + u"-mean,")
        f.write(unicode(column) + u"-std,")
f.write(u"\n")
for row in xrange(len(time)):
    f.write(u"{:2.1f}".format(time[row] / conversion_factor) + u",")
    for i in xrange(number_of_measurements):
        f.write(u"{:1.3g}".format(means[row][i]) + u"," + u"{:1.3g}".format(stds[row][i]) + u",")
    f.write(u"\n")
f.close()

data_summary = []
for row in xrange(len(time)):
    temp_row = []
    temp_row.append(u"{:2.1f}".format(time[row]))
    for i in xrange(number_of_measurements):
        temp_row.append(u"{:1.3g}".format(means[row][i]))
        temp_row.append(u"{:1.3g}".format(stds[row][i]))
    data_summary.append(temp_row)

f = open(out_dir_root + u"/output/data_summary.txt", u"w")
f.write(DataTable(data=data_summary, headers=table_headers, template=template))
f.close()

if template == u"Total+Viability":
    data_key = u"live_dead"
    target_dictionary = {
        u"L": means[:, [1]],
        u"T": means[:, [0]],
        u"LD": means[:, [1, 2]],
        u"live": means[:, [1]],
        u"live_logistic": means[:, [1]],
        u"live_dead": means[:, [1, 2]],
        u"live_dead_logistic": means[:, [1, 2]],
        u"total": means[:, [0]],
        u"total_logistic": means[:, [0]],
        u"total_allee": means[:, [0]],
    }
    sigmas_dictionary = {
        u"L": stds[:, [1]],
        u"T": stds[:, [0]],
        u"LD": stds[:, [1, 2]],
        u"live": stds[:, [1]],
        u"live_logistic": stds[:, [1]],
        u"live_dead": stds[:, [1, 2]],
        u"live_dead_logistic": stds[:, [1, 2]],
        u"total": stds[:, [0]],
        u"total_logistic": stds[:, [0]],
        u"total_allee": stds[:, [0]],
    }
    std_error_dictionary = {
        u"L": sem[:, [1]],
        u"T": sem[:, [0]],
        u"LD": sem[:, [1, 2]],
        u"live": sem[:, [1]],
        u"live_logistic": sem[:, [1]],
        u"live_dead": sem[:, [1, 2]],
        u"live_dead_logistic": sem[:, [1, 2]],
        u"total": sem[:, [0]],
        u"total_logistic": sem[:, [0]],
        u"total_allee": sem[:, [0]],
    }
elif template == u"Total_Cells":
    data_key = u"total"
    target_dictionary = {
        u"T": means[:, [0]],
        u"total": means[:, [0]],
        u"total_logistic": means[:, [0]],
        u"total_allee": means[:, [0]],
    }
    sigmas_dictionary = {
        u"T": stds[:, [0]],
        u"total": stds[:, [0]],
        u"total_logistic": stds[:, [0]],
        u"total_allee": stds[:, [0]],
    }
    std_error_dictionary = {
        u"T": sem[:, [0]],
        u"total": sem[:, [0]],
        u"total_logistic": sem[:, [0]],
        u"total_allee": sem[:, [0]],
    }

########################################################################################################################
########################################################################################################################


fig_counter = 1
target = target_dictionary[data_key]
# sigmas = sigmas_dictionary[data_key] #Ploting the variance instead of the standard error
sigmas = std_error_dictionary[data_key]  # Ploting the standard error instead of the variances

if drop_values:
    # Drop all the points where the first diff is negative and/or the second diff is positive
    x = np.log(target[:, 0])
    first_diff = np.diff(x, n=1, axis=0)
    second_diff = np.diff(first_diff, n=1, axis=0)
    (a,) = (first_diff >= 0).ravel().nonzero()
    (b,) = (second_diff <= 0).ravel().nonzero()

    if len(a) == 0:
        print u"These data does not behave like exponential growth, cells do not even increase in value."
        a = [0]
    if len(b) == 0:
        print u"These data does not behave like exponential growth, cells appear to be growing close to logistic limits from the beggining of experiment."
        b = [0]
    first_index = max(a[0], b[0])

    # Find the index when the cell population decreases by at least 10%
    (last_index,) = (np.diff(target[first_index:, 0]) < -0.1 * target[first_index:-1, 0]).ravel().nonzero()

    if last_index.size == 0:
        last_index = len(x) - 1  # If population doesn't decrease, then use all of the time points
    else:
        last_index = last_index[0] + first_index

    print u"Dropping {:d} time points at the beginning and keeping up to time point number {:d}".format(first_index,
                                                                                                       last_index + 1)

    old_target = np.copy(target)
    target = target[first_index:last_index + 1, :]

    old_means = np.copy(means)
    means = means[first_index:last_index + 1, :]

    old_sem = np.copy(sem)
    sem = sem[first_index:last_index + 1, :]

    old_stds = np.copy(stds)
    stds = stds[first_index:last_index + 1, :]

    old_time = np.copy(time)
    time = time[first_index:last_index + 1]

    old_sigmas = np.copy(sigmas)
    sigmas = sigmas[first_index:last_index + 1, :]



PlotData(time, target, sigmas, data_key, fig_counter, color_list)

fig_counter += 1
units = dict()
models_chi = []
models_mape = []
models_chi_table = []
models_mape_table = []

def running_mean(x,N):
    cumsum = np.cumsum(np.insert(x,0,0))
    return (cumsum[N:]-cumsum[:-N])/N

######################### Start the Optimization ##########################

# Compute a first guess for the growth rate:
gr_init = np.log(target[-1, 0] / target[0, 0]) / (time[-1] - time[0])

# First guess for the carrying capacity as twice the largest value measured:
cc_init = max(target[:, 0] * 10)

fit_summary_dict = {}

function_name = u"live"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'growth_rate', value=gr_init, min=0)
    init_params.add(u'seeding_cells', value=y_init_dictionary[function_name][0], min=0)
    units.update(growth_rate=u"1/hours")
    units.update(seeding_cells=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1

function_name = u"live_logistic"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'growth_rate', value=gr_init, min=0)
    init_params.add(u'carrying_capacity', value=cc_init, min=0)
    init_params.add(u'seeding_cells', value=y_init_dictionary[function_name][0], min=0)
    units.update(growth_rate=u"1/hours")
    units.update(carrying_capacity=u"number of cells")
    units.update(seeding_cells=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1

function_name = u"total"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'growth_rate', value=gr_init, min=0)
    init_params.add(u'seeding_cells', value=y_init_dictionary[function_name][0], min=0)
    units.update(growth_rate=u"1/hours")
    units.update(seeding_cells=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1

function_name = u"gompertz"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'growth_rate', value=gr_init, min=0)
    init_params.add(u'carrying_capacity', value=cc_init, min=0)
    init_params.add(u'seeding_cells', value=y_init_dictionary[function_name][0], min=0)
    units.update(growth_rate=u"1/hours")
    units.update(carrying_capacity=u"number of cells")
    units.update(seeding_cells=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1

function_name = u"total_logistic"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'growth_rate', value=gr_init, min=0)
    init_params.add(u'carrying_capacity', value=cc_init, min=0)
    init_params.add(u'seeding_cells', value=y_init_dictionary[function_name][0], min=0)
    units.update(growth_rate=u"1/hours")
    units.update(carrying_capacity=u"number of cells")
    units.update(seeding_cells=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1

function_name = u"logistic_gf"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'growth_rate', value=gr_init, min=0)
    init_params.add(u'carrying_capacity', value=cc_init, min=0)
    init_params.add(u'seeding_cells', value=y_init_dictionary[function_name][0], min=0)
    units.update(growth_rate=u"1/hours")
    units.update(carrying_capacity=u"number of cells")
    units.update(seeding_cells=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=0.01*sem/sem, sem=0.01*sem/sem, color_list=color_list)
    fig_counter += 1

function_name = u"total_allee"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'growth_rate', value=gr_init, min=0)
    init_params.add(u'carrying_capacity', value=cc_init, min=0)
    init_params.add(u'seeding_cells', value=y_init_dictionary[function_name][0], min=0)
    # init_params.add('minimum_population', value=0.11*y_init_dictionary[function_name][0], min=0, max=y_init_dictionary[function_name][0] - 1e-16)
    init_params.add(u'minimum_population', value=0.065, vary=False)
    units.update(growth_rate=u"1/hours")
    units.update(carrying_capacity=u"number of cells")
    units.update(seeding_cells=u"number of cells")
    units.update(minimum_population=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1

function_name = u"live_dead"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'birth_rate', value=gr_init * 0.9, min=0)
    init_params.add(u'death_rate', value=gr_init * 0.1, min=0)
    init_params.add(u'clearance_rate', value=gr_init * 0.01, min=0)
    init_params.add(u'seeding_cells_live', value=y_init_dictionary[function_name][0], min=0)
    init_params.add(u'seeding_cells_dead', value=y_init_dictionary[function_name][1], min=0)
    units.update(birth_rate=u"1/hours")
    units.update(death_rate=u"1/hours")
    units.update(clearance_rate=u"1/hours")
    units.update(seeding_cells_live=u"number of cells")
    units.update(seeding_cells_dead=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1

function_name = u"live_dead_logistic"
if function_name in model_list:
    print u"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print u"We are fitting the model named", function_name
    # create a set of Parameters
    init_params = Parameters()
    init_params.add(u'birth_rate', value=gr_init * 0.9, min=0)
    init_params.add(u'death_rate', value=gr_init * 0.1, min=0)
    init_params.add(u'clearance_rate', value=gr_init * 0.01, min=0)
    init_params.add(u'carrying_capacity', value=cc_init, min=0)
    init_params.add(u'seeding_cells_live', value=y_init_dictionary[function_name][0], min=0)
    init_params.add(u'seeding_cells_dead', value=y_init_dictionary[function_name][1], min=0)
    units.update(birth_rate=u"1/hours")
    units.update(death_rate=u"1/hours")
    units.update(clearance_rate=u"1/hours")
    units.update(carrying_capacity=u"number of cells")
    units.update(seeding_cells_live=u"number of cells")
    units.update(seeding_cells_dead=u"number of cells")
    fit_summary_dict[function_name] = CustomEstimation(init_params=init_params, units=units,
                                                       function_name=function_name, time=time, fig_counter=fig_counter,
                                                       means=means, stds=sem, sem=sem, color_list=color_list)
    fig_counter += 1


def PlotAll():
    # It assumes that we have acces to:
    # data, choose_model, fit_summary_dict
    number_of_subplots = 1
    fig, axs = plt.subplots(1, number_of_subplots, sharex=True)

    # First plot the data:
    if template == u'Total_Cells':
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Total'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Total'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Total'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)
            if True:  # not all_single_bio_rep_flag:
                # axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=':.',
                #              label='Measured Total cells', alpha=0.5)
                axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u'.',
                             label=u'Measured Total cells', alpha=0.5)
                # axs.errorbar(time,target[:,0],yerr=sigmas[:,0],color=color_list[0],fmt=':.',label='Measured Total cells',alpha=0.5)
        elif True:  # not all_single_bio_rep_flag:
            axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u':.',
                         label=u'Measured Total cells')
    elif template == u'Total+Viability':
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Live'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Live'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Live'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u':.',
                             label=u'Measured Live cells', alpha=0.5)
        else:
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u':.',
                             label=u'Measured Live cells')
    else:
        print u'An unexpected error happened [the template selecte id not one of the two valid options "Total_Cells", or "Total+Viability"]. CellPD will error out now.'

    axs.set_ylabel(u"Number of Cells")

    if template == u'Total+Viability':
        if show_replicates == True:
            for time_point in data.keys():
                if data[time_point][u'Dead'][u'single_bio_rep']:
                    for tech_rep in data[time_point][u'Dead'][u'bio_reps']:
                        axs.plot(time_point * conversion_factor, tech_rep, u'.', color=u'#808080', alpha=0.5)
                else:
                    for br_key in data[time_point].keys():
                        if not isinstance(br_key, unicode):
                            axs.plot(time_point * conversion_factor, data[time_point][br_key][u'Dead'][u'mean'], u'.',
                                     color=color_list[0], alpha=0.5)
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, 0], yerr=sigmas[:, 0], color=color_list[0], fmt=u':.',
                             label=u'Measured Dead cells', alpha=0.5)
        else:
            if True:  # not all_single_bio_rep_flag:
                axs.errorbar(time, target[:, 1], yerr=sigmas[:, 1], color=color_list[1], fmt=u':.',
                             label=u'Measured Dead cells')

    # Plot all models in the same graph
    for function_name in model_list:
        params = fit_summary_dict[function_name][u'parameters']
        # Simulation time is a vector of 100 evenly spaced values in the interval [experiment_initial_time,experiemnt_final_time]
        t_sim = np.array(np.linspace(np.min(time), np.max(time), num=100))
        if function_name in [u'live', u'total', u'gompertz', u'logistic_gf']:
            y_init = params[u'seeding_cells']
            y = choose_model[function_name](y_init, t_sim, params)
        elif function_name in [u'live_logistic', u'total_logistic',u'total_allee']:
            y = odeint(choose_model[function_name], params[u'seeding_cells'], t_sim, args=(params,))
        elif function_name in [u'live_dead', u'live_dead_logistic']:
            y = odeint(choose_model[function_name], [params[u'seeding_cells_live'], params[u'seeding_cells_live']], t_sim,
                       args=(params,))
        else:
            print u'The model [:s] is not supported.'.format(function_name)
        axs.plot(t_sim, y, u'--', label=function_name)

    plt.xlim(xmin=plt.xlim()[0] - 0.05, xmax=plt.xlim()[1] + 0.05)
    plt.xlabel(u"time (Hours)")

    lgd_1 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
    lgd_2 = axs.legend(loc=u'center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

    suptitle = plt.suptitle(u"All Models", size=24, y=1.01)

    # Saving the figure in multiple formats
    out_dir = out_dir_root + u"/output/data/"
    for format in format_list:
        fig.savefig(out_dir + u'all_models' + u"." + format, dpi=300, format=format, bbox_inches=u'tight')
    plt.close(fig)

    return


PlotAll()

sorted_models = sorted(models_chi, key=lambda model: float(model[1]), reverse=False)
sorted_models_2 = sorted(models_mape, key=lambda model: float(model[1].strip(u'%')), reverse=False)

sorted_models_table = sorted(models_chi_table, key=lambda model: float(model[1]), reverse=False)
sorted_models_2_table = sorted(models_mape_table, key=lambda model: model[1], reverse=False)

# Writing the Digital Cell Line xml file

# First concatenate all the estimated models
models_concatenated = u""
for model in model_list:
    f = open(out_dir_root + u"/output/" + model + u"/" + model + u"_partial.xml")
    models_concatenated = models_concatenated + u"\n" + f.read()
    f.close()

xml_dictionary = {
    u"MCDS_label": unicode(metadata_dictionary[u"cell_line"][u"name"]),
    u"MCDS_name": unicode(metadata_dictionary[u"cell_line"][u"MultiCellDB"]),
    u"cell_description": unicode(metadata_dictionary[u"cell_line"][u"description"]),
    u"tool_name": TOOL_NAME,
    u"version": VERSION,
    u"data_url": unicode(metadata_dictionary[u"cell_line"][u"link"]),
    u"short_date": get_time.strftime(u"%Y-%m-%d"),
    u"long_date": TIME_STAMP,
    u"user_ORCID": unicode(metadata_dictionary[u"user"][u"orcid"]),
    u"user_given_names": unicode(metadata_dictionary[u"user"][u"given_names"]),
    u"user_family_name": unicode(metadata_dictionary[u"user"][u"family_name"]),
    u"user_email": unicode(metadata_dictionary[u"user"][u"email"]),
    u"user_organization": unicode(metadata_dictionary[u"user"][u"organization"]),
    u"user_department": unicode(metadata_dictionary[u"user"][u"department"]),
    u"user_url": unicode(metadata_dictionary[u"user"][u"website"]),
    u"CLO_id": unicode(metadata_dictionary[u"cell_line"][u"CLO"]).zfill(7),
    u"BTO_id": unicode(metadata_dictionary[u"cell_line"][u"BTO"]).zfill(7),
    u"cell_species": unicode(metadata_dictionary[u"cell_line"][u"organism"]),
    u"cell_organ": unicode(metadata_dictionary[u"cell_line"][u"organ"]),
    u"cell_disease": unicode(metadata_dictionary[u"cell_line"][u"disease"]),
    u"cell_morphology": unicode(metadata_dictionary[u"cell_line"][u"morphology"]),
    u"uncertainty_note": uncertainty_note,
    u"experiment_oxygenation_type": unicode(metadata_dictionary[u"cell_line"][u"oxygenation_name"]),
    u"experiment_oxygenation_measurement_type": unicode(metadata_dictionary[u"cell_line"][u"oxygenation_measurement_type"]),
    u"experiment_oxygenation": unicode(metadata_dictionary[u"cell_line"][u"oxygenation_level"]),
    u"cell_cycle_models": models_concatenated,

}
f = open(out_dir_root + u"/output/" + unicode(metadata_dictionary[u"cell_line"][u"MultiCellDB"]) + u".xml", u"w")
xml_template = open(u"files/DCL_Python_template.txt").read()
f.write(xml_template.format(xml_dictionary))
f.close()

# Writing the overall report
f = open(out_dir_root + u"/overall_report.html", u"w")

html_dictionary = {
    u"tool_name": TOOL_NAME,
    u"version": VERSION,
    u"data_table": open(out_dir_root + u"/output/data_summary.txt").read(),
    u"user_name": unicode(metadata_dictionary[u"user"][u"given_names"]) + u" " + unicode(
        metadata_dictionary[u"user"][u"family_name"]),
    u"date": get_time.strftime(u"%Y-%m-%d"),
    u"user_data_table": tabulate(metadata_dictionary[u"user"][u"list"], tablefmt=u"html", numalign=u"none", floatfmt=u"1.3g"),
    u"experiment_data_table": tabulate(metadata_dictionary[u"cell_line"][u"list"], tablefmt=u"html", numalign=u"none",
                                      floatfmt=u"1.3g"),
    u"gof_table": tabulate(sorted_models,
                          [u"Name", u"&Chi;<sup>2</sup><sub style='position: relative; left: -.5em;'>&nu;</sub>",
                           u"Warning"], tablefmt=u"html", floatfmt=u".3g", numalign=u"none"),
    u"models_table": tabulate(sorted_models_2, [u"Name", u"MAPE", u"Warning"], tablefmt=u"html", floatfmt=u".3g",
                             numalign=u"none"),
    u"data_caption": metadata_dictionary[u"cell_line"][u"caption"],
    u"citation_text": open(u"files/citation.txt").read(),
    u"DCL_name": unicode(metadata_dictionary[u"cell_line"][u"MultiCellDB"]),
    u"cell_line_name": unicode(metadata_dictionary[u"cell_line"][u"name"]),
}

f_2 = open(out_dir_root + u"/output/rcs_ranking.csv", u"w")
f_2.write(u"Name,Reduced_Chi_Squared,Warning\n")
for model_row in sorted_models_table:
    f_2.write(u"{:s},".format(model_row[0]))
    f_2.write(u"{:.3g},".format(model_row[1]))
    f_2.write(u"{:s}\n".format(model_row[2]))
f_2.close()

f_2 = open(out_dir_root + u"/output/mape_ranking.csv", u"w")
f_2.write(u"Name,MAPE,Warning\n")
for model_row in sorted_models_2_table:
    f_2.write(u"{:s},".format(model_row[0]))
    f_2.write(u"{:.3g},".format(model_row[1]))
    f_2.write(u"{:s}\n".format(model_row[2]))
f_2.close()

html_template = open(u"files/index_template.html").read()
f.write(html_template.format(html_dictionary))
f.close()
shutil.copy(u"files/style.css", out_dir_root + u"/output/")

# clean up the working folder
os.remove(u"matplotlibrc")

# creating the zip file with the ouputs of CellPD
shutil.make_archive(u'CellPD_output', u'zip', root_dir=None, base_dir=out_dir_root)
if os.path.isfile(out_dir_root + u"/output/CellPD_output.zip"):
    os.remove(out_dir_root + u"/output/CellPD_output.zip")
shutil.move(u"CellPD_output.zip", out_dir_root + u"/output/")

if OPEN_BROWSER:
    new = 2  # open in a new tab, if possible
    # open an HTML file on my own (Windows) computer
    url = u"overall_report.html"
    webbrowser.open(u'file://' + os.path.realpath(out_dir_root + u"/" + url), new=new)

if HOLD_CMD:
    raw_input(u"Press enter to exit, please :) \n")
