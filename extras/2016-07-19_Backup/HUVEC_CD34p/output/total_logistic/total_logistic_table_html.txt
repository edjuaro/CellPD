<table>
<tr><th>Parameter        </th><th>   Value</th><th>  Std. Error</th><th>units          </th><th>  Inv_value</th></tr>
<tr><td>growth_rate      </td><td>0.0322  </td><td>0.0119      </td><td>1/hours        </td><td>31         </td></tr>
<tr><td>carrying_capacity</td><td>4.82e+08</td><td>2.56e+14    </td><td>number of cells</td><td>2.07e-09   </td></tr>
</table>