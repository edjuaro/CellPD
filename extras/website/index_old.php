<!DOCTYPE HTML> 
<html> 
  
<head> 
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8"> 
 
<meta name="author" content="Edwin Juarez"> 
<meta name="publisher" content="Center for Applied Molecular Medicine (CAMM), USC"> 
<meta name="description" content="CellPD: Cell Phenotype Digitizer"> 
<meta name="keywords" content="CellPD,Biomathematics,Oncology,Predictive Oncology,Cancer,Modeling,Multidisciplinary,MultiCellXML,MultiCellDS,digital cell lines,data standards">  

<link rel="stylesheet" type="text/css" media="screen" href="./css/multicellds-screen.css"> 
<!-- 
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"> 
<link rel="icon" type="image/x-icon" href="images/favicon.ico">
-->
 
<title> 
CellPD : Cell Phenotype Digitizer
</title> 

</head> 
 
<body> 

<?php
 date_default_timezone_set('America/Los_Angeles');

 /* top background gradient, logo bar, menu bar */
 include_once( "./header.php" );
 include_once( "./php_functions/MultiCellDS_functions.php" ); 
?>

<div id="page"> 
<?php
 /* this is the purty banner */
 // include_once( "./banner.php" ); 
 
 /* this is the twitter panel */
 include_once( "./tweets-panel.php" ); 

 /* this is the news parsing on the righthand side */
 include_once( "./news-panel.php" ); 
?>

<div id="content">

<h1>CellPD: Cell Phenotype Digitizer</h1>

<p class="center" style="margin: 4mm;">
<a href="https://sourceforge.net/projects/cellpd/files/CellPD/" class="menu_button"> 
  <span class="download_button">Download the latest version</span></a> 
</p>

<h2 ID="introduction">Introduction</h2>

<? /*
<p>
CellPD responds to the need of user-friendly and open source software in the (computational & systems) biology community. 
Particularly, CellPD allows for automatic quantification of key parameters of cell phenotype such as cell growth rate and doubling time.
</p>
*/
?>

<h3>Project goals
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h3>

<? /*
<p>
CellPD aims to facilitate the use of computational biology techniques and allow scientits who are not trainied in computer programming to leverage mathematical modeling. The design parameters for CellPD can be found in <a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5">CellPD's paper</a> and are sumarized here:
</p>

<ul>

<li>
<b>Utility for experimental biologists:</b> CellPD allows for quantification of key parameters of cell phenotype. Among the possible uses for CellPD are quality control, comparison of cell cultures, and generation of experimental results databases. 
</li>

<li>
<b>Ease of learning and ease of use:</b> CellPD can be learned to use in a few minutes and it only requires familirarity with spreadsheets (such as Microsoft Excel). We provide a tutorial in each download of CellPD.
</li>

<li>
<b>Robustness to sparsity in data:</b> CellPD can estimate parameters of time series data with as few as two samples. Additionally, CellPD was designed to handle data points measured at irregular time intervals. 
</li>

<li>
<b>Accessibility and Shareability:</b> CellPD's source code is freely available via <a href="https://CellPD.sf.net">SourceForge</a>. CellPD is open source and anyone may modify it, provided they follow the terms of the (unrestrictive) MIT licence. 
</li>

<li>
<b>Extensibility:</b> CellPD has been designed to be easily extendable, we have planned a series of extensions to CellPD's functinality. Because CellPD is open source it may also be modiffied by anyone who follows the MIT licence.
</li>

<li>
<b>Portability:</b> In addition to the Python source code of CellPD, we provide executable files for <a href="https://sourceforge.net/projects/cellpd/files/CellPD/">Windows</a> and <a href="https://sourceforge.net/projects/cellpd/files/CellPD/">OSX</a> (no installation required for those). 
</li>


</ul>

*/
?>

<!-- <p class="center" style="margin: 0cm;"><a href="#top" class="action_button">Back to top</a></p> -->


<h2 ID="whats_new">What's New<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>

<? /*
<p>
<b>December 12, 1015:</b> 
The CellPD software paper is now publicly available (free and open access) at 
<i>BMC Systems Biology.</i></p>

<!-- <p style="margin-left: 0.25in; margin-right: 0.25in;"> -->
<blockquote>
E.F. Juarez et al. 
<span class="red">Quantifying differences in cell line population dynamics using CellPD</span>. <i>BMC Systems Biology</i>, 2016.<br>
<b>Open access download</b>: <a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5">click here</a>.
</blockquote>

*/ 
?>

<h2 ID="method">
Method
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>

<? /*
<p>
We use the Levenberg-Marquardt Algorithm (LMA) to perform nonlinear estimation of parameters for various mathematical models of cell population dynamics. Further technical details 
can be found in <a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5">CellPD's manuscript, Juarez et al. (2016)</a>. 
</p>
*/
?>

<!-- <p class="center" style="margin: 0cm;"><a href="#top" class="action_button">Back to top</a></p> -->

<h2 ID="examples">Examples
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>
<? /*
<p>
Examples are included as a tutorial, which is included in 
every <a href="#downloads">CellPD download</a>. Comparison to to other tools as well as using CellPD for quality control and to analize pharmacological effects of (simulated) unknown drugs can be found in <a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5">Juarez et al. (2016)</a>. 
More details will be included here in the near future. 
</p>
  
<!-- <p class="center" style="margin: 0cm;"><a href="#top" class="action_button">Back to top</a></p> -->
*/
?>

<h2 ID="license">Licensing and disclaimers
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>
<p>
CellPD is licensed under the 
<a href="https://opensource.org/licenses/MIT">MIT License</a>.  
Commercial users should <a href="http://MathCancer.org/Contact.php">contact us</a> for alternative licensing. 
</p>
<p>
CellPD is an academic/scientific code, and it should not be used as the basis for individual 
medical decisions. (That's what peer review, clinical trials, and FDA oversight are for!) 
Always consult your physician when making medical decisions. 
</p>
<!-- <p class="center" style="margin: 0cm;"><a href="#top" class="action_button">Back to top</a></p> -->

<h2 ID="downloads">
Downloads
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>

<h3 ID="CellPD_software_download">Software</h3>
<p>
CellPD is available for download at 
<a href="http://CellPD.SourceForge.net">SourceForge</a>. Each download includes a tutorial and exmaples. 
</p>

<h4>Most recent versions</h4>
<? /*
<table style="width: 100%; border-collapse: collapse; margin-bottom: 5mm;" >
<tr>
<th style="padding-left: 0mm; padding-right:4mm; text-align: left; min-width: 2cm; border-bottom: 1.25mm solid black;">Version</th>
<th style="padding-left: 0mm; padding-right:4mm; text-align: left; min-width: 3cm; border-bottom: 1.25mm solid black;">Release Date</th>
<th style="padding-left: 0mm; padding-right:4mm; text-align: left; min-width: 8cm; border-bottom: 1.25mm solid black;">Download link</th>
</tr>

<tr>
<td>1.0.1</td>
<td>26 September 2016</td>
<td><a href="https://sourceforge.net/projects/cellpd/files/CellPD/1.0.1/">https://goo.gl/7CA9ss [sf.net]</a></td>
</tr>
<tr style="margin-bottom: 2mm;">
<td style="padding-left: 5mm; padding-bottom: 2mm; border-bottom: 1px solid rgb(128,128,128);" colspan="3">
<b>Notes:</b> Minor text fixes. No effect on scientific results.
</td>
</tr>

<tr>
<td>1.0.0</td>
<td>16 January 2016</td>
<td><a href="https://sourceforge.net/projects/cellpd/files/CellPD/1.0.0/">https://goo.gl/FPBtIS [sf.net]</a></td>
</tr>
<tr style="margin-bottom: 2mm;">
<td style="padding-left: 5mm; padding-bottom: 2mm; border-bottom: 1px solid rgb(128,128,128);" colspan="3">
<b>Notes:</b> First public release
</td>
</tr>
</table>

*/
?> 

<h3 ID="CellPD_documentation">Documentation</h3>

<? /*
<dl>
<dt><b>CellPD Software Paper:</b></dt>
<dd>
<a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5">Juarez et al. (2016)</a> published the original version of CellPD. 
</dd>

<dt><b>Tutorials</b></dt>
<dd>A user tutorial with several examples is included with every 
<a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5">CellPD download</a>. </dd>
</dl>

<p>We anticipate writing further tutorials and walk-throughs on the <a href="http://MathCancer.blogspot.com">MathCancer blog</a> over the 
next few months.</p>
*/ 
?>

<h2 ID="support">Support 
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>
<p>
For support, please 
see the <a href="https://sourceforge.net/p/cellpd/tickets/?source=navbar">support ticket tracker</a> or contact 
<a href="https://sourceforge.net/u/edjuaro/profile/send_message">Edwin Juarez</a> or 
<a href="http://MathCancer.org/Contact.php">Paul Macklin</a> . Note that each CellPD download includes a tutorial and examples. 
</p>
<p>
If you plan to use CellPD in a grant proposal, please consider including Paul Macklin as a 
consultant for more dedicated support.  
</p>

<!-- <p class="center" style="margin: 0cm;"><a href="#top" class="action_button">Back to top</a></p> -->

<!--
<h2 ID="roadmap">
Development Roadmap 
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>
<p>
More soon. 
</p>
-->

<h2 ID="cite">How to Cite CellPD
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>
<p>
If you use CellPD in your project, please cite CellPD and the version number, such as below: 
</p>
<blockquote>
<p>We fit the data to several growth models using CellPD (Version 1.0.1), which uses the Levenberg-Marquardt algorithm to perform a least-squares fit of several mathematical models to data [1].</p>
<p>[1] Juarez, E. F., et al. (2016). <span class="red">Quantifying differences in cell line population dynamics using CellPD.</span> BMC systems biology 10(1): 1-12.</p>
<b>DOI:</b> <a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5">10.1186/s12918-016-0337-5</a>. 
</blockquote>
<p>The paper can be downloaded (free, open access) at <a href="https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-016-0337-5"><i>BMC Systems Biology</i></a>. 
To import the paper into your citation manager click here: 
<a href="http://citation-needed.services.springer.com/v2/references/10.1186/s12918-016-0337-5?format=bibtex&flavour=citation">[BibTeX, Mendeley, JabRef (.BIB)]</a> 
<a href="http://citation-needed.services.springer.com/v2/references/10.1186/s12918-016-0337-5?format=endnote&flavour=citation">[EndNote (.ENW)]</a> 
<a href="http://citation-needed.services.springer.com/v2/references/10.1186/s12918-016-0337-5?format=refman&flavour=citation">[Papers, Zotero, RefWorks (.RIS)]</a>. 
</p>
<!--
<p>
You can find full citation information (including downloadable BibTeX and EndNote files)
</p>
-->

<!-- add some downloadable BibTeX and EndNote citations -->
<!-- change this to dd 
<h3>Some publications and projects that cite CellPD</h3>


<ol>
<li ID="CellPD_method_paper">
A. Ghaffarizadeh, S.H. Friedman, and 
P. Macklin, <span class="red">CellPD: an efficient parallelized diffusive transport solver 
for 3-D biological simulations</span>, <i>Bioinformatics</i>, 2015. <br>
<b>DOI:</b> <a href="http://dx.doi.org/10.1093/bioinformatics/btv730">10.1093/bioinformatics/btv730</a>. 
</li>
</ol>

--> 
<h2 ID="other">
Additional topics
<a href="#top" class="action_button" style="position: relative; float: right; margin-right: -15mm;">Back to top</a>
</h2>

<h3>Related Projects</h3>

<ul>
<li><b>PhysiCell:</b> Our lab's physics-based agent-based simulator uses CellPD for simulating the microenvironment.<br>
<b>Link:</b> <a href="http://PhysiCell.MathCancer.org">PhysiCell.MathCancer.org</a> 
</li>

<li><b>MultiCellDS:</b> CellPD will use the MultiCellDS data format once released. As the first major continuum code to commit to the standard, our data structures should influence the structuring of microenvironment data elements in the standard. 
<br>
<b>Link:</b> <a href="http://MultiCellDS.org">MultiCellDS.org</a> 
</li>

</ul>

</div>

</div>

<?php
 /* the bottom footer and such */
 $data = file_get_contents( "./common_html/bottom-formatting.html" );
 echo $data; 
?>

</body>

</html>