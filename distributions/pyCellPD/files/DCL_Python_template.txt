<?xml version="1.0" encoding="UTF-8"?>
<MultiCellDS version="0.0" type="cell_line">
	<cell_line MultiCellDB_ID="-1" ID="0" MultiCellDB_name="{0[MCDS_label]:s}" label="{0[MCDS_name]:s}">
		<metadata>
			<description>
				Digital cell line for {0[MCDS_name]:s} cells, generated from data provided by {0[user_given_names]:s} {0[user_family_name]:s} on {0[short_date]:s}. These are {0[cell_description]:s}.
			</description>
			<citation>
				<text>
					If this digital cell line is used in a publication, cite it as:
					"These are the results from running CellPD (Version {0[version]:1.1f})..."
					[1] E. Juarez et al., CellPD: A Cell Line Digitizer to estimate and record critical cell line growth parameters (2016, in preparation)
					[2] S. H. Friedman et al., Recording microenvironment-dependent cell phenotypes as digital cell lines with MultiCellDS (2016, in preparation)
				</text>			
				<URL>{0[data_url]:s}</URL>
			</citation>
			<curation curated="false">  
				<created>{0[long_date]:s}</created>
				<last_modified>{0[long_date]:s}</last_modified>
				<version>1.0</version>
				<creator>
					<orcid-identifier>
						<path>{0[user_ORCID]:s}</path>
						<given-names>{0[user_given_names]:s}</given-names>		
						<family-name>{0[user_family_name]:s}</family-name>
						<email>{0[user_email]:s}</email>
						<url>{0[user_url]:s}</url>
						<organization-name>{0[user_organization]:s}</organization-name>
						<department-name>{0[user_department]:s}</department-name>
					</orcid-identifier>
				</creator>
				<current_contact>  
					<orcid-identifier>
						<path>{0[user_ORCID]:s}</path>
						<given-names>{0[user_given_names]:s}</given-names>		
						<family-name>{0[user_family_name]:s}</family-name>
						<email>{0[user_email]:s}</email>
						<url>{0[user_url]:s}</url>
						<organization-name>{0[user_organization]:s}</organization-name>
						<department-name>{0[user_department]:s}</department-name>
					</orcid-identifier>
				</current_contact>
				<curator>  
					<orcid-identifier>
						<path>0000-0003-1062-3642</path>
						<given-names>Edwin</given-names>		
						<family-name>Juarez</family-name>
						<email>juarezro@usc.edu</email>
						<url>http://MathCancer.org</url>
						<organization-name>University of Southern California</organization-name>
						<department-name>Center for Applied Molecular Medicine</department-name>
					</orcid-identifier>
				</curator>
				<last_modified_by>
					<orcid-identifier>
						<path>{0[user_ORCID]:s}</path>
						<given-names>{0[user_given_names]:s}</given-names>		
						<family-name>{0[user_family_name]:s}</family-name>
						<email>{0[user_email]:s}</email>
						<url>{0[user_url]:s}</url>
						<organization-name>{0[user_organization]:s}</organization-name>
						<department-name>{0[user_department]:s}</department-name>
					</orcid-identifier>
				</last_modified_by>
			</curation>
			<data_origins> 
				<data_origin>
					<citation>
						<URL>{0[data_url]:s}</URL>
					</citation>
				</data_origin>
			</data_origins>
			<data_analysis> 
				<URL>http://MathCancer.org</URL>
				<notes>cell_line/phenotype_dataset/phenotype/cell_cycle/cell_cycle_phase/duration does not have associated uncertainty measures because it is a derived quantity from the corresponding rate.</notes>
			</data_analysis>
			<cell_origin>
				<CLO_ID>{0[CLO_id]:s}</CLO_ID>
				<BTO_ID>{0[BTO_id]:s}</BTO_ID>
				<species>{0[cell_species]:s}</species>
				<organ>{0[cell_organ]:s}</organ>
				<disease>{0[cell_disease]:s}</disease>
				<morphology>{0[cell_morphology]:s}</morphology>
			</cell_origin>
			<notes>
				{0[uncertainty_note]:s}
			</notes>
		</metadata>	
		<phenotype_dataset ID="0" keywords="{0[experiment_oxygenation_type]:s}">
			<microenvironment>
				<scale>
					<variables>
						<variable type="concentration" ChEBI_ID="15379" name="oxygen" units="mmHg">
							<mean measurement_type="{0[experiment_oxygenation_measurement_type]:s}">
							{0[experiment_oxygenation]:s}
							</mean>
						</variable>
					</variables>
				</scale>
			</microenvironment>			
			<phenotype type="expected">
				{0[cell_cycle_models]:s}
			</phenotype>
		</phenotype_dataset>
	</cell_line>
</MultiCellDS>