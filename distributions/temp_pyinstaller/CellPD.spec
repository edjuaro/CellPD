# -*- mode: python -*-

block_cipher = None


a = Analysis(['CellPD.py'],
             pathex=['C:\\Users\\edjua\\Dropbox (USC WCC-CAMM)\\CellPD_dev\\distributions\\temp_pyinstaller'],
             binaries=[],
             datas=[],
             hiddenimports=['scipy.linalg.cython_blas', 'scipy.linalg.cython_lapack', 'tkinter', 'tkinter.filedialog'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='CellPD',
          debug=True,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='CellPD')
