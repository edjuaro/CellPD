				<cell_cycle model="{0[model_name]:s}" ID="{0[model_number]:s}">
					<cell_cycle_phase name="{0[param_name]:s}" ID="{0[param_number]:d}">
						<duration units="{0[param_units]:s}" measurement_type="inferred" standard_error_of_the_mean ="{0[param_stderr]:1.3g}">
						{0[param_value]:1.3g}
						</duration> 
					</cell_cycle_phase>
				</cell_cycle>